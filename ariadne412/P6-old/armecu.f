C***********************************************************************
C $Id: armecu.f,v 3.4 2001/11/21 19:24:32 leif Exp $

      DOUBLE PRECISION FUNCTION ARMECU(IN1,IN2)

C...ARiadne function Matrix Element CUt of

C...Return > 0.0 if the current state is above the cut-off used in the
C...matrix element generation. Otherwise return < 0.0

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      common /testme/dy134,dy234,dy12,dy13,dy14,dy23,dy24,dy34
      common /testme2/y134,y234,y12,y13,y14,y23,y24,y34,yc34,yc134,yc234

      INXTP(I)=IP3(IDO(I))
      IPRVP(I)=IP1(IDI(I))
      SM2(I,J)=ARMAS2(I,J)-(BP(I,5)+BP(J,5))**2


      ARMECU=-1.0D0
      CUT=DBLE(PHAR(161)**2)
      IF ( PHAR(162).GT.0.0D0 ) CUT=CUT*PHAR(162)
      IF ( MHAR(160).EQ.1 ) THEN
C...Cut in invariant pt
        IF ( PT2LST.LT.CUT) RETURN
        ARMECU=1.0D0
        RETURN

      ELSEIF (MHAR(160).EQ.2) THEN
C...Cut in invariant mass
        DO 100 I=1,IPART
          DO 110 J=1,I-1
            IF (ARMAS2(I,J).LT.CUT) RETURN
 110      CONTINUE
 100    CONTINUE

      ELSEIF (MHAR(160).EQ.-1) THEN
C...External cut
        IF ( ARUMEC(IN1,IN2).LT.0.0D0 ) RETURN

      ELSE
C...Cut in reduced invariant mass
        DO 200 I=1,IPART
          DO 210 J=1,I-1
            IF (SM2(I,J).LT.CUT) RETURN
 210      CONTINUE
 200    CONTINUE

      ENDIF

      IF ( MHAR(167).GE.7 ) THEN
        IF ( ARPTCA().LT.0.0D0 ) RETURN
      ELSEIF ( MHAR(167).GE.3 ) THEN
C...Always cut on minimum invariant pt
        DO 300 I=1,IPART
          DO 310 J=1,IPART
            IF ( J.EQ.I ) GOTO 310
            DO 320 K=J+1,IPART
              IF ( K.EQ.I) GOTO 320
              IF ( ARIPT2(J,I,K).LT.DBLE(PARA(3))**2 ) RETURN
 320        CONTINUE
 310      CONTINUE
 300    CONTINUE
      ENDIF

      IF ( MHAR(160).eq.0 ) THEN
C...Special pythia 4-jet cuts. First find correct quarks and gluons.
        IF (IPART.NE.4) GOTO 900
        IQ1=IPF(1)
        IF ( .NOT.QQ(IQ1) ) GOTO 900
        IF ( IDO(IQ1).GT.0 ) THEN
          IG2=INXTP(IQ1)
        ELSE
          IG2=IPRVP(IQ1)
        ENDIF
        IF ( ISTRS.EQ.2 ) THEN
          IF ( IFL(IPL(2)).EQ.-IFL(IQ1) ) THEN
            IQ4=IPL(2)
          ELSEIF ( IFL(IPF(2)).EQ.-IFL(IQ1) ) THEN
            IQ4=IPF(2)
          ELSE
            GOTO 900
          ENDIF
          IF ( IDO(IQ4).GT.0 ) THEN
            IG3=INXTP(IQ4)
          ELSE
            IG3=IPRVP(IQ4)
          ENDIF          
        ELSE
          IF ( IDO(IQ1).GT.0 ) THEN
            IG3=INXTP(IG2)
            IQ4=INXTP(IG3)
          ELSE
            IG3=IPRVP(IG2)
            IQ4=IPRVP(IG3)
          ENDIF
        ENDIF

C...Calculate and cut on special y_ij
        Y14=ARMAS2(IQ1,IQ4)
        Y23=ARMAS2(IG2,IG3)
        XQ14=0.5D0*(1.0D0-SQRT(1.0D0-((BP(IQ1,5)+BP(IQ4,5))**2)/Y14))
        XQ23=0.5D0*(1.0D0-SQRT(1.0D0-((BP(IG2,5)+BP(IG3,5))**2)/Y23))
        X12=ARMAS2(IQ1,IG2)-BP(IQ1,5)**2-BP(IG2,5)**2
        X13=ARMAS2(IQ1,IG3)-BP(IQ1,5)**2-BP(IG3,5)**2
        X24=ARMAS2(IG2,IQ4)-BP(IG2,5)**2-BP(IQ4,5)**2
        X34=ARMAS2(IG3,IQ4)-BP(IG3,5)**2-BP(IQ4,5)**2
        Y12=((1.0D0-XQ14)*(1.0D0-XQ23)*X12-XQ14*X24+
     $       XQ23*(XQ14*(X24+X34)-(1.0D0-XQ14)*X13))/
     $       ((1.0D0-2.0D0*XQ14)*(1.0D0-2.0D0*XQ23))
        IF (Y12.LT.CUT) RETURN
        Y13=((1.0D0-XQ14)*(1.0D0-XQ23)*X13-XQ14*X34+
     $       XQ23*(XQ14*(X34+X24)-(1.0D0-XQ14)*X12))/
     $       ((1.0D0-2.0D0*XQ14)*(1.0D0-2.0D0*XQ23))
        IF (Y13.LT.CUT) RETURN
        Y24=((1.0D0-XQ14)*(1.0D0-XQ23)*X24-XQ14*X12+
     $       XQ23*(XQ14*(X12+X13)-(1.0D0-XQ14)*X34))/
     $       ((1.0D0-2.0D0*XQ14)*(1.0D0-2.0D0*XQ23))
        IF (Y24.LT.CUT) RETURN
        Y34=((1.0D0-XQ14)*(1.0D0-XQ23)*X34-XQ14*X13+
     $       XQ23*(XQ14*(X13+X12)-(1.0D0-XQ14)*X24))/
     $       ((1.0D0-2.0D0*XQ14)*(1.0D0-2.0D0*XQ23))
        IF (Y34.LT.CUT) RETURN

        IF ( QQ(IG3) ) THEN
          yc34=y13
          yc134=y12+y13+y23
          yc234=y13+y14+y34
        ELSE
          yc34=y23
          yc134=y12+y13+y23
          yc234=y24+y34+y23
        ENDIF

        IF ( yc34.le.yc134+yc234-armas4(iq1,iq4,ig2,ig3).or.
     $       yc34.ge.yc134*yc234 ) then
          write(*,*) "hello"
          return
        endif

        ARMECU=1.0D0
        RETURN
      ENDIF

 900  ARMECU=1.0D0

      RETURN

C**** END OF ARMECU ****************************************************
      END
