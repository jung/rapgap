C***********************************************************************
C $Id: arsuve.f,v 3.2 2001/12/06 13:34:40 leif Exp $

      DOUBLE PRECISION FUNCTION ARSUVE(I1,I2,I3,PT2,NEMIT)

C...ARiadne function SUdakov VEto

C...Veto the given reconstucted emission sequence according to a sudakov.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'

      DIMENSION I1(5),I2(5),I3(5),PT2(5),PT(0:6)


      ARSUVE=1.0D0

C...Get the number of steps we have to go back
      PT(0)=0.0
      IF ( MHAR(160).EQ.1 ) PT(0)=DBLE(PHAR(161))
      PT(NEMIT+1)=DBLE(PARA(40))

C...First go backward reconstructing the sequence
      DO 100 IEMIT=1,NEMIT
        PT(IEMIT)=SQRT(PT2(IEMIT))
        CALL ARUNDO(I1(IEMIT),I2(IEMIT),I3(IEMIT))
        CALL ARPUTR(10-IEMIT)
 100  CONTINUE

C...Now go forward again checking if there would be an emission
C...vetoing the given emission sequence.
      DO 200 IEMIT=NEMIT,1,-1

C...Make one trial emission
        NP=IPART
        IF ( MSTA(28).NE.0 ) THEN
          DO 210 I=1,IPART
            IF ( .NOT.QQ(I) ) PT2GG(I)=PT(IEMIT+1)**2
 210      CONTINUE
        ENDIF
        CALL AREVO1(PT(IEMIT+1),PT(IEMIT))
        IF ( IPART.GT.NP ) GOTO 900

C...Recall next saved partonic state
        CALL ARGETR(11-IEMIT)

 200  CONTINUE

      RETURN

 900  CALL ARGETR(10)
      ARSUVE=-1.0D0

      RETURN

C**** END OF ARSUVE ****************************************************
      END
