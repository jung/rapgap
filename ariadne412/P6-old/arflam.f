C***********************************************************************
C $Id: arflam.f,v 3.7 2001/06/15 12:11:54 leif Exp $

      DOUBLE PRECISION FUNCTION ARFLAS(PT2)

C...ARiadne function FLavour dependent Alpha_S

C...Returns the ratio of alpha_S(PT2) for variable vs. fixed flavours 

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'ardat2.f'


      ARFLAS=1.0D0
      IF (MSTA(12).LE.1.OR.PT2.LT.PQMAS(4)**2) RETURN
      PT=SQRT(PT2)
      XLAM4=PQMAS(4)*(DBLE(PARA(1))/PQMAS(4))**(27.0D0/25.0D0)
      ARFLAS=(54.0D0*LOG(PT/DBLE(PARA(1))))/(50.0D0*LOG(PT/XLAM4))
      IF (PT2.LT.PQMAS(5)**2) RETURN
      XLAM5=PQMAS(5)*(XLAM4/PQMAS(4))**(25.0D0/23.0D0)
      ARFLAS=(54.0D0*LOG(PT/DBLE(PARA(1))))/(46.0D0*LOG(PT/XLAM5))

      RETURN

C**** END OF ARFLAS ****************************************************
      END
