C***********************************************************************
C $Id: arcasc.f,v 3.17 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE ARCASC

C...ARiadne subroutine perform dipole CASCade

C...Performs a colour dipole cascade on string put in the ariadne
C...event record.

      INCLUDE 'arimpl.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arlist.f'


C...Calculate total momentum of strings for debugging
      IF (MSTA(9).GT.0) CALL ARCHEM(1)

C...Reset counter
      IO=0
      NPTOT=0

C...Perform the evolution
      CALL AREVOL(SQRT(PT2LST),0.0D0)

C...Check momentum and dump to /LUJETS/
      IF (.NOT.QDUMP) CALL ARDUMP
      IF (MSTA(9).GT.0) CALL ARCHEM(0)

      RETURN

C**** END OF ARCASC ****************************************************
      END
C***********************************************************************
C $Id: arcasc.f,v 3.17 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE ARCONT

C...ARiadne subroutine CONTinue dipole cascade

C...Continues a dipole cascade peviously started with ARCASC

      INCLUDE 'arimpl.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'


C...Perform the evolution
      CALL AREVOL(SQRT(PT2LST),0.0D0)

C...Check momentum and dump to /LUJETS/
      IF (.NOT.QDUMP) CALL ARDUMP
      IF (MSTA(9).GT.0) CALL ARCHEM(0)

      RETURN

C**** END OF ARCONT ****************************************************
      END
C***********************************************************************
C $Id: arcasc.f,v 3.17 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE AREVO1(PTMAX,PTMIN)

C...ARiadne subroutine EVOlute with one dipole emission

C...Evolves a string in the ariadne event record using one dipole
C...emissions between the scale PTMAX and PTMIN

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arstrs.f'


      IF ( PTMAX.LE.PTMIN ) RETURN
      MSTA6=MSTA(6)
      MSTA(6)=1
      IO=0
      CALL AREVOL(PTMAX,PTMIN)
      MSTA(6)=MSTA6
      
      RETURN

C**** END OF AREVO1 ****************************************************
      END
C***********************************************************************
C $Id: arcasc.f,v 3.17 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE AREVOL(PTMAX,PTMIN)

C...ARiadne subroutine EVOLute with dipole emissions

C...Evolves a string in the ariadne event record using dipole
C...successive emissions between the scale PTMAX and PTMIN

      INCLUDE 'arimpl.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'


C...Set limits
      PT2LST=PTMAX**2
      PSAV3=DBLE(PARA(3))
      PSAV5=DBLE(PARA(5))
      PARA(3)=MAX(PARA(3),REAL(PTMIN))
      PARA(5)=MAX(PARA(5),REAL(PTMIN))
      QABOVE=(MSTA(35).GT.1.AND.PT2LST.GT.ABS(DBLE(PHAR(112))).AND.
     $     PHAR(112).NE.0.0)

C...Loop over all dipole to find largest possible p_t^2
 100  ISEL=0
      IF (MSTA(35).GT.0.AND.(IO.GT.0.OR.MHAR(108).GT.0)) CALL AREARR
      PT2MAX=0.0D0
      DO 110 I=1,IDIPS
        PT2I=ARGPT2(I)
        IF (PT2I.GT.PT2MAX) THEN
          PT2MAX=PT2I
          ISEL=I
        ENDIF
 110  CONTINUE

C...Check that largest p_t^2 is above cuts.
      IF (ISEL.GT.0) THEN
        IF ((QEM(ISEL).AND.PT2MAX.LE.DBLE(PARA(5))**2).OR.
     $     ((.NOT.QEM(ISEL)).AND.PT2MAX.LE.DBLE(PARA(3))**2)) ISEL=0
      ENDIF

      IF (MSTA(6).GE.0.AND.IO.GE.MSTA(6)) ISEL=0

C...Check if reconnection between strings are possible.
      IF (QABOVE.AND.(PT2MAX.LT.DBLE(ABS(PHAR(112))).OR.ISEL.EQ.0)) THEN
        QABOVE=.FALSE.
        DO 200 ID=1,IDIPS
          IF (QEM(ID)) GOTO 200
          ICOLI(ID)=MOD(ICOLI(ID),1000)
 200    CONTINUE
      ENDIF

C...Exit if below cuts or limit of number of emissions is reached
      IF (MSTA(9).GT.20) XDUM=ARPRNT()
      IF (ISEL.EQ.0) THEN
        IF (MHAR(107).EQ.-1) THEN
          DO 210 ID=1,IDIPS
            IF (QEM(ID)) GOTO 210
            ICOLI(ID)=MOD(ICOLI(ID),1000)
 210      CONTINUE
        ENDIF
        IF (MSTA(35).NE.0) CALL AREARR
        PARA(3)=REAL(PSAV3)
        PARA(5)=REAL(PSAV5)
        RETURN
      ENDIF

C...Perform the emission
      IO=IO+1
      PT2LST=PT2MAX
      CALL AREMIT(ISEL)
      QDUMP=.FALSE.

C...Check total momentum and dump according to debug mode
      IF (MSTA(9).GT.2) CALL ARDUMP
      IF (MSTA(9).GT.1) CALL ARCHEM(0)
      GOTO 100

C**** END OF ARCASC ****************************************************
      END
C***********************************************************************
C $Id: arcasc.f,v 3.17 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE ARESET

C...Ariadne subroutine RESET dipoles

C...Mark all dipole to regenreate their emission variables next call to
C...ARGPT2

      INCLUDE 'arimpl.f'
      INCLUDE 'ardips.f'

      DO 100 I=1,IDIPS
        QDONE(I)=.FALSE.
 100  CONTINUE

      RETURN      

C**** END OF AREVO1 ****************************************************
      END
