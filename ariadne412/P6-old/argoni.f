C***********************************************************************
C $Id: argoni.f,v 3.22 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARGONI(ID)

C...ARiadne subroutine Generate pt2 for ONIum production.

C...Generates a p_t^2 for all possible onium productions from dipole ID


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'aronia.f'


      IF (MSTA(38).EQ.0.OR.MHAR(101).LT.2.OR.MHAR(145).NE.0.OR.
     $     (MHAR(147).GT.0.AND.IO.EQ.0)) RETURN

      DO 100 IONI=1,NONI
        IF (MEONI(IONI).LE.0) GOTO 100
        IF (IPONI(IONI).EQ.0) GOTO 100
        IF (ABS(IFL(IP1(ID))).EQ.IPONI(IONI).AND.
     $       (.NOT.QEX(IP1(ID)))) THEN
          CALL ARPTYO(IP1(ID),IP3(ID),SDIP(ID),IONI,
     $         PT2LST,PT2IN(ID),PT2O,YO,GGM2)
          IF (PT2O.GT.MAX(PT2IN(ID),DBLE(PARA(3))**2)) THEN
            IRAD(ID)=-100000+IONI
            PT2IN(ID)=PT2O
            AEX1(ID)=YO
            AEX3(ID)=GGM2
          ENDIF
        ENDIF
        IF (ABS(IFL(IP3(ID))).EQ.IPONI(IONI).AND.
     $       (.NOT.QEX(IP3(ID)))) THEN
          CALL ARPTYO(IP3(ID),IP1(ID),SDIP(ID),IONI,
     $         PT2LST,PT2IN(ID),PT2O,YO,GGM2)
          IF (PT2O.GT.MAX(PT2IN(ID),DBLE(PARA(3))**2)) THEN
            IRAD(ID)=-100000-IONI
            PT2IN(ID)=PT2O
            AEX1(ID)=YO
            AEX3(ID)=GGM2
          ENDIF
        ENDIF

 100  CONTINUE

C**** END OF ARGONI ****************************************************
      END
C***********************************************************************
C $Id: argoni.f,v 3.22 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARPTYO(I1,I3,S,IONI,PT2MXI,PT2MNI,PT2O,YO,GGM2)

C...ARiadne subroutine Generate PT2 and Y for Onium production.

C...Generates a p_t^2 for a given onium chanel, given the radiating
C...particle I1, the spectator I3, the dipole mass S, the maximum
C...PT2MXI allowed and maximum PT2MNI Generated so far. PT2O and YO
C...gives the result.


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'aronia.f'
      INCLUDE 'pydat1.f'

      DOUBLE PRECISION ARSNGL,ARIACH

      INCLUDE 'ardble.f'


      PT2O=0.0D0
      YO=0.0D0
      Y=0.0D0

      IFLO=IFLONI(IONI)
      DM1=BP(I1,5)
      DM12=DM1**2
      GGM2=DM12
      DMO=PYMASS(IFLO)
      DMO2=DMO**2
      DM3=BP(I3,5)
      DM32=DM3**2
      DW=SQRT(S)
      IF (DW.LE.DMO) RETURN
      IFLQ=MOD(ABS(IFLO)/100,10)
      DMQ=PYMASS(IFLQ)
      DMQ2=DMQ**2
      XNUMFL=MAX(ARNOFL(DW,MAX(5,MSTA(15))),3.0D0)
      ALPHA0=12.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)
      DLAM2=DBLE(PARA(1)**2)
      PT2MN=PT2MNI
      PT2MX=PT2MXI
      IF (MSTA(38).LT.0) THEN
        PT2MN=PT2MNI+DMO2
        PT2MX=PT2MXI+DMO2
      ENDIF
      DSMAX=DBLE(PARA(40))
      IF (MSTA(28).NE.0.AND.IFL(I1).EQ.21.AND.PT2GG(I1).GT.0.0D0) THEN
        IF (MSTA(28).LT.0) PT2MX=DBLE(PARA(40))
        DSMAX=PT2GG(I1)
      ENDIF
      DMT2CT=MAX(DMO2,DBLE(PT2MN))
      DWR2=(DW-DM3)**2
      DMT2MX=((DMO2-DM12+DWR2)**2)/(4.0D0*DWR2)
      DMT2=0.0D0

      IF (MEONI(IONI).EQ.1) THEN
C...This is g->O according to the singlet mechanism
        IF (IFL(I1).NE.21) RETURN
        DME=PONI(1,IONI)
        IF (MHAR(152).NE.0) DME=DME*DBLE(ARWGHT(-100000-IONI))
        DRMIN=(DMO/DW)**2
        DZMIN=DRMIN
        DITOT=3.0D0*LOG((1.0D0+SQRT(1.0D0-DZMIN**2))/DZMIN)
        DTOTM=10.0D0*(ALPHA0**3)*DME*DITOT/
     $       (288.0D0*(ARX2DB(PARU(1))**2)*
     $       (DMO**3)*(LOG(DMT2CT/DLAM2)**3))
        IF (DTOTM.LT.PYR(IDUM)) RETURN
        DARG=PYR(IDUM)*DITOT/3.0D0
        DZ=2.0D0*EXP(DARG)/(1.0D0+EXP(2.0D0*DARG))
        W=2.0D0*LOG((1.0D0-SQRT(DRMIN))/(1.0D0-SQRT(DZ)))
     $       *SQRT(1.0D0-DZ**2)/3.0D0
        IF (W.LT.PYR(IDUM)) RETURN
        DR=(1.0D0-(1.0D0-SQRT(DZ))*
     $       (((1.0D0-SQRT(DRMIN))/(1.0D0-SQRT(DZ)))**PYR(IDUM)))**2
        W=0.5D0*ARSNGL(SQRT(DR),DZ)
        IF (W.LT.PYR(IDUM)) RETURN
        DS=DMO2/DR
        IF (DS.GT.DSMAX) RETURN
        DU=0.0D0
        DUMAX=(1.0D0-DZ)*(1.0D0-DR/DZ)
        IF (ABS(MSTA(38)).EQ.2.AND.Q2GONI(IONI).AND.
     $       DUMAX*DS.GT.DBLE(PARA(3))**2) THEN
          WMAX=MAX(AR2GDI(DR,DZ,0.0D0),AR2GDI(DR,DZ,DUMAX))
 200      DU=PYR(IDUM)*DUMAX
          W=AR2GDI(DR,DZ,DU)/WMAX
          IF (W.LT.PYR(IDUM)) GOTO 200
        ENDIF
        DMGG2=DU*DS
        IF (DMGG2.LE.DBLE(PARA(3))**2) DMGG2=0.0D0
        DMT2=DS*DZ*(1.0D0-DZ)+(DMO2-DMGG2)*DZ
        IF (DMT2.LT.DMT2CT) RETURN
        IF (DMT2.GT.MIN(DMT2MX,DBLE(PT2MX))) RETURN
        IF ((LOG(DMT2CT/DLAM2)/LOG(DMT2/DLAM2))**3.LT.PYR(IDUM)) RETURN
        DMT=SQRT(DMT2)
        DEY=DZ*DW/DMT
        Y=LOG(DEY)
        CALL ARCHKO(DW,DMT2,DMT,DEY,DMO2,DMGG2,DM32,I3,*900)
        GGM2=DMGG2
      ELSEIF (MEONI(IONI).EQ.3) THEN
C...This is g->O according to the octet mechanism
        DV2=PONI(2,IONI)**2
        DV4=DV2**2
        DOF=PONI(1,IONI)
        IF (MHAR(152).NE.0) DOF=DOF*DBLE(ARWGHT(-100000-IONI))
        DMT2U=DMO2*((2.0D0+DV4)**2)/(4.0D0+4.0D0*DV4)
        IF (MSTA(38).LT.0) DMT2U=(DMO2+DBLE(PARA(3))**2)*
     $       ((2.0D0+DV4)**2)/(4.0D0+4.0D0*DV4)
        DMT2=MIN(DMT2U,DBLE(PT2MX))
        DMT2=MIN(DMT2,DMT2MX)
        DMT20=DMT2
        IF (DMT2.LT.DMT2CT) RETURN
        DI=ARIACH(DW,DMO,DM3,DV4,DPPMIN,DPPMAX,DSMAX)
        IF (DI.LE.0.0D0) RETURN
        YINT=LOG(DPPMAX/DPPMIN)
        DCN=(48.0D0*(DMQ**3))/
     $       (YINT*ALPHA0*ARX2DB(PARU(1))*DOF/DI)
 300    DARG=PYR(IDUM)
        IF (LOG(DARG)*DCN.LT.
     $       LOG(LOG(DMT2CT/DLAM2)/LOG(DMT2/DLAM2))) RETURN
        DMT2=DLAM2*(DMT2/DLAM2)**(DARG**DCN)
        DMT=SQRT(DMT2)
        Y=LOG(DPPMIN/DMT)+PYR(IDUM)*YINT
        DEY=EXP(Y)
        DZ=DMT*DEY/DPPMAX
        DS=DMT2/DZ+(DMT2-DMO2)/(1.0D0-DZ)
        IF (DS.GT.DSMAX) GOTO 300
        IF (DS.GT.DMO2*(1.0D0+DV4)) GOTO 300
        W=16.0D0*(DMQ**4)/(DS**2)
        IF (W.LT.PYR(IDUM)) GOTO 300
        CALL ARCHKO(DW,DMT2,SQRT(DMT2),DEY,DMO2,DM12,DM32,I3,*300)
      ELSEIF (MEONI(IONI).EQ.41) THEN
        R02=PONI(1,IONI)
        IF (MHAR(152).NE.0) R02=R02*ARWGHT(-100000-IONI)
        DMT2=MIN(DMT2MX,DBLE(PT2MX))
        IF (DMT2.LT.DMT2CT) RETURN
        YMAXI=0.5D0*LOG(S/DMT2CT)
        YMINI=-YMAXI
        IF (YMAXI.LE.YMINI) RETURN
        ALPHA0=12.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)
        DMT2M=DMT2
        CN=(27.0D0*DMT2CT*DMQ*ARX2DB(PARU(1))*LOG(DMT2CT/DLAM2))/
     $       (3.0D0*8.0D0*(ALPHA0**2)*(YMAXI-YMINI)*R02)
 410    ARG=PYR(IDUM)
        IF (LOG(ARG)*CN.LT.
     $       LOG(LOG(DMT2CT/DLAM2)/LOG(DMT2/DLAM2))) RETURN
        DMT2=DLAM2*(DMT2/DLAM2)**(ARG**CN)
        YMAX=0.5D0*LOG(S/DMT2)
        YMIN=-YMAX
        IF (YMAX.LE.YMIN) GOTO 410
        Y=YMIN+PYR(IDUM)*(YMAX-YMIN)
        W1=(YMAX-YMIN)/(YMAXI-YMINI)
        W2=LOG(DMT2CT/DLAM2)/LOG(DMT2/DLAM2)
        DMT=SQRT(DMT2)
        DEY=EXP(Y)
        DZ=DMT*DEY/DW
        DS=DMT2/DZ+(DMT2-DMO2+DMQ2)/(1.0D0-DZ)
        W3=DMT2CT*DMT2/((1.0D0-DZ)*(DS-DMQ2)**2)
        W4=((DS**2-2.0D0*DMQ2*DS-47.0D0*DMQ**4)
     $         - DZ*(DS-DMQ2)*(DS-9.0D0*DMQ2)
     $         + 4.0D0*DS*(DS-DMQ2)*DZ*(1.0D0-DZ)/(2.0D0-DZ)
     $         - 4.0D0*DMQ2*(DS-DMQ2)*(8.0D0-7.0D0*DZ 
     & -5.0D0*DZ**2)/(2.0D0-DZ)
     $         + 12.0D0*(((DS-DMQ2)*DZ)**2)*(1.0D0-DZ)/((2.0D0-DZ)**2))/
     $       (3.0D0*(DS-DMQ2)**2)
        IF (W1*W2*W3*W4.LT.PYR(IDUM)) GOTO 410
        CALL ARCHKO(DW,DMT2,DMT,DEY,DMO2,DM12,DM32,I3,*410)
      ELSEIF (MEONI(IONI).EQ.40) THEN
        R02=PONI(1,IONI)
        IF (MHAR(152).NE.0) R02=R02*ARWGHT(-100000-IONI)
        DMT2=MIN(DMT2MX,DBLE(PT2MX))
        IF (DMT2.LT.DMT2CT) RETURN
        YMAXI=0.5D0*LOG(S/DMT2CT)
        YMINI=-YMAXI
        IF (YMAXI.LE.YMINI) RETURN
        ALPHA0=12.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)
        DMT2M=DMT2
        CN=(27.0D0*DMT2CT*DMQ*ARX2DB(PARU(1))*LOG(DMT2CT/DLAM2))/
     $       (3.0D0*8.0D0*(ALPHA0**2)*(YMAXI-YMINI)*R02)
 400    ARG=PYR(IDUM)
        IF (LOG(ARG)*CN.LT.
     $       LOG(LOG(DMT2CT/DLAM2)/LOG(DMT2/DLAM2))) RETURN
        DMT2=DLAM2*(DMT2/DLAM2)**(ARG**CN)
        YMAX=0.5D0*LOG(S/DMT2)
        YMIN=-YMAX
        IF (YMAX.LE.YMIN) GOTO 400
        Y=YMIN+PYR(IDUM)*(YMAX-YMIN)
        W1=(YMAX-YMIN)/(YMAXI-YMINI)
        W2=LOG(DMT2CT/DLAM2)/LOG(DMT2/DLAM2)
        DMT=SQRT(DMT2)
        DEY=EXP(Y)
        DZ=DMT*DEY/DW
        DS=DMT2/DZ+(DMT2-DMO2+DMQ2)/(1.0D0-DZ)
        W3=DMT2CT*DMT2/((1.0D0-DZ)*(DS-DMQ2)**2)
        W4=((DS**2-2.0D0*DMQ2*DS-15.0D0*DMQ**4)
     $         - DZ*(DS-DMQ2)*(DS-9.0D0*DMQ2)
     $         + 4.0D0*DS*(DS-DMQ2)*DZ*(1.0D0-DZ)/(2.0D0-DZ)
     $         - 4.0D0*DMQ2*(DS-DMQ2)*DZ*(1.0D0-3.0D0*DZ)/(2.0D0-DZ)
     $         + 4.0D0*(((DS-DMQ2)*DZ)**2)*(1.0D0-DZ)/((2.0D0-DZ)**2))/
     $       (3.0D0*(DS-DMQ2)**2)
        IF (W1*W2*W3*W4.LT.PYR(IDUM)) GOTO 400
        CALL ARCHKO(DW,DMT2,DMT,DEY,DMO2,DM12,DM32,I3,*400)
      ENDIF

      PT2O=DMT2
      YO=Y
      IF (MSTA(38).LT.0) PT2O=DMT2-DMO2

 900  CONTINUE

      RETURN

C**** END OF ARPTYO ****************************************************
      END
C***********************************************************************
C $Id: argoni.f,v 3.22 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARADDO(IPO,IFLO,MEO,Q2G,P1,P2,P3,P4,P5)

C...ARiadne subroutine ADD Onium production channel

C...Adds an onium production channel in the /ARONIA/ common block,
C...producing an onium of type IFLO from a parton IPO using process
C...type MEO with parameters P1,...,P5 and producing two gluons if Q2G


      INCLUDE 'arimpl.f'
      INCLUDE 'aronia.f'


      CALL ARREMO(IPO,IFLO,MEO,Q2G)
      NONI=NONI+1
      IPONI(NONI)=IPO
      MEONI(NONI)=MEO
      IFLONI(NONI)=IFLO
      Q2GONI(NONI)=Q2G
      PONI(1,NONI)=P1
      PONI(2,NONI)=P2
      PONI(3,NONI)=P3
      PONI(4,NONI)=P4
      PONI(5,NONI)=P5

      RETURN

C**** END OF ARADDO ****************************************************
      END
C***********************************************************************
C $Id: argoni.f,v 3.22 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARREMO(IPO,IFLO,MEO,Q2G)

C...ARiadne subroutine AREMove Onium production channel

C...Remove an onium production channel in the /ARONIA/ common block,
C...producing an onium of type IFLO from a parton IPO using process
C...type MEO and producing two gluons if Q2G

      INCLUDE 'arimpl.f'
      INCLUDE 'aronia.f'


      IONI=1
 100  IF (IONI.GT.NONI) RETURN
      IF (IPONI(IONI).NE.IPO.OR.MEONI(IONI).NE.MEO.OR.
     $     IFLONI(IONI).NE.IFLO.OR.(Q2GONI(IONI).NEQV.Q2G)) THEN
        IONI=IONI+1
        GOTO 100
      ENDIF

      NONI=NONI-1
      DO 200 JONI=IONI,NONI
        IPONI(JONI)=IPONI(JONI+1)
        MEONI(JONI)=MEONI(JONI+1)
        IFLONI(JONI)=IFLONI(JONI+1)
        Q2GONI(JONI)=Q2GONI(JONI+1)
        PONI(1,JONI)=PONI(1,JONI+1)
        PONI(2,JONI)=PONI(2,JONI+1)
        PONI(3,JONI)=PONI(3,JONI+1)
        PONI(4,JONI)=PONI(4,JONI+1)
        PONI(5,JONI)=PONI(5,JONI+1)
 200  CONTINUE

      RETURN

C**** END OF ARREMO ****************************************************
      END
C***********************************************************************
C $Id: argoni.f,v 3.22 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARCHKO(DW,DMT2,DMT,DEY,DMO2,DMP2,DMR2,IR,*)

C...ARiadne subroutine CHeck Kinematics of Onium emission

C...Checks the kinematics of a possible onium emission. Uses alternate
C...return for failior.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'arhide.f'


      DPO=DMT*DEY
      DMO=DMT/DEY
      IF (DMO.GE.DW.OR.DPO.GT.DW) GOTO 900
      DB=(DW-DPO)*(DW-DMO)
      DMTP2=DMP2+DMT2-DMO2
      DA=DB+DMR2-DMTP2
      DARG=DA**2-4.0D0*DMR2*DB
      IF (DARG.LT.0.0D0.OR.DA.LE.0.0D0) GOTO 900
      DMR=0.5D0*(DA+SQRT(DARG))/(DW-DPO)
      DPR=DMR2/DMR
      IF (DW-DPR-DPO.LT.0.0D0.OR.DW-DMR-DMO.LT.0.0D0) GOTO 900
      IF (MHAR(141).GT.0.AND.DW-DPR-DPO.LT.DW-DMR-DMO) GOTO 900
      IF (MHAR(140).EQ.0.OR.(.NOT.QEX(IR))) RETURN
      IF (DW-DMR.GT.DW*((XPMU(IR)/DMT)**XPA(IR))) GOTO 900

      RETURN

 900  RETURN 1

C**** END OF ARCHKO ****************************************************
      END
