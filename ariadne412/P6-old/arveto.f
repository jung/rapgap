C***********************************************************************
C $Id: arveto.f,v 3.24 2001/11/22 10:52:01 leif Exp $

      DOUBLE PRECISION FUNCTION ARVET1()

C...ARiadne function VETo factor version 1

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for photon emission with constant alpha_EM

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint1.f'


      ARVET1=0.0D0
      IF (B2.LE.0) RETURN
      ARVET1=-((FQ1*(1.0D0-B1)/B2-FQ3*(1.0D0-B3)/B2)**2)*
     $         (B1**NXP1+B3**NXP3)*(YMAX-YMIN)*0.5D0/LOG(XT2)

      IF (MSTA(19).EQ.0) RETURN

      ARVET1=ARVET1*ARVETH()

      RETURN

C**** END OF ARVET1 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVET2()

C...ARiadne function VETo factor version 2

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for photon emission with running alpha_EM

      INCLUDE 'arimpl.f'
      INCLUDE 'arint1.f'


      ARVET2=ARVET1()*PYALEM(XT2*S)/PYALEM(0.25D0*S)

      RETURN

C**** END OF ARVET2 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVET3()

C...ARiadne function VETo factor version 3

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for gluon emission with constant alpha_QCD

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint1.f'


      ARVET3=-(B1**NXP1+B3**NXP3)*(YMAX-YMIN)*0.5D0/LOG(XT2)

      IF (MSTA(19).EQ.0) RETURN

      ARVET3=ARVET3*ARVETH()


      RETURN

C**** END OF ARVET3 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVET4()

C...ARiadne function VETo factor version 4

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for gluon emission with running alpha_QCD

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'arint1.f'
      INCLUDE 'arstrs.f'

      save rr

      XNOM=0.5D0
      XGFUN=0.0D0
      IF (PHAR(133).GT.0.0.AND.(NXP1.EQ.3.OR.NXP3.EQ.3)) THEN
        IF (NXP1.EQ.3.AND.NXP3.EQ.3) THEN
          XGFUN=DBLE(PHAR(133))*XT2
          XNOM=1.0D0/(2.0D0+0.25D0*DBLE(PHAR(133)))
        ELSE
          XGFUN=0.5D0*DBLE(PHAR(133))*XT2
          XNOM=1.0D0/(2.0D0+0.125D0*DBLE(PHAR(133)))
        ENDIF
      ENDIF
      ARVET4=(B1**NXP1+B3**NXP3+XGFUN)*(YMAX-YMIN)*XNOM/YINT

      IF (MHAR(155).LT.0) ARVET4=ARVET4*ARDIME()

      IF (ABS(MSTA(19)).EQ.2.OR.
     $     (ABS(MSTA(19)).EQ.3.AND.IFL1.EQ.-IFL3)) THEN
        ARVET4=ARVET4*XT2*((B1**NXP1+B3**NXP3)/((BC1-B1)*(BC3-B3))
     $       -4.0D0*Y1/(BC3-B3)-4.0D0*Y3/(BC1-B1)
     $       -2.0D0*Y1/(BC3-B3)**2-2.0D0*Y3/(BC1-B1)**2
     $       -4.0D0*(Y1**2)/(BC3-B3)-4.0D0*(Y3**2)/(BC1-B1))
     $       /(B1**NXP1+B3**NXP3)
        RETURN
      ENDIF

      IF (ABS(MSTA(19)).EQ.4.OR.(ABS(MSTA(19)).EQ.5.0.AND.IO.EQ.0)) THEN
        VECAX=0.0
        IVECAX=11
        IF (ABS(MSTA(19)).EQ.4.AND.IO.EQ.0) THEN
          VECAX=0.13
          IVECAX=13
        ENDIF
        RR=XT2*ARMAEL(IVECAX,B1,B3,SY1,SY3,VECAX)/(B1**2+B3**2)
        ARVET4=MAX(RR*ARVET4,0.0D0)
        IF (ABS(MSTA(19)).EQ.6.AND.RR.GT.0.0D0)
     $       ARVET4=ARVET4*SQRT((1D0-(SY1+SY3)**2)*(1D0-(SY1-SY3)**2))
        RETURN
      ENDIF

      IF (MSTA(19).LE.0) RETURN
      IF (MSTA(19).GE.2.AND.IO.EQ.0) RETURN

      ARVET4=ARVET4*ARVETH()


      RETURN

C**** END OF ARVET4 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVET5()

C...ARiadne function VETo factor version 5

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for q-qbar emission

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint1.f'


      ARVET5=((1.0D0-B3+Y3)**2+(1.0D0-B2+Y2)**2)*XT*
     $         (EXP(-YMIN)-EXP(-YMAX))/YINT

      IF (MSTA(23).GE.1) ARVET5=ARVET5*(BC1-B1)/(1.0D0-B1+Y1)

      RETURN

C**** END OF ARVET5 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVET6()

C...ARiadne function VETo factor version 6

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for gluon emission with running alpha_QCD according to
C...O(alpha_S) gluon emission for lepto production

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint1.f'
      INCLUDE 'arint4.f'
      INCLUDE 'arhide.f'


      BCNST0=0.0D0
      Y3P=ZSQEV*Y3
      Y2P=ZSQEV*Y2
      Y1P=ZSQEV*Y1
      B3P=1.0D0-ZSQEV*(1.0D0-B3)
      B1P=B1-(Y1P-Y3P)*(1.0D0-ZSQEV)/ZSQEV
      B2P=2.0D0-B1P-B3P
      BASSX1=B1P
      BASSX3=B3P

      B13=MAX(1.0D0-B3P+Y3P,BCNST0)
      B12=MAX(1.0D0-B2P+Y2P,BCNST0)
      B11=MAX(1.0D0-B1P+Y1P,BCNST0)
      B13Q=B13+SQ2
      CG1=((B12/B3P)**2+(SQ2/B13Q)**2)*SQ2/B13Q
      CG2=2.0D0*(B12*SQ2/(B13Q*B3P)+1.0D0)*B13*B11*SQ2/(B3P*B13Q**2)
      CG3=4.0D0*YFAC*B11*B12*B13*(SQ2**2)/((B3P**2)*B13Q**3)
      CG0=(3.0D0+4.0D0*YFAC/27.0D0)
      IF (MHAR(116).LE.0) THEN
        CG1=(B12/B3P)**2+(SQ2/B13Q)**2
        CG2=2.0D0*(B12*SQ2/(B13Q*B3P)+1.0D0)*B13*B11/(B3P*B13Q)
        CG3=4.0D0*YFAC*B11*B12*B13*SQ2/((B3P**2)*B13Q**2)
        CG0=(6.0D0+0.25D0*YFAC)
        IF (MHAR(116).LT.0) CG0=CG0*SQRT(B13Q/SQ2)
      ENDIF

      ARVET6=(YMAX-YMIN)*(CG1+CG2+CG3)/(YINT*CG0)

      IF (MSTA(19).EQ.0) RETURN

      IF (MHAR(151).EQ.1) ARVET6=ARVET6*
     $     MIN(1.0D0,LOG(XT2/XLAM2)/LOG(DBLE(PARA(21))*SQ2/XLAM2))

      ARVET6=ARVET6*ARVETH()


      RETURN

C**** END OF ARVET6 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVET7()

C...ARiadne function VETo factor version 7

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for gluon emission with constant alpha_QCD according to
C...O(alpha_S) gluon emission for lepto production


      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint1.f'
      INCLUDE 'arint4.f'
      INCLUDE 'arhide.f'

      BCNST0=0.0D0
      Y3P=ZSQEV*Y3
      Y2P=ZSQEV*Y2
      Y1P=ZSQEV*Y1
      B3P=1.0D0-ZSQEV*(1.0D0-B3)
      B1P=B1-(Y1P-Y3P)*(1.0D0-ZSQEV)/ZSQEV
      B2P=2.0D0-B1P-B3P
      BASSX1=B1P
      BASSX3=B3P

      B13=MAX(1.0D0-B3P+Y3P,BCNST0)
      B12=MAX(1.0D0-B2P+Y2P,BCNST0)
      B11=MAX(1.0D0-B1P+Y1P,BCNST0)
      B13Q=B13+SQ2
      CG1=((B12/B3P)**2+(SQ2/B13Q)**2)*SQ2/B13Q
      CG2=2.0D0*(B12*SQ2/(B13Q*B3P)+1.0D0)*B13*B11*SQ2/(B3P*B13Q**2)
      CG3=4.0D0*YFAC*B11*B12*B13*(SQ2**2)/((B3P**2)*B13Q**3)
      CG0=(3.0D0+4.0D0*YFAC/27.0D0)
      IF (MHAR(116).LE.0) THEN
        CG1=(B12/B3P)**2+(SQ2/B13Q)**2
        CG2=2.0D0*(B12*SQ2/(B13Q*B3P)+1.0D0)*B13*B11/(B3P*B13Q)
        CG3=4.0D0*YFAC*B11*B12*B13*SQ2/((B3P**2)*B13Q**2)
        CG0=(6.0D0+0.25D0*YFAC)
        IF (MHAR(116).LT.0) CG0=CG0*SQRT(B13Q/SQ2)
      ENDIF

      ARVET7=-(YMAX-YMIN)*(CG1+CG2+CG3)/(LOG(XT2)*CG0)

      IF (MSTA(19).EQ.0) RETURN

      ARVET7=ARVET7*ARVETH()


      RETURN

C**** END OF ARVET7 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVETH()

C...ARiadne function Heavy VETo factor

C...Extra acceptance factor for heavy dipoles

      INCLUDE 'arimpl.f'
      INCLUDE 'arint1.f'


      ARVETH=0.0D0
      BX1=1.0D0-B1+Y1-Y3
      BX3=1.0D0-B3+Y3-Y1
      IF (B2.GE.1.0D0.OR.BX1.LE.0.OR.BX3.LE.0) RETURN
      BXM=BX1/BX3
      ARVETH=1.0D0-(Y1*BXM+Y3/BXM)/(1.0D0-B2)

      RETURN

C**** END OF ARVETH ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARVET8()

C...ARiadne function VETo factor version 8

C...Determine the acceptance factor for chosen x_t^2 and y
C...Suitable for q-qbar emission

      INCLUDE 'arimpl.f'
      INCLUDE 'arint1.f'


      ARVET8=((1.0D0-B3+Y3)**2+(1.0D0-B2+Y2)**2)*(BC3-B3)*
     $         (YMAX-YMIN)/YINT

      RETURN

C**** END OF ARVET8 ****************************************************
      END
C***********************************************************************

      DOUBLE PRECISION FUNCTION ARDIME()

C...ARiadne function DIpole Matrix Element correction.

C...Determines a correction factor to account for the full 1->3 matrix
C...element.

      INCLUDE 'arimpl.f'
      INCLUDE 'arint1.f'
      INCLUDE 'arhide.f'


      ARDIME=1.0D0
      IF (NXP1.EQ.2.AND.NXP3.EQ.2) RETURN

      IF (NXP1.EQ.3) THEN
        X1=B1
        X3=B3
      ELSE
        X1=B3
        X3=B1
      ENDIF
      X2=B2

      DIVG=1.0D0
      IF (MHAR(155).EQ.-1) DIVG=1.0D0

      IF (NXP1.EQ.2.OR.NXP3.EQ.2) THEN
        XM12=2.0D0*X3*((1.0D0-X1)*(1.0D0-X2)*(1.0D0/X1**2+1.0D0/X2**2)
     $                  +X1*X2/(X1+X2)**2
     $                  -DIVG*2.0D0*(1.0D0-X1)*(1.0D0-X2)*(1.0D0-X3)
     $                  /(X1*X2*(X1+X2)**2))/(1.0D0-X3)
        XM22=(X2+2.0D0*(X2+X3)*(1.0D0-X2)*(1.0D0-X3)/X2**2)
     $       /(1.0D0-X1)
        XM1M2=((X2+X3)*(1.0D0-X2)/X1**2
     $        +(X2+2.0D0*X3)*(1.0D0-X2)/X2**2
     $        +DIVG*((1.0D0-X2)*X1-2.0D0*(1.0D0-X3)/X2)
     $         /((X1+X2)*X1**2)
     $        +DIVG*X2/(X1+X2)
     $        -DIVG*2.0D0*(1.0D0-X1)*(1.0D0-X2)*(1.0D0-X3)
     $         /(X2*(X1+X2)*X1**2))
        XM=XM12+XM22+2.0D0*XM1M2
        XM=2.0D0*(1.0D0-X2)/((1.0D0-X1)*(1.0D0-X3))
     $    +2.0D0*X2*(1.0D0-X2)/((1.0D0-X3)*X1**2)
     $    +2.0D0*X1*X2*X3/((1.0D0-X3)*(X1+X2)**2)
     $    +X2/(1.0D0-X1)
      ELSE
        XM=2.0D0*(1.0D0-X2)*((1.0D0/X1**2+1.0D0/X2**2+1.0D0/X3**2)
     $                         *(X2**2)/((1.0D0-X1)*(1.0D0-X3))
     $                       -4.0D0*X1/((X1+X2)*X3**2)
     $                       -4.0D0*X3/((X3+X2)*X1**2))
      ENDIF

      ARDIME=XM*((1.0D0-X1)*(1.0D0-X3))

      RETURN

C**** END OF ARDIME ****************************************************
      END
