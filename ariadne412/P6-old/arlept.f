C***********************************************************************
C $Id: arlept.f,v 3.26 2001/01/24 21:37:58 leif Exp $

      SUBROUTINE ARLEPT

C...ARiadne subroutine perform cascade on LEPTo event

C...Performs a cascade starting on a zero'th order event from LEPTO


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint4.f'
      INCLUDE 'arhide.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'leptou.f'


C...Check that Ariadne was properly initialized
      IF (MSTA(2).EQ.0.OR.MSTA(1).NE.3) CALL ARERRM('ARLEPT',12,0)
      IFLASS=0
      MSAV33=MSTA(33)

C...Boost to the hadronic center of mass
      CALL ARBOLE(THEL,PHI1,PHI2,DBXL,DBYL,DBZL)

C...Check which quark was struck and try to decide whether it was
C...a sea quark
      IFLSTR=LST(25)

C...Copy to Ariadne event record
      IPART=0
      CALL ARBOOP
      CALL ARCOPA(5,1,SIGN(1,IFLSTR))
      IF (MSTA(30).LT.2) THEN
        QEX(1)=.FALSE.
        XPMU(1)=0.0D0
        XPA(1)=0.0D0
      ELSE
        QEX(1)=.TRUE.
        IF (PARA(14).GE.0.0) THEN
          XPMU(1)=SQRT(XQ2)*DBLE(PARA(14))
        ELSE
          XPMU(1)=DBLE(ABS(PARA(14)))
        ENDIF
        XPA(1)=DBLE(PARA(15))
      ENDIF
      CALL ARBOOP
      CALL ARCOPA(6,2,-SIGN(1,IFLSTR))
      IDIPS=0
      CALL ARBOOD
      CALL ARCRDI(1,1,2,1,.FALSE.)
      CALL ARCOLI(1,-1)
      ISTRS=1
      IFLOW(1)=SIGN(1,IFLSTR)
      IPF(1)=1
      IPL(1)=2
      IMF=5
      IML=N
      QDUMP=.FALSE.
      NSAV=N+1

      IRQ=0
      IRD=2
      IRP=0
      IF (N.GT.6) IRP=7

C...Check if Lepto has run LUPREP
      DO 100 I=7,N
        IF (K(I,2).EQ.91) THEN
          DO 110 J=K(I,4),K(I,5)
            K(J,1)=K(J,1)+10
 110      CONTINUE
        ENDIF
 100  CONTINUE

      CALL ARREMN(2,IRQ,IRD,IRP,-1)

      IF (PHAR(112).LT.0.0) THEN
        PHAR(112)=-REAL(XPMU(IRD)**2)
        IF (MHAR(107).EQ.4) PHAR(112)=-XQ2
      ENDIF

      IF (IRP.LE.0.OR.MSTA(32).GT.1) THEN
        ALPNEW=ARALPQ(XPA(IRD),ARMAS2(1,IRD),DBLE(XQ2),XPMU(IRD),QSOFT)
        IF (.NOT.QSOFT) THEN
          XPA(IRD)=ALPNEW
          XPA(1)=0.0D0
          XPMU(1)=0.0D0
          QEX(1)=.FALSE.
          IF (IRP.GT.0) XPA(IRP)=
     $         ARALPQ(XPA(IRP),ARMAS2(1,IRP),DBLE(XQ2),XPMU(IRP),QSOFT)
        ENDIF
        LST(24)=1
        PT2LST=DBLE(PARA(40))
        CALL ARCASC
        GOTO 900
      ENDIF

C...Initiate initial g->QQ splitting
      PT2BGF=DBLE(PARA(40))
      IF (MSTA(9).GT.0) CALL ARCHEM(1)
      IO=0
      LST(24)=1

      PT2LST=DBLE(PARA(40))
      PT2MIN=ARGPT2(1)/DBLE(PHAR(103))
 210  IF (ABS(MSTA(33)).GT.0) THEN
        CALL ARPTQQ(K(2,2),IFLSTR,SQRT(DBLE(W2)),
     $       PT2BGF,PT2MIN,DBLE(X),DBLE(XQ2),DBLE(XY),XP,ZQ,YQ,PHI)
      ELSE
        CALL ARPTQQ(K(2,2),IFLSTR,SQRT(DBLE(W2)),
     $       PT2BGF,PT2MIN,DBLE(X),DBLE(XQ2),1.0D0,XP,ZQ,YQ,PHI)
      ENDIF
      IF (PT2BGF.GT.PT2MIN) THEN
        CALL ARINQQ(2,IFLSTR,IRP,PT2BGF,YQ,ZQ,PHI,QFAIL)
        IF (QFAIL) GOTO 210
        LST(24)=3
        CALL AREVOL(SQRT(DBLE(PHAR(103))*PT2BGF),0.0D0)
      ELSE
        CALL AREVOL(SQRT(PT2LST),0.0D0)
        IF (IO.GT.0) LST(24)=2
      ENDIF

C...Check momentum and dump to /LUJETS/
      IF (.NOT.QDUMP) CALL ARDUMP
      IF (MSTA(9).GT.0) CALL ARCHEM(0)
      GOTO 900

C...Include Phi asymmetries for matrix element
 900  IF (IO.GT.0.AND.ABS(MSTA(33)).EQ.1) CALL ARPHAS(NSAV)

      MSTA(33)=MSAV33
      CALL PYROBO(1,N,0.0D0,PHI2,0.0D0,0.0D0,0.0D0)
      CALL PYROBO(1,N,THEL,PHI1,DBXL,DBYL,DBZL)
        
      RETURN

C**** END OF ARLEPT ****************************************************
      END
