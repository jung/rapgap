C***********************************************************************
C $Id: arphas.f,v 3.11 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE ARPHAS(IFIRST)

C...ARiadne function PHi ASymmetry

C...Calculate a phi-angle to rotate en emission to  correctly treat
C...asymmetries in O(alpha_S)  ME for lepto production.

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'leptou.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'arint4.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'ardble.f'


      IF (IFLASS.EQ.0) RETURN

      PHI=PYANGL(BASS(1),BASS(2))+ARX2DB(PARU(1))
      CALL PYROBO(IFIRST,N,0.0D0,-PHI,0.0D0,0.0D0,0.0D0)


      BEPS1=1.0D0-DBLE(PARA(39))
      BEPS0=DBLE(PARA(39))
      B1=MIN(BASSX1,BEPS1)
      B3=MIN(BASSX3,BEPS1)
      B2=MIN(2.0D0-B1-B3,BEPS1)
      B12=1.0D0-B2

      SQ2=XQ2/W2
      XP=MAX(MIN(SQ2/(1.0D0-B3+SQ2),BEPS1),BEPS0)
      ZQ=MAX(MIN(B12/B3,BEPS1),BEPS0)


      IF (IFLASS.EQ.21) THEN
C.......Calculate guon asymmetry
        LST(24)=2
        SIGT=(ZQ**2+XP**2)/((1.0D0-XP)*(1.0D0-ZQ))+2.0D0*(XP*ZQ+1.0D0)
        SIGS=4.0D0*XP*ZQ
        SIG1=2.0D0*XY*SQRT((1.0D0-XY)*XP*ZQ/((1.0D0-XP)*(1.0D0-ZQ)))*
     $       (1.0D0-2.0D0/XY)*(1.0D0-ZQ-XP+2.0D0*XP*ZQ)
        SIG2=2.0D0*(1.0D0-XY)*XP*ZQ
      ELSE
C.......Calculate quark asymmetry
        LST(24)=3
        SIGT=(XP**2+(1.0D0-XP)**2)*(ZQ**2+(1.0D0-ZQ)**2)/(ZQ*(1.0D0-ZQ))
        SIGS=8.0D0*XP*(1.0D0-XP)
        SIG1=2.0D0*XY*SQRT((1.0D0-XY)*XP*(1.0D0-XP)/(ZQ*(1.0D0-ZQ)))
        SIG2=4.0D0*(1.0D0-XY)*XP*(1.0D0-XP)
      ENDIF

      SIG0=0.5D0*(1.0D0+(1.0D0-XY)**2)*SIGT+(1.0D0-XY)*SIGS

 100  PHI=PYR(0)*ARX2DB(PARU(2))
      IF (SIG0+COS(PHI)*SIG1+COS(2.0D0*PHI)*SIG2.LT.
     $     PYR(0)*(ABS(SIG0)+ABS(SIG1)+ABS(SIG2))) GOTO 100

      PHAR(123)=REAL(PHI)

      CALL PYROBO(IFIRST,N,0.0D0,PHI,0.0D0,0.0D0,0.0D0)
      
      RETURN

C**** END OF ARPHAS ****************************************************
      END
