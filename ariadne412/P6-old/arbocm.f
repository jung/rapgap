C***********************************************************************
C $Id: arbocm.f,v 3.10 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARBOCM(ID)

C...ARiadne subroutine BOost to Center of Mass

C...Boost the partons in dipole ID to the CMS of the dipole

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arint2.f'


C...Calculate boostvector and boost
      I1=IP1(ID)
      I3=IP3(ID)
      DPE1=BP(I1,4)
      DPE3=BP(I3,4)
      DPE=DPE1+DPE3
      DPX1=BP(I1,1)
      DPX3=BP(I3,1)
      DBEX=(DPX1+DPX3)/DPE
      DPY1=BP(I1,2)
      DPY3=BP(I3,2)
      DBEY=(DPY1+DPY3)/DPE
      DPZ1=BP(I1,3)
      DPZ3=BP(I3,3)
      DBEZ=(DPZ1+DPZ3)/DPE
      CALL AROBO2(0.0D0,0.0D0,-DBEX,-DBEY,-DBEZ,I1,I3)

C...Calculate rotation angles but no need for rotation yet
      PX=BP(I1,1)
      PY=BP(I1,2)
      PZ=BP(I1,3)
      PHI=PYANGL(PX,PY)
      THE=PYANGL(PZ,SQRT(PX**2+PY**2))

      RETURN

C**** END OF ARBOCM ****************************************************
      END
C***********************************************************************
C $Id: arbocm.f,v 3.10 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARBCM3(I1,I2,I3,THE,PHI1,PHI3,DBEX,DBEY,DBEZ)

C...ARiadne subroutine Boost to Center of Mass of 3 partons


C...Boost the three given partons to their CMS with I1 along the z-axis
C...and I2 in the xz-plane. Return the parameters needed for the boost.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'


C...Calculate boostvector and boost
      DPE1=BP(I1,4)
      DPE2=BP(I2,4)
      DPE3=BP(I3,4)
      DPE=DPE1+DPE2+DPE3
      DPX1=BP(I1,1)
      DPX2=BP(I2,1)
      DPX3=BP(I3,1)
      DBEX=(DPX1+DPX2+DPX3)/DPE
      DPY1=BP(I1,2)
      DPY2=BP(I2,2)
      DPY3=BP(I3,2)
      DBEY=(DPY1+DPY2+DPY3)/DPE
      DPZ1=BP(I1,3)
      DPZ2=BP(I2,3)
      DPZ3=BP(I3,3)
      DBEZ=(DPZ1+DPZ2+DPZ3)/DPE
      CALL AROBO3(0.0D0,0.0D0,-DBEX,-DBEY,-DBEZ,I1,I2,I3)

C...Calculate rotation angles but no need for rotation yet
      DPX1=BP(I1,1)
      DPY1=BP(I1,2)
      DPZ1=BP(I1,3)
      PHI1=PYANGL(DPX1,DPY1)
      THE=PYANGL(DPZ1,SQRT(DPX1**2+DPY1**2))
      CALL AROBO3(0.0D0,-PHI1,0.0D0,0.0D0,0.0D0,I1,I2,I3)
      CALL AROBO3(-THE,0.0D0,0.0D0,0.0D0,0.0D0,I1,I2,I3)
      DPX3=BP(I3,1)
      DPY3=BP(I3,2)
      PHI3=PYANGL(DPX3,DPY3)
      CALL AROBO3(0.0D0,-PHI3,0.0D0,0.0D0,0.0D0,I1,I2,I3)

      RETURN

C**** END OF ARBOCM ****************************************************
      END
C***********************************************************************
C $Id: arbocm.f,v 3.10 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARBOLE(THEL,PHI1,PHI2,DBXL,DBYL,DBZL)

C...ARiadne subroutine BOost to hadronic center of mass of LEpto event

C...Boost partons to the hadronic CMS of a LEPTO event


      INCLUDE 'arimpl.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'ardble.f'


      DBXL=0.0D0
      DBYL=0.0D0
      DBZL=0.0D0
      DBEL=0.0D0
      THEL=0.0D0
      PHI1=0.0D0
      PHI2=0.0D0

      DO 100 I=5,N
        DBXL=DBXL+ARX2DB(P(I,1))
        DBYL=DBYL+ARX2DB(P(I,2))
        DBZL=DBZL+ARX2DB(P(I,3))
        DBEL=DBEL+ARX2DB(P(I,4))
 100  CONTINUE

      DBXL=DBXL/DBEL
      DBYL=DBYL/DBEL
      DBZL=DBZL/DBEL

      CALL PYROBO(1,N,0.0D0,0.0D0,-DBXL,-DBYL,-DBZL)
      P31=ARX2DB(P(3,1))
      P32=ARX2DB(P(3,2))
      P33=ARX2DB(P(3,3))
      PHI1=PYANGL(P31,P32)
      THEL=PYANGL(P33,SQRT(P31**2+P32**2))
        
      CALL PYROBO(1,N,0.0D0,-PHI1,0.0D0,0.0D0,0.0D0)
      CALL PYROBO(1,N,-THEL,0.0D0,0.0D0,0.0D0,0.0D0)

      P11=ARX2DB(P(1,1))
      P12=ARX2DB(P(1,2))
      PHI2=PYANGL(P11,P12)
      CALL PYROBO(1,N,0.0D0,-PHI2,0.0D0,0.0D0,0.0D0)

      RETURN

C**** END OF ARBOLE ****************************************************
      END

C***********************************************************************
C $Id: arbocm.f,v 3.10 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARBOPY(THEPY,PHIPY,DBXPY,DBYPY,DBZPY,PHI2PY)

C...ARiadne subroutine BOost to center of mass of PYthia event

C...Boost partons to the total CMS of a PYTHIA event


      INCLUDE 'arimpl.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'pypars.f'
      INCLUDE 'leptou.f'

      DOUBLE PRECISION DBP(4)

      INCLUDE 'ardble.f'


      ISUB=MSTI(1)
      DO 10 J=1,4
        DBP(J)=0.0D0
 10   CONTINUE
     
      IF ((ISUB.EQ.10.OR.ISUB.EQ.83).AND.
     $     ABS(MSTI(15)).GE.11.AND.ABS(MSTI(15)).LE.18.AND.
     $     ABS(MSTI(16)).GE.1.AND.ABS(MSTI(16)).LE.8) THEN

        DO 100 I=MSTI(4),N
          IF (K(I,3).NE.2.AND.K(I,3).NE.MSTI(8)) GOTO 100
          DO 110 J=1,4
            DBP(J)=DBP(J)+ARX2DB(P(I,J))
 110      CONTINUE
 100    CONTINUE

        IQ=-2
        IL=MSTI(7)

        DBXPY=DBP(1)/DBP(4)
        DBYPY=DBP(2)/DBP(4)
        DBZPY=DBP(3)/DBP(4)

        X=ARX2RL(PARI(34))
        XQ2=-ARX2RL(PARI(15))
        W2=REAL(DBP(4)**2-DBP(3)**2-DBP(2)**2-DBP(1)**2)
        U=ARX2RL(P(2,4)*(P(IL-2,4)-P(IL,4))-P(2,3)*(P(IL-2,3)-P(IL,3))-
     $       P(2,2)*(P(IL-2,2)-P(IL,2))-P(2,1)*(P(IL-2,1)-P(IL,1)))
        XY=U/ARX2RL(P(1,4)*P(2,4)-P(1,3)*P(2,3)-
     $       P(1,2)*P(2,2)-P(1,1)*P(2,1))

      ELSEIF ((ISUB.EQ.10.OR.ISUB.EQ.83).AND.
     $       ABS(MSTI(16)).GE.11.AND.ABS(MSTI(16)).LE.18.AND.
     $       ABS(MSTI(15)).GE.1.AND.ABS(MSTI(15)).LE.8) THEN

        DO 200 I=MSTI(4)+1,N
          IF (K(I,3).NE.1.AND.K(I,3).NE.MSTI(7)) GOTO 200
          DO 210 J=1,4
            DBP(J)=DBP(J)+ARX2DB(P(I,J))
 210      CONTINUE
 200    CONTINUE

        IQ=1
        IL=MSTI(8)

        DBXPY=DBP(1)/DBP(4)
        DBYPY=DBP(2)/DBP(4)
        DBZPY=DBP(3)/DBP(4)

        X=ARX2RL(PARI(33))
        XQ2=-ARX2RL(PARI(15))
        W2=REAL(DBP(4)**2-DBP(3)**2-DBP(2)**2-DBP(1)**2)
        U=ARX2RL(P(1,4)*(P(IL-2,4)-P(IL,4))-P(1,3)*(P(IL-2,3)-P(IL,3))-
     $       P(1,2)*(P(IL-2,2)-P(IL,2))-P(1,1)*(P(IL-2,1)-P(IL,1)))
        XY=U/ARX2RL(P(1,4)*P(2,4)-P(1,3)*P(2,3)-
     $       P(1,2)*P(2,2)-P(1,1)*P(2,1))

      ELSE

        DEPY=ARX2DB(P(1,4))+ARX2DB(P(2,4))
        DBXPY=(ARX2DB(P(1,1))+ARX2DB(P(2,1)))/DEPY
        DBYPY=(ARX2DB(P(1,2))+ARX2DB(P(2,2)))/DEPY
        DBZPY=(ARX2DB(P(1,3))+ARX2DB(P(2,3)))/DEPY

        IQ=1
        IL=0

        XQ2=-1.0D0

      ENDIF

      CALL PYROBO(1,N,0.0D0,0.0D0,-DBXPY,-DBYPY,-DBZPY)

      I=ABS(IQ)
      PI1=ARX2DB(P(I,1))
      PI2=ARX2DB(P(I,2))
      PI3=ARX2DB(P(I,3))
      PHIPY=PYANGL(PI1,PI2)
      THEPY=PYANGL(PI3,SQRT(PI1**2+PI2**2))
      IF (IQ.LT.0) THEPY=ARX2DB(PARU(1))+THEPY
        
      CALL PYROBO(1,N,0.0D0,-PHIPY,0.0D0,0.0D0,0.0D0)
      CALL PYROBO(1,N,-THEPY,0.0D0,0.0D0,0.0D0,0.0D0)

      PHI2PY=0.0D0
      IF (IL.GT.0) THEN
        PIL1=ARX2DB(P(IL,1))
        PIL2=ARX2DB(P(IL,2))
        PHI2PY=PYANGL(PIL1,PIL2)
        CALL PYROBO(1,N,0.0D0,-PHI2PY,0.0D0,0.0D0,0.0D0)
      ENDIF

      RETURN

C**** END OF ARBOPY ****************************************************
      END
