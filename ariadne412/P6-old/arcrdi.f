C***********************************************************************
C $Id: arcrdi.f,v 3.2 1995/06/07 09:19:17 lonnblad Exp $

      SUBROUTINE ARCRDI(ID,IPA1,IPA3,IS,QED)

C...ARiadne subroutine CReate DIpole

C...Creates a dipole from partons IPA1 and IPA3

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'


      IDO(IPA1)=ID
      IDI(IPA3)=ID
      IP1(ID)=IPA1
      IP3(ID)=IPA3
      ISTR(ID)=IS
      QDONE(ID)=.FALSE.
      QEM(ID)=QED
      ICOLI(ID)=0

      RETURN

C**** END OF ARCRDI ****************************************************
      END
