C***********************************************************************
C $Id: aript2.f,v 3.9 2001/11/22 12:12:31 leif Exp $

      DOUBLE PRECISION FUNCTION ARIPT2(I1,I2,I3)

C...ARiadne function Invariant PT2

C...Returns the invariant p_t^2 of three partons

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardat1.f'


      ARIPT2=(ARMAS2(I1,I2)-(BP(I1,5)+BP(I2,5))**2)*
     $       (ARMAS2(I2,I3)-(BP(I2,5)+BP(I3,5))**2)/
     $        ARMAS3(I1,I2,I3)
      IF ( IFL(I2).EQ.21 ) RETURN
      IF ( MSTA(23).LE.1 ) RETURN
      IQB=I3
      IG=I1
      IF ( (IDO(I2).GT.0.AND.IDO(I2).EQ.IDI(I3)).OR.
     $     (IDI(I2).GT.0.AND.IDI(I2).EQ.IDO(I3))) THEN
        IQB=I1
        IG=I3
      ENDIF
      ARIPT2=(ARMAS2(IG,I2)-(BP(IG,5)+BP(I2,5))**2)*
     $       (ARMAS2(I2,IQB))/ARMAS3(I1,I2,I3)

      RETURN

C**** END OF ARIPT2 ****************************************************
      END
