C***********************************************************************
C $Id: argdis.f,v 3.19 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARGDIS(ID)

C...ARiadne subroutine Generate first Deep Inelastic Scattering emission

C...Generates a p-t^2 for a possible emission from an original dipole in
C...a DIS scattering according to O(alpha_S) matrix elements

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'leptou.f'
      INCLUDE 'ardble.f'


C...Copy some information from dipole record
C...S      = the invariant mass squared
C...W      = total energy in dipole
C...XT2MP  = maximum allowed fractional p_t^2 (x_t^2) for restricted  
C...         phase space option
C...QQ1(3) = Boolean variable 'is quark' for parton 1(3)
C...QE1(3) = true if parton 1(3) is extended
C...ALP1(3)= alpha parameter of parton 1(3)
C...XMU1(3)= mu parameter of parton 1(3)
C...SY1(3) = fractional mass of parton 1(3)
      PT2IN(ID)=0.0D0
      S=SDIP(ID)
      IF (S.LE.4.0D0*DBLE(PARA(3))**2) RETURN
      W=SQRT(S)
      QQ1=QQ(IP1(ID))
      QQ3=QQ(IP3(ID))
      QE1=QEX(IP1(ID))
      QE3=QEX(IP3(ID))
      ALP1=XPA(IP1(ID))
      ALP3=XPA(IP3(ID))
      XMU1=XPMU(IP1(ID))
      XMU3=XPMU(IP3(ID))
      SY1=BP(IP1(ID),5)/W
      SY3=BP(IP3(ID),5)/W
      IFL1=IFL(IP1(ID))
      IFL3=IFL(IP3(ID))
      IF (PARA(19).LT.0.0) CALL ARPRGC(ID)

      IF (S.LT.4.0D0*DBLE(PARA(3))**2) RETURN

      IF (MSTA(36).EQ.-1) XMU3=DBLE(PARA(11))

C...XLAM = scaled lambda_QCD squared
      XLAM2=DBLE(PARA(1))**2/S

C...alpha_0 for alpha_QCD = alpha_0/ln(p_t^2/lambda_QCD^2)
      XNUMFL=MAX(ARNOFL(W,MAX(5,MSTA(15))),3.0D0)
      ALPHA0=12.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)

C...Set Q^2 and Y dependencies for veto algorithm
      ZSQEV=S/W2
      SQ2=XQ2/W2
      YFAC=2.0D0*(1.0D0-XY)/(1.0D0+(1.0D0-XY)**2)

C...Call veto algorithm
      CALL ARGDIG(ID)


      RETURN

C**** END OF ARGDIS ****************************************************
      END
C***********************************************************************
C $Id: argdis.f,v 3.19 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARGDIG(ID)

C...ARiadne subroutine Generate first Deep Inelastic Scattering emission

C...Generates a p-t^2 for a possible emission from an original dipole in
C...a DIS scattering according to O(alpha_S) matrix elements

      INCLUDE 'arimpl.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'pydat1.f'

      EXTERNAL ARNDX1,ARNDX2,ARNDY2,ARVET6,ARVET7
      DOUBLE PRECISION ARNDX1,ARNDX2,ARNDY2,ARVET6,ARVET7

      INCLUDE 'ardble.f'


C...First gluon emission
      SY2=0.0D0
      IFLG=0

C...Calculate mass dependent parameters
      CALL ARMADE

C...C = colour factors etc. in cross section
      C=2.0D0*(3.0D0+4.0D0*YFAC/27.0D0)/(3.0D0*ARX2DB(PARU(1)))
      IF (MHAR(116).LE.0) THEN
        C=2.0D0*(6.0D0+0.25D0*YFAC)/(3.0D0*ARX2DB(PARU(1)))
      ENDIF
      IF (MHAR(152).NE.0) C=C*ARWGHT(0)

C...Minimum x_t^2
      XT2C=MAX(PT2IN(ID),DBLE(PARA(3))**2)/S
      XT2=0.0D0

      IF (XT2M.LE.XT2C) GOTO 900

C...Set additional parameters and call the veto algorith with
C...Suitable random functions
      IF (MSTA(12).GT.0) THEN
C.......Running alpha_QDC
        YINT=2.0D0*LOG(0.5D0/SQRT(XLAM2)+SQRT(0.25D0/XLAM2-1.0D0))
        CN=1.0D0/(YINT*C*ALPHA0)
        CALL ARMCDI(ARNDX1,ARNDY2,ARVET6)
      ELSE
C.......Constant alpha_QCD
        YINT=1.0D0
        CN=2.0D0/(C*DBLE(PARA(2)))
        CALL ARMCDI(ARNDX2,ARNDY2,ARVET7)
      ENDIF

C...Save the generated values of p_t^2, x1, x3, a1 and a3
      IF (XT2.GT.XT2C) THEN
        PT2IN(ID)=XT2*S
        BX1(ID)=B1
        BX3(ID)=B3
        AEX1(ID)=AE1
        AEX3(ID)=AE3
        IRAD(ID)=0
      ENDIF

 900  CONTINUE


      RETURN

C**** END OF ARGDIG ****************************************************
      END
