      PROGRAM HERA


C...Initialize parameters in Ariadne and Jetset
      CALL ARTUNE('4.11')

C...Call a user supplied routine setting
C...the parameters and switches in LEPTO
      CALL LEPSET

C...Call a user supplied routine setting
C...the parameters and switches in Ariadne
      CALL ARISET


C...Initialize Ariadne to run with LEPTO
      CALL ARINIT('LEPTO')


C...Initialize LEPTO for HERA
      CALL LINIT(0,11,-26.5,820.0,4)


C...Loop over a number of events
      DO 100 IEVE=1,10


C...Call generate an event with LEPTO
        CALL LEPTO


C...Apply the Dipole Cascade
        CALL AREXEC


C...Call a user supplied analysis routine
        CALL HERANA


 100  CONTINUE


      END

      SUBROUTINE LEPSET

      INCLUDE 'arimpl.f'
      INCLUDE 'leptou.f'

C...Use structure funtions from Pythia
      LST(15)=0

C...Suppress printouts from LEPTO
      LST(3)=1

      RETURN
      END

      SUBROUTINE ARISET

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'

      MSTA(6)=1

      RETURN
      END

      SUBROUTINE HERANA

C...Trivial analysis - print the event
      CALL PYLIST(1)

      RETURN
      END

      SUBROUTINE DUMMY9
C...This is a dummy routine to ensure the relevant block data routines
C...are linked from the JETSET/PYTHIA and LEPTO libraries. This routine
C...should never be called.

      CALL LUDATA
      CALL PYDATA
      CALL LEPTOD

      RETURN
      END
