C***********************************************************************
C $Id: argtyp.f,v 3.4 2000/03/08 21:25:39 leif Exp $

      SUBROUTINE ARGTYP(I,ITYP)

C...ARiadne subroutine Get TYpe of Particle

C...Determines the type of particle I according to ITYP=2: gluon,
C...ITYP=1: quark or anti-di-quark, ITYP=-1: anti-quark or di-quark,
C...ITYP=0: other.

      INCLUDE 'arimpl.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat2.f'

      INTEGER PYCOMP

      ITYP=KCHG(PYCOMP(K(I,2)),2)*ISIGN(1,K(I,2))

      RETURN

C**** END OF ARGTYP ****************************************************
      END
