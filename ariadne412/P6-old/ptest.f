      PROGRAM LHC

C...Hack to allow the same program to be used with old and new
C...versions of Pythia.
      INCLUDE 'pyjets.f'
      V(1,1)=17000.0

C...Initialize parameters in Ariadne and Jetset
      CALL ARTUNE('4.11')

C...Call a user supplied routine setting 
C...the parameters and switches in PYTHIA
      CALL PYTSET

C...Call a user supplied routine setting 
C...the parameters and switches in Ariadne
      CALL ARISET

C...Initialize Ariadne to run with PYTHIA
      CALL ARINIT('PYTHIA')

C...Initialize PYTHIA for LHC
      CALL PYINIT('CMS','p+','p+',V(1,1))

C...Loop over a number of events
      DO 100 IEVE=1,1000

C...Call generate an event with PYTHIA
        CALL PYEVNT

C...Apply the Dipole Cascade
        CALL AREXEC

C...Call a user supplied analysis routine
        IF (IEVE.LE.10) CALL LHCANA

100   CONTINUE

      END

      SUBROUTINE PYTSET

      INCLUDE 'arimpl.f'
      INCLUDE 'pysubs.f'
      INCLUDE 'pypars.f'


C...Select QCD high-p_T processes
      MSEL=1

C...Set cutoff in p_T-hat
      CKIN(3)=10.0D0

C...Switch off fragmentation
      MSTP(111)=0

      RETURN
      END

      SUBROUTINE ARISET

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'

C...Set up fragmentation production of Quarkonium:
C...Mastrer switch for Quarkonium production
      MSTA(38)=2

C...Switch on Colour Octet production of g->J/psi with Octet matrix
C...element = 0.015 GeV^3 (multiplied by 1000 to get reasonable
C...statistics) and the scaled velocity of the charm within
C...the J/psi = 0.45.
      CALL ARADDO(21,443,3,.FALSE.,0.015D0*1000.0D0,0.45D0,0.0D0,0.0D0, 
     & 0.0D0)

C...Switch on Colour Singlet production of g->J/psi+2g with Singlet
C...parameter |R(0)|^2 = 0.512 GeV^3.
C      CALL ARADDO(21,443,1,.TRUE.,0.512D0,0.0D0,0.0D0,0.0D0,0.0D0)

C...Switch on Colour Singlet production of c->J/psi+c with Singlet
C...parameter |R(0)|^2 = 0.512 GeV^3.
C      CALL ARADDO(4,443,41,.FALSE.,0.512D0,0.0D0,0.0D0,0.0D0,0.0D0)

      RETURN
      END

      SUBROUTINE LHCANA

C...Trivial analysis - print the event
      CALL PYLIST(1)

      RETURN
      END

      SUBROUTINE DUMMY9
C...This is a dummy routine to ensure the relevant block data routines
C...are linked from the JETSET/PYTHIA library. This routine should never
C...be called.

      CALL PYDATA
      CALL LUDATA

      RETURN
      END
