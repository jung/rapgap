C***********************************************************************
C $Id: aralps.f,v 3.2 2001/10/21 19:36:49 leif Exp $

      DOUBLE PRECISION FUNCTION ARALPS(PT2,STOT)

C...ARiadne function ALPha_S

C...Returns alpha_s for a given scale.

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'ardat2.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'ardble.f'


      ARALPS=DBLE(PARA(2))
      IF ( MSTA(12).EQ.0 ) RETURN

C...alpha_0 for alpha_QCD = alpha_0/ln(p_t/lambda_QCD)
      PT=SQRT(MAX(PT2,DBLE(PARA(3))**2))
      XNUMFL=MAX(ARNOFL(SQRT(STOT),MAX(5,MSTA(15))),3.0D0)
      ALPHA0=6.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)
      IF ( MSTA(12).EQ.1.OR.XNUMFL.LT.3.5D0 ) THEN
        ARALPS=ALPHA0/LOG(PT/DBLE(PARA(1)))
        RETURN
      ENDIF

      XLAM4=PQMAS(4)*(DBLE(PARA(1))/PQMAS(4))**(27.0D0/25.0D0)
      IF ( XNUMFL.GT.4.5D0 ) THEN
        XLAM5=PQMAS(5)*(XLAM4/PQMAS(4))**(25.0D0/23.0D0)
        ARALPS=ALPHA0/LOG(PT/XLAM5)
      ELSE
        ARALPS=ALPHA0/LOG(PT/XLAM4)
      ENDIF

      RETURN

C**** END OF ARALPS ****************************************************
      END
