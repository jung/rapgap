C***********************************************************************
C $Id: arwght.f,v 3.2 2000/02/15 14:02:42 leif Exp $

      DOUBLE PRECISION FUNCTION ARWGHT(IRAD)

C...ARiadne dummy routine user WeiGHTs

C...Enables a user to preweight the emissions of kind IRAD with a given
C...factor. The user routine ARUTHW can then be used to throw away
C...emissions to obtain a costumized distribution.

      INCLUDE 'arimpl.f'


      ARWGHT=1.0D0

      RETURN

C**** END OF ARWGHT ****************************************************
      END
