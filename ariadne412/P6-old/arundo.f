C***********************************************************************
C $Id: arundo.f,v 3.1 2001/06/15 12:11:54 leif Exp $

      SUBROUTINE ARUNDO(I1I,I2I,I3I)

C...ARiadne function UNDO

C...Try to undo a previous emission fixing up recoils etc.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'ardble.f'


      I1=I1I
      I2=I2I
      I3=I3I
      S=ARMAS3(I1,I2,I3)
C...Boost to CM frame orient everything according to most likely recoil
      CALL ARBCM3(I1,I2,I3,THE,PHI1,PHI3,DBEX,DBEY,DBEZ)
      ALP=ARANGL(I1,I3)
      B1=BP(I1,4)
      B3=BP(I3,4)
      PSI=0
      IF ( IFL(I2).EQ.21 ) THEN
        IF ( (IFL(I1).EQ.21.AND.IFL(I3).EQ.21).OR.
     $       (IFL(I1).NE.21.AND.IFL(I3).NE.21).OR.MHAR(166).EQ.2 ) THEN
          PSI=(ARX2DB(PARU(1))-ALP)*(B3**2)/(B1**2+B3**2)
        ELSEIF ( IFL(I3).EQ.21 )  THEN
          PSI=(ARX2DB(PARU(1))-ALP)
        ENDIF
      ELSE
        BP(I3,5)=0.0D0
      ENDIF
      CALL AROBO3(PSI,0.0D0,0.0D0,0.0D0,0.0D0,I1,I2,I3)

C...Set momenta of the radiating partons
      BP(I1,1)=0.0D0
      BP(I1,2)=0.0D0
      BP(I1,3)=ARZCMS(S,BP(I1,5),BP(I3,5))
      BP(I1,4)=SQRT(BP(I1,1)**2+BP(I1,2)**2+BP(I1,3)**2+BP(I1,5)**2)
      BP(I3,1)=0.0D0
      BP(I3,2)=0.0D0
      BP(I3,3)=-BP(I1,3)
      BP(I3,4)=SQRT(BP(I3,1)**2+BP(I3,2)**2+BP(I3,3)**2+BP(I3,5)**2)

C...Now boost back everything
      CALL AROBO2(-PSI,0.0D0,0.0D0,0.0D0,0.0D0,I1,I3)
      CALL AROBO2(0.0D0,PHI3,0.0D0,0.0D0,0.0D0,I1,I3)
      CALL AROBO2(THE,PHI1,DBEX,DBEY,DBEZ,I1,I3)

C...Remove the radiated parton
      IF ( IFL(I2).EQ.21 ) THEN
        CALL ARREMG(I2)
      ELSE
        CALL ARJOQQ(I2,I3)
      ENDIF

      RETURN

C**** END OF ARUNDO ****************************************************
      END
