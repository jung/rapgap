C***********************************************************************
C $Id: arumec.f,v 3.2 2001/11/09 17:33:08 leif Exp $

      DOUBLE PRECISION FUNCTION ARUMEC(IN1,IN2)

C...ARiadne dummy routine User Matrix Element Cut/

C...Produce an error message if user defined function for matrix element
C...cut has not been linked

C...This should implement the exact kinematical cuts used in an
C...external matrix element routine

      INCLUDE 'arimpl.f'


      CALL ARERRM('ARUMEC',24,0)

      ARUMEC=0.0D0

      RETURN

C**** END OF ARUPOM ****************************************************
      END
