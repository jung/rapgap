C***********************************************************************
C $Id: arx3jt.f,v 3.4 2001/11/10 02:03:32 leif Exp $

      SUBROUTINE ARX3JT(NJET,CUT,KFL,ECM,X1,X3)

C...ARiadne subroutine construct kinematics for 3 JeT

C...Construct kinematics for a three-jet event according to first
C...order matrix element.

      INCLUDE 'arimpl.f'
      INCLUDE 'arhide.f'
      INCLUDE 'ardat1.f'


      NJET=2
      PMQ=PYMASS(KFL)
      XM=PMQ/ECM

      IF ( MHAR(160).EQ.1 ) THEN
        XKAMAX=-LOG(CUT)
        XKAPPA=XKAMAX*(1.0D0-SQRT(PYR(0)))
        Y=0.5D0*(XKAPPA-XKAMAX)+PYR(0)*(XKAMAX-XKAPPA)
        XT=EXP(0.5D0*(XKAPPA-XKAMAX))
        X1=1.0D0-XT*EXP(Y)
        X3=1.0D0-XT*EXP(-Y)
      ELSE
        X1=1.0D0-CUT**PYR(0)
        X3=1.0D0-CUT**PYR(0)
      ENDIF
      X2=2.0D0-X1-X3
      E1=0.5D0*X1
      E2=0.5D0*X2
      E3=0.5D0*X3
      P12=E1**2-XM**2
      P22=E2**2
      P32=E3**2-XM**2
      IF (E1.LT.XM.OR.E3.LT.XM.OR.
     $    2.0D0*(P12*P22+P22*P32+P32*P12).LE.(P12**2+P22**2+P32**2).OR.
     $    X1**2+X3**2.LT.2.0D0*PYR(0)) RETURN
      NJET=3

      RETURN

C**** END OF ARX3JT ****************************************************
      END

C*********************************************************************

C...PYX3JT
C...Selects the kinematical variables of three-jet events.

      SUBROUTINE ARXJT3(NJET,CUT,KFL,ECM,X1,X2)

C...Double precision and integer declarations.
      IMPLICIT DOUBLE PRECISION(A-H, O-Z)
      IMPLICIT INTEGER(I-N)
C...Commonblocks.
      COMMON/PYDAT1/MSTU(200),PARU(200),MSTJ(200),PARJ(200)
      SAVE /PYDAT1/

C...Event type. Mass effect factors and other common constants.
      PMQ=PYMASS(KFL)
      QME=(2D0*PMQ/ECM)**2
      CUTL=LOG(CUT)
      CUTD=LOG(1D0/CUT-2D0)
      CF=4D0/3D0
      CN=3D0
      TR=2D0
      WTMX=MIN(20D0,37D0-6D0*CUTD)

C...Choose three-jet events in allowed region.
      NJET=3
      Y13L=CUTL+CUTD*PYR(0)
      Y23L=CUTL+CUTD*PYR(0)
      Y13=EXP(Y13L)
      Y23=EXP(Y23L)
      Y12=1D0-Y13-Y23
      IF(Y12.LE.CUT) GOTO 999
      IF(Y13**2+Y23**2+2D0*Y12.LE.2D0*PYR(0)) GOTO 999

C...Impose mass cuts (gives two jets). For fixed jet number new try.
      X1=1D0-Y23
      X2=1D0-Y13
      X3=1D0-Y12
      IF(4D0*Y23*Y13*Y12/X3**2.LE.QME) GOTO 999
      IF(MOD(MSTJ(103),4).GE.2.AND.QME*X3+
     &     0.5D0*QME**2+(0.5D0*QME+0.25D0*QME**2)*((1D0-X2)/(1D0-X1)+
     &     (1D0-X1)/(1D0-X2)).GT.(X1**2+X2**2)*PYR(0)) GOTO 999

      RETURN

 999  NJET=2

      RETURN
      END
