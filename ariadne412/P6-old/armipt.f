C***********************************************************************
C $Id: armipt.f,v 3.6 2001/01/24 21:37:58 leif Exp $

      DOUBLE PRECISION FUNCTION ARMIPT(IFST,ILST)

C...ARiadne function determine MInimum PT2

C...Determines the minimum p_t^2 of any gluon between positions 
C...IF and IL.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'

      INXT(IP)=IP3(IDO(IP))
      IPRV(IP)=IP1(IDI(IP))


      ARMIPT=DBLE(PARA(40))
      DO 100 I=IFST,ILST
        IF (.NOT.QQ(I)) THEN
          IF (INXT(I).NE.IPRV(I))
     $         ARMIPT=MIN(ARMIPT,ARIPT2(IPRV(I),I,INXT(I)))
        ENDIF
 100  CONTINUE

      RETURN

C**** END OF ARMIPT ****************************************************
      END
