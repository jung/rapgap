      COMMON /ARDIPS/ BX1(MAXDIP),BX3(MAXDIP),PT2IN(MAXDIP),
     $                SDIP(MAXDIP),IP1(MAXDIP),IP3(MAXDIP),
     $                AEX1(MAXDIP),AEX3(MAXDIP),QDONE(MAXDIP),
     $                QEM(MAXDIP),IRAD(MAXDIP),ISTR(MAXDIP),
     $                ICOLI(MAXDIP),PTMX2(MAXDIP),IDIPS
      SAVE /ARDIPS/
C...Ede'n, Friberg, Gustafson, H\"akkinen extra DIpole fields
      COMMON /ARDIP2/ YGLU1(MAXDIP),YGLU3(MAXDIP),
     $                YG1,YG3,IGSIDE(MAXDIP),IGS
      SAVE /ARDIP2/
