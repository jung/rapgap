C***********************************************************************
C $Id: arrecs.f,v 3.7 2001/11/22 10:52:01 leif Exp $

      SUBROUTINE ARRECS(I1,I2,I3,PT2,NLOOP)

C...ARiadne function REConstruct Sequence

C...Reconstruct the most likely sequence of NLOOP emissions in the event
C...record

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'

      DIMENSION I1(5),I2(5),I3(5),PT2(5)
      DIMENSION ISL(3,MAXPAR),PT2I(MAXPAR),PROB(MAXPAR),SQQI(MAXPAR)
      DIMENSION ISLS(3,5,MAXPAR),PT2S(5,MAXPAR),PROBS(5,MAXPAR)
      DIMENSION ISEL(MAXPAR),SPROB(0:MAXPAR),SQQS(5,MAXPAR)


      I1(1)=0
      I2(1)=0
      I3(1)=0
      IF ( NLOOP.LE.0 ) RETURN

C...Reconstruct a first emission
      CALL ARRECO(ISL,PT2I,SQQI,PROB,NSL)

      DO 100 I=1,NSL
        ISLS(1,1,I)=ISL(1,I)
        ISLS(2,1,I)=ISL(2,I)
        ISLS(3,1,I)=ISL(3,I)
        PT2S(1,I)=PT2I(I)
        SQQS(1,I)=SQQI(I)
        IF ( MHAR(167).NE.0.AND.PT2I(I).LT.DBLE(PARA(3))**2)
     $       ISLS(1,1,I)=0
        PROBS(1,I)=PROB(I)
        DO 110 J=2,NLOOP
          PROBS(J,I)=0.0D0
 110    CONTINUE
 100  CONTINUE

      NS0=0
      NS=NSL
      NS1=NSL

C...Reconstruct subsequent emissions

      DO 200 ILOOP=2,NLOOP

        DO 210 IS=NS0+1,NS1
          IF (ISLS(1,1,IS).LE.0) GOTO 210
          DO 220 IL=1,ILOOP-1
            CALL ARUNDO(ISLS(1,IL,IS),ISLS(2,IL,IS),ISLS(3,IL,IS))
 220      CONTINUE
          CALL ARRECO(ISL,PT2I,SQQI,PROB,NSL)
          CALL ARGETR(10)
            
          DO 230 I=1,NSL
            NS=NS+1
            DO 240 IL=1,ILOOP-1
              ISLS(1,IL,NS)=ISLS(1,IL,IS)
              ISLS(2,IL,NS)=ISLS(2,IL,IS)
              ISLS(3,IL,NS)=ISLS(3,IL,IS)
              PT2S(IL,NS)=PT2S(IL,IS)
              PROBS(IL,NS)=PROBS(IL,IS)
 240        CONTINUE
            ISLS(1,ILOOP,NS)=ISL(1,I)
            ISLS(2,ILOOP,NS)=ISL(2,I)
            ISLS(3,ILOOP,NS)=ISL(3,I)
            PT2S(ILOOP,NS)=PT2I(I)
            SQQS(ILOOP,NS)=SQQI(I)
            IF ( MHAR(167).GE.4 ) THEN
              IF ( MHAR(167).GE.5.AND.PT2I(I).LT.PT2S(ILOOP-1,NS) )
     $             PROB(I)=PROB(I)*PT2I(I)/PT2S(ILOOP-1,NS)
              PT2S(ILOOP,NS)=MAX(PT2S(ILOOP,NS),PT2S(ILOOP-1,NS))
            ENDIF
            IF ( MHAR(167).GE.2.AND.PT2I(I).LT.DBLE(PARA(3))**2 )
     $           ISLS(1,1,NS)=0
            PROBS(ILOOP,NS)=PROBS(ILOOP-1,NS)*PROB(I)
 230      CONTINUE
 210    CONTINUE

        NS0=NS1
        NS1=NS

 200  CONTINUE

C...First try to find an ordered sequence of emissions
      NSEL=0
      SPROB(0)=0.0D0
      PT2MIN=DBLE(PARA(40))
      PT2MAX=0.0D0
      IPTMIN=0
      IPTMAX=0
      DO 300 IS=NS0+1,NS1
        IF ( ISLS(1,1,IS).LE.0 ) GOTO 300
        DO 310 IL=2,NLOOP
          IF ( PT2S(IL,IS).LE.PT2S(IL-1,IS) ) GOTO 300
          IF ( MSTA(28).NE.0.AND.SQQS(IL-1,IS).GT.0.0D0.AND.
     $         SQQS(IL-1,IS).GT.PT2S(IL,IS) ) GOTO 300
 310    CONTINUE
        IF ( PROBS(NLOOP,IS)+SPROB(NSEL).LE.SPROB(NSEL) ) GOTO 300
        NSEL=NSEL+1
        ISEL(NSEL)=IS
        SPROB(NSEL)=SPROB(NSEL-1)+PROBS(NLOOP,IS)
        IF ( PT2S(1,IS).LT.PT2MIN ) THEN
          PT2MIN=PT2S(1,IS)
          IPTMIN=NSEL
        ENDIF
        IF ( PT2S(1,IS).GT.PT2MAX ) THEN
          PT2MAX=PT2S(1,IS)
          IPTMAX=NSEL
        ENDIF
 300  CONTINUE

      IF ( MHAR(165).EQ.3.OR.NSEL.EQ.0.OR.SPROB(NSEL).LE.0.0D0 )
     $     GOTO 390
 340  P=SPROB(NSEL)*PYR(0)
      DO 320 I=1,NSEL
        IF ( (MHAR(164).LE.0.AND.P.LE.SPROB(I)).OR.
     $       (MHAR(164).EQ.1.AND.I.EQ.IPTMIN).OR.
     $       (MHAR(164).EQ.2.AND.I.EQ.IPTMAX) ) THEN
          DO 330 ILOOP=1,NLOOP
            I1(ILOOP)=ISLS(1,ILOOP,ISEL(I))
            I2(ILOOP)=ISLS(2,ILOOP,ISEL(I))
            I3(ILOOP)=ISLS(3,ILOOP,ISEL(I))
            PT2(ILOOP)=PT2S(ILOOP,ISEL(I))
 330      CONTINUE
          IF ( NSEL.GT.1.AND.MHAR(164).LT.0.AND.
     $         ARSUVE(I1,I2,I3,PT2,NLOOP).LT.0.0D0 ) GOTO 340
          RETURN
        ENDIF
 320  CONTINUE

C...Now check also unordered ones
 390  NSEL=0
      PT2MIN=DBLE(PARA(40))
      PT2MAX=0.0D0
      IPTMIN=0
      IPTMAX=0
      DO 400 IS=NS0+1,NS1
        IF ( ISLS(1,1,IS).LE.0 ) GOTO 400
        IF ( PROBS(NLOOP,IS)+SPROB(NSEL).LE.SPROB(NSEL) ) GOTO 400
        NSEL=NSEL+1
        ISEL(NSEL)=IS
        SPROB(NSEL)=SPROB(NSEL-1)+PROBS(NLOOP,IS)
        IF ( PT2S(1,IS).LT.PT2MIN ) THEN
          PT2MIN=PT2S(1,IS)
          IPTMIN=NSEL
        ENDIF
        IF ( PT2S(1,IS).GT.PT2MAX ) THEN
          PT2MAX=PT2S(1,IS)
          IPTMAX=NSEL
        ENDIF
 400  CONTINUE

      IF ( NSEL.EQ.0.OR.SPROB(NSEL).LE.0.0D0 ) RETURN
 440  P=SPROB(NSEL)*PYR(0)
      DO 410 I=1,NSEL
        IF ( (MHAR(164).LE.0.AND.P.LE.SPROB(I)).OR.
     $       (MHAR(164).EQ.1.AND.I.EQ.IPTMIN).OR.
     $       (MHAR(164).EQ.2.AND.I.EQ.IPTMAX) ) THEN
          DO 420 ILOOP=1,NLOOP
            I1(ILOOP)=ISLS(1,ILOOP,ISEL(I))
            I2(ILOOP)=ISLS(2,ILOOP,ISEL(I))
            I3(ILOOP)=ISLS(3,ILOOP,ISEL(I))
            PT2(ILOOP)=PT2S(ILOOP,ISEL(I))
 420      CONTINUE
          IF ( NSEL.GT.1.AND.MHAR(164).LT.0.AND.
     $         ARSUVE(I1,I2,I3,PT2,NLOOP).LT.0.0D0 ) GOTO 440
          RETURN
        ENDIF
 410  CONTINUE

      RETURN

C**** END OF ARRECS ****************************************************
      END
