C***********************************************************************
C $Id: arthrw.f,v 3.31 2001/11/21 19:24:32 leif Exp $

      DOUBLE PRECISION FUNCTION ARTHRW(ID,JRAD,I1,I3,IN1,IN2)

C...ARiadne subroutine THRoW emission

C...Signals emissions not fulfilling certain criteria

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint5.f'
      INCLUDE 'arhide.f'

      INXT(I)=IP3(IDO(I))
      IPRV(I)=IP1(IDI(I))


      ARTHRW=-1.0D0

      IF (PARA(28).NE.0.0) THEN
        GMAX=DBLE(PARA(40))
        GMIN=0.0D0
        IF (PARA(28).GT.0.0) THEN
          GMIN=DBLE(PARA(28))
        ELSE
          GMAX=-DBLE(PARA(28))
        ENDIF
        DO 100 IP=IN1,IN2
          IF (IFL(IP).EQ.21.AND.(BP(IP,4).GT.GMAX.OR.BP(IP,4).LT.GMIN))
     $         RETURN
 100    CONTINUE
      ENDIF

      IF (MSTA(32).LE.-3) THEN
        DO 200 IP=IN1,IN2
          ABOVE=ARABLI(IP)
          IF (MHAR(149).EQ.0) THEN
            IF ((MHAR(150).EQ.0.OR.IFL(IP).NE.21).AND.
     $           ABOVE.LT.1.0D0) RETURN
            IF (IFL(IP).EQ.21.AND.ABOVE.LT.PYR(0)) RETURN
          ELSE
            IF (ABOVE**DBLE(PARA(25)).LT.PYR(0)) RETURN
          ENDIF
 200    CONTINUE
        IF (JRAD.LT.0.AND.JRAD.GT.-9) THEN
          IF (ARABLI(I1).LT.1.0D0) RETURN
        ENDIF
        IF (JRAD.GT.0.AND.JRAD.LT.9) THEN
          IF (ARABLI(I3).LT.1.0D0) RETURN
        ENDIF
      ENDIF

      IF ( MHAR(180).EQ.1 ) THEN
        DO 300 IG=IN1,IN2
          IF ( IFL(IG).NE.21 ) GOTO 300
          IF ( IFL(INXT(IG)).EQ.21 ) THEN
            IF ( ARIPT2(IG,INXT(IG),INXT(INXT(IG))).LT.PT2LST ) RETURN
          ENDIF
          IF ( IFL(IPRV(IG)).EQ.21 ) THEN
            IF ( ARIPT2(IG,IPRV(IG),IPRV(IPRV(IG))).LT.PT2LST ) RETURN
          ENDIF
 300    CONTINUE
      ENDIF

      IF ( MHAR(167).GE.7 ) THEN
        IF ( ARPTCA().LT.0.0D0 ) RETURN
      ELSEIF ( MHAR(167).GE.6 ) THEN
C...Always cut on minimum invariant pt on anything
        DO 400 I=1,IPART
          DO 410 J=1,IPART
            IF ( J.EQ.I ) GOTO 410
            DO 420 K=J+1,IPART
              IF ( K.EQ.I) GOTO 420
              IF ( ARIPT2(J,I,K).LT.DBLE(PARA(3))**2 ) RETURN
 420        CONTINUE
 410      CONTINUE
 400    CONTINUE
      ENDIF

      IF ( ARUTHR(ID,JRAD,I1,I3,IN1,IN2).LT.0.0D0 ) RETURN

      ARTHRW=1.0D0

      RETURN

C**** END OF ARTHRW ****************************************************
      END
C***********************************************************************
C $Id: arthrw.f,v 3.31 2001/11/21 19:24:32 leif Exp $

      DOUBLE PRECISION FUNCTION ARABLI(IP)

C...ARiadne subroutine ABove initial state LInks

C...Determine how much a given particle IP is above the phase space
C...limits given by the initial linked dipole chain as given in
C.../ARINT5/

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint5.f'
      INCLUDE 'arhide.f'

      DIMENSION DMT2LL(0:MAXPAR),DMT2LU(0:MAXPAR)

      DRAT(DY,DY0,DY1,DMT2,DMT20,DMT21)=
     $     DMT20*EXP(LOG(DMT21/DMT20)*(DY-DY0)/(DY1-DY0))/DMT2


      IF (MSTA(32).EQ.-33) THEN
        ARABLI=ARABL2(IP)
        RETURN
      ENDIF

      ARABLI=0.0D0
      DPP=BP(IP,4)+BP(IP,3)
      DPM=BP(IP,4)-BP(IP,3)
      DMT2=MIN(DPP*DPM,DBLE(PT2LST))
      IF (MHAR(148).GT.0) DMT2=DPP*DPM
      IF (MHAR(148).LT.0) DMT2=DBLE(PT2LST)
      DCUT2=DKY(NLINKS*4+2)
      DCUT=SQRT(DCUT2)
      DKAPPA=LOG(DMT2/DCUT2)
      DY=0.5D0*LOG(DPP/DPM)

      DMT2LL(0)=DKY(2)
      DMT2LU(0)=DKY(2)

      DO 10 I=1,NLINKS-1
        DMT2LL(I)=MAX(DKY(4*I+2),DKY(4*I-3))
        DMT2LU(I)=MAX(DKY(4*I+2),DKY(4*I+1))
        IF (ABS(MHAR(148)).EQ.1) THEN
          DMT2LL(I)=DKY(4*I-3)
          DMT2LU(I)=DKY(4*I+1)
        ELSEIF (ABS(MHAR(148)).EQ.2) THEN
          DMT2LL(I)=DKY(4*I+2)
          DMT2LU(I)=DKY(4*I+2)
        ENDIF
 10   CONTINUE
      DMT2LL(NLINKS)=DCUT2
      DMT2LU(NLINKS)=DCUT2

      ISEL=0
      A=2.0D0
      DY0=LOG(DKY(3)/DCUT)
      DY2=0.5D0*LOG(DKY(3)**2/DKY(2))
      IF (DY.GT.DY0) THEN
        A=DCUT2/DMT2
        GOTO 900
      ENDIF
      IF (DY.LE.DY0.AND.DY.GT.DY2) THEN
        A=DRAT(DY,DY0,DY2,DMT2,DCUT2,DMT2LU(0))
        GOTO 900
      ENDIF

      DO 100 I=1,NLINKS
        DY0=DY2
        DY1=0.5D0*LOG((DKY(4*I-1)**2)/DKY(4*I+2))-
     $       MAX(LOG(DKY(4*I-2)/DKY(4*I+2)),0.0D0)
        DY2=0.5D0*LOG((DKY(4*I+3)**2)/DKY(4*I+2))
        IF (DY.LE.DY0.AND.DY.GT.DY1) THEN
          A=DRAT(DY,DY0,DY1,DMT2,DMT2LU(I-1),DMT2LL(I))
          ISEL=I
          IF (DKY(4*I+2).GT.DKY(4*I-2)) ISE1=I-1
          GOTO 900
        ENDIF
        IF (DY.LE.DY1.AND.DY.GT.DY2) THEN
          A=DRAT(DY,DY1,DY2,DMT2,DMT2LL(I),DMT2LU(I))
          ISEL=I
          GOTO 900
        ENDIF
 100  CONTINUE

 900  IF (A.LT.1.0D0.AND.IFLL(ISEL+1).EQ.0.AND.MHAR(149).EQ.0) A=0.0D0
      ARABLI=A

      RETURN

C**** END OF ARABLI ****************************************************
      END
C***********************************************************************
C $Id: arthrw.f,v 3.31 2001/11/21 19:24:32 leif Exp $

      DOUBLE PRECISION FUNCTION ARABL2(IP)

C...ARiadne subroutine ABove initial state Links version 2

C...Determine how much a given particle IP is above the phase space
C...limits given by the initial linked dipole chain as given in
C.../ARINT5/

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint5.f'
      INCLUDE 'arhide.f'

      ARABL2=0.0D0
      DPP=BP(IP,4)+BP(IP,3)
      DPM=BP(IP,4)-BP(IP,3)
      DEY=DPP/DPM
      DMT2=MIN(DPP*DPM,DBLE(PT2LST))
      ISEL=1

      IF (NLINKS.LE.1) THEN
        A=DVIRT(NLINKS+1)/DMT2
        A=MAX(A,(DKY(4)/DPM)**2)
        GOTO 900
      ENDIF

      IF (DEY*DPMV(1).GT.DPPV(1)) THEN
        A=(DKY(4)/DPM)**2
        GOTO 900
      ENDIF
      DO 100 I=2,NLINKS
        IF (DEY*DPMV(I).GT.DPPV(I)) THEN
          A=DVIRT(I)/DMT2
          A=MAX(A,(DPMV(I-1)/DPM)**2)
          A=MAX(A,(DPPV(I)/DPP)**2)
          ISEL=I
          GOTO 900
        ENDIF
 100  CONTINUE

      A=DVIRT(NLINKS+1)/DMT2
      A=MAX(A,(DPMV(NLINKS)/DPM)**2)
      ISEL=NLINKS

 900  IF (A.LT.1.0D0.AND.IFLL(ISEL+1).EQ.0.AND.MHAR(149).EQ.0) A=0.0D0
      ARABL2=A

      RETURN

C**** END OF ARABL2 ****************************************************
      END
