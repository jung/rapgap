C***********************************************************************
C $Id: arcopa.f,v 3.14 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARCOPA(IJ,IP,ITYP)

C...ARiadne subroutine COpy PArton

C...Copies a parton from position IJ in /LUJETS/ common block to
C...Position IP in /ARPART/ common block.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'aronia.f'

      INCLUDE 'ardble.f'


      DO 100 I=1,5
        BP(IP,I)=ARX2DB(P(IJ,I))
 100  CONTINUE

      IFL(IP)=K(IJ,2)
      IEX=MOD(K(IJ,4),10)
      IF (IEX.NE.0) THEN
        QEX(IP)=.TRUE.
        IF (PARA(10+IEX).GT.0.0) THEN
          XPMU(IP)=DBLE(PARA(10+IEX))
        ELSE
          XPMU(IP)=ARX2DB(V(IJ,4))
        ENDIF
        XPA(IP)=DBLE(PARA(10))
      ELSE
        QEX(IP)=.FALSE.
        XPMU(IP)=0.0D0
        XPA(IP)=0.0D0
      ENDIF
        
      QQ(IP)=(ITYP.NE.2)
      INO(IP)=0
      INQ(IP)=0
      IDI(IP)=0
      IDO(IP)=0
      IF (MSTA(1).EQ.2) INQ(IP)=-IJ
      PT2GG(IP)=0.0D0
      K(IJ,4)=-IP

      IF (ABS(IFL(IP)).LT.4.OR.ABS(IFL(IP)).GE.10) RETURN

      NHQ=NHQ+1
      IHQI(NHQ,1)=IP
      IHQI(NHQ,2)=IJ
      IHQI(NHQ,3)=-1
      IHQI(NHQ,4)=0

      RETURN

C**** END OF ARCOPA ****************************************************
      END
