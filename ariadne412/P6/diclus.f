C***********************************************************************
C $Id: diclus.f,v 3.8 2000/03/08 21:25:42 leif Exp $
      SUBROUTINE DICLUS(MODE,NDIM,NP,PP,IC1,IC2,
     $                  PTCUT,PTNEXT,NJMAX,NJMIN,NJ,IJ,PJ,IERR)
C
C Main interface to the Dipole Clustering algorithm (aka ARCLUS).
C
C Inputs:
C        INTEGER MODE       Recoil Strategy: 0 => Both final jets
C                           receives transverse recoil, 1=> Only
C                           closest jet receives recoil. 2 => Same as 1
C                           but use invariant mt instead of invariant pt
C                           as measure. -1 => same as 1 but join the
C                           selected cluster with nearest cluster with
C                           nearest cluster just adding four momenta.
C                           -2 => as -1 but with mt measure.
C        INTEGER NDIM       Dimension of PP and PJ vectors (PP(NDIM,*)
C        INTEGER NP         Number of entries in PP vector
C        DOUBLE PP(NDIM,*)  Vector of 3- or 4- momenta of particles
C                           to be clustered
C        INTEGER IC1,IC2    IC1/10 is the index of a pseudoparticle in PP
C                           which is not allowed to be clustered in
C                           DICLUS. If MOD(IC1,10) > 0 the direction of
C                           the pseudoparticle must not change. If
C                           MOD(IC1,10) > 1 the magnitude of the pseudo
C                           particle must not change. IC2 means the same
C                           for a second pseudo particle.
C        DOUBLE PTCUT       Maximum invariant pt allowed when
C                           clusterning three particles into two
C        INTEGER NJMAX      Maximum number of jets allowed
C        INTEGER NJMIN      ABS(NJMIN) is the minimum number of jets
C                           allowed. If less than 0, continue from
C                           previous clustering
C        INTEGER IJ(*)      If IJ(1) is larger than 0 then all particles
C                           in PP are assigned to a jet and the jet number
C                           is stored in the IJ vector. If IJ(1)=NP, then
C                           each jets is redefined as the sum of the
C                           momenta of the particles assigned to it.
C
C Outputs:
C        DOUBLE PTNEXT      Minimum invariant pt among the remaining
C                           jets
C        INTEGER NJ         Number of jets produced
C        INTEGER IJ(*)      The number of the nearest jet for each entry
C                           In the PP vector. Will only be given if
C                           IJ(1)>0 when called, otherwise IJ is left
C                           untouched.
C        DOUBLE PJ(NDIM,*)  Vector of 3- or 4- momenta of jets produced
C        INTEGER IERR       If <> 0 an error occurred during clustering
C
C
      IMPLICIT NONE
C Subroutine arguments
      INTEGER MODE,NDIM,NP,IC1,IC2,NJMAX,NJMIN,NJ,IERR
      DOUBLE PRECISION PP(NDIM,NP),PJ(NDIM,NJMAX),PTCUT,PTNEXT
C Other variables
      INTEGER NPMAX,I,J,I1,I2,I3,J1,J2,J3,NJC,MAXI,IJ(NP),I1C,I3C
      PARAMETER (NPMAX=1000)
      DOUBLE PRECISION P(6,NPMAX),MAXE,PT2M(NPMAX),PT2,PT2MIN,PT2MI2
      DOUBLE PRECISION D11,D12,D13,D14,D15,D16
      DOUBLE PRECISION D21,D22,D23,D24,D25,D26
      DOUBLE PRECISION D31,D32,D33,D34,D35,D36
      DOUBLE PRECISION D1D2,D2D3,D1D3,DR12,DR23
      DOUBLE PRECISION DIPT2I,DINVM2
      INTEGER ISLEFT(NPMAX),MIN1(NPMAX),MIN3(NPMAX),MIN1I2,MIN3I2
      LOGICAL TED(NPMAX),TED1,REDO,NEWJET
      SAVE ISLEFT,MIN1,MIN3,TED,P,PT2M,NJC
C End of declarations
C
C Check sanity of input
      IERR=-1
      IF (NDIM.LT.3) RETURN
      IF (NP.LE.0.OR.NP.GT.NPMAX) RETURN
      IF (NJMAX.LT.2) RETURN
      IF (ABS(NJMIN).GT.NJMAX) RETURN
      IERR=1
C
C Initialize:
      IF (NJMIN.GE.0) THEN      
        NJC=NP
        DO 100 I=1,NP
          DO 110 J=1,MIN(NDIM,5)
            P(J,I)=PP(J,I)
 110      CONTINUE
          IF (NDIM.LT.4) THEN
            P(4,I)=SQRT(P(3,I)**2+P(2,I)**2+P(1,I)**2)
            P(5,I)=0.0D0
          ELSEIF (NDIM.LT.5) THEN
            P(5,I)=SQRT(MAX(0.0D0,
     $           P(4,I)**2-P(3,I)**2-P(2,I)**2-P(1,I)**2))
          ENDIF
          P(6,I)=P(5,I)**2

          ISLEFT(I)=1
          IF (I.EQ.IC1/10) ISLEFT(I)=2+MOD(IC1,10)
          IF (I.EQ.IC2/10) ISLEFT(I)=2+MOD(IC2,10)
          TED(I)=.TRUE.
          MIN1(I)=0
          MIN3(I)=0
 100    CONTINUE
      ELSE
C
C Continue from previous run
        DO 120 I=1,NP
          ISLEFT(I)=-ISLEFT(I)
 120    CONTINUE
      ENDIF
C
C Start main loop
 200  J1=0
      J2=0
      J3=0
      PT2MIN=1.0D20
C
C Loop through all remaining particles
      DO 210 I2=1,NP
        IF (ISLEFT(I2).EQ.1) THEN
          PT2MI2=PT2M(I2)
          MIN1I2=MIN1(I2)
          MIN3I2=MIN3(I2)
          D21=P(1,I2)
          D22=P(2,I2)
          D23=P(3,I2)
          D24=P(4,I2)
          D25=P(5,I2)
          D26=P(6,I2)
          REDO=TED(I2).OR.MIN1(I2).EQ.0.OR.MIN3(I2).EQ.0
          IF (MIN1I2.NE.0) THEN
            IF (ISLEFT(MIN1I2).EQ.0) REDO=.TRUE.
            IF (TED(MIN1I2)) REDO=.TRUE.
          ENDIF
          IF (MIN3I2.NE.0) THEN
            IF (ISLEFT(MIN3I2).EQ.0) REDO=.TRUE.
            IF (TED(MIN3I2)) REDO=.TRUE.
          ENDIF
          IF (REDO) PT2MI2=1.0D20
C For each jet find pair of jets w.r.t. which pt is minimum
C Only check pairs where one or both jets have changed.
          DO 220 I1=1,NP-1
            IF (ISLEFT(I1).NE.0.AND.I1.NE.I2) THEN
              D11=P(1,I1)
              D12=P(2,I1)
              D13=P(3,I1)
              D14=P(4,I1)
              D15=P(5,I1)
              D16=P(6,I1)
              D1D2=MAX(D14*D24-D13*D23-D12*D22-D11*D21,0.0D0)
              IF (ABS(MODE).GE.2) THEN
                DR12=MAX(2.0D0*D1D2+D16+D26,0.0D0)
              ELSE
                DR12=4.0D0*MAX(D1D2-D15*D25,0.0D0)
              ENDIF
              TED1=TED(I1)
              DO 230 I3=I1+1,NP
                IF (ISLEFT(I3).NE.0.AND.I3.NE.I2.AND.
     $               (REDO.OR.TED(I3).OR.TED1)) THEN 
                  D31=P(1,I3)
                  D32=P(2,I3)
                  D33=P(3,I3)
                  D34=P(4,I3)
                  D35=P(5,I3)
                  D36=P(6,I3)
                  D2D3=MAX(D24*D34-D23*D33-D22*D32-D21*D31,0.0D0)
                  IF (ABS(MODE).GE.2) THEN
                    DR23=MAX(2.0D0*D2D3+D26+D36,0.0D0)
                  ELSE
                    DR23=MAX(D2D3-D25*D35,0.0D0)
                  ENDIF
                  D1D3=MAX(D14*D34-D13*D33-D12*D32-D11*D31,0.0D0)
                  PT2=DR12*DR23/(D16+D26+D36+2.0D0*(D1D2+D2D3+D1D3))
                  IF (ABS(MODE).GT.2) PT2=PT2-D26
                  IF (PT2.LT.PT2MI2) THEN
                    PT2MI2=PT2
                    MIN1I2=I1
                    MIN3I2=I3
                  ENDIF
                ENDIF
 230          CONTINUE
            ENDIF
 220      CONTINUE
C
          PT2M(I2)=PT2MI2
          MIN1(I2)=MIN1I2
          MIN3(I2)=MIN3I2
          IF (PT2MI2.LT.PT2MIN) THEN
            PT2MIN=PT2MI2
            J1=MIN1I2
            J2=I2
            J3=MIN3I2
          ENDIF
        ENDIF
 210  CONTINUE
C
C If no pt was found below ptcut then copy remaining jets to ouput
C vector and exit.
      IF (J1.EQ.0.OR.NJC.LE.MAX(ABS(NJMIN),2).OR.
     $     PT2MIN.GT.PTCUT**2) THEN
        PTNEXT=SQRT(PT2MIN)
        IF (NJC.GT.NJMAX) RETURN
        IERR=0
        DO 300 NJ=1,NJC
          MAXE=0.0D0
          MAXI=0
          DO 310 I=1,NP
            IF (ISLEFT(I).GT.0) THEN
              IF (P(4,I).GT.MAXE) THEN
                MAXE=P(4,I)
                MAXI=I
              ENDIF
            ENDIF
 310      CONTINUE
          DO 320 J=1,MIN(NDIM,5)
            PJ(J,NJ)=P(J,MAXI)
 320      CONTINUE
          ISLEFT(MAXI)=-ISLEFT(MAXI)
 300    CONTINUE
        NJ=NJC
        IF (IJ(1).LE.0) RETURN
        NEWJET=(IJ(1).EQ.NP)
        DO 400 I=1,NP
          I1C=0
          I3C=0
          PT2MI2=1.0E20
          DO 410 I1=1,NJ-1
            DO 420 I3=I1+1,NJ
              PT2=DIPT2I(PJ(1,I1),PP(1,I),PJ(1,I3),NDIM,MODE)
              IF (PT2.GT.PT2MI2) GOTO 420
              I1C=I1
              I3C=I3
              PT2MI2=PT2
 420        CONTINUE
 410      CONTINUE
          IJ(I)=0
          IF (I1C.LE.0) GOTO 400
          IF (DINVM2(PJ(1,I1C),PP(1,I),NDIM,MODE).LT.
     $        DINVM2(PJ(1,I3C),PP(1,I),NDIM,MODE)) THEN
            IJ(I)=I1C
          ELSE
            IJ(I)=I3C
          ENDIF
 400    CONTINUE
          
        IF (.NOT.NEWJET) RETURN
        DO 440 I=1,NJ
          DO 450 J=1,NDIM
            PJ(J,I)=0.0D0
 450      CONTINUE
 440    CONTINUE
        DO 460 I=1,NP
          DO 470 J=1,NDIM
            PJ(J,IJ(I))=PJ(J,IJ(I))+PP(J,I)
 470      CONTINUE
 460    CONTINUE
        RETURN
      ENDIF
C
C Cluster the chosen jet into the two jets w.r.t. which its pt is
C minimum
      DO 500 I=1,NP
        TED(I)=.FALSE.
 500  CONTINUE
      CALL DIJOIN(MODE,P(1,J1),P(1,J2),P(1,J3),ISLEFT(J1),ISLEFT(J3))
      
      ISLEFT(J2)=0
      IF (ISLEFT(J1).LT.4) TED(J1)=.TRUE.
      IF (ISLEFT(J3).LT.4) TED(J3)=.TRUE.
      NJC=NJC-1
C
C Loop back
      GOTO 200
C
      END
C
C***********************************************************************
      DOUBLE PRECISION FUNCTION DIPT2I(P1,P2,P3,NDIM,MODE)
C
      IMPLICIT NONE
C
      INTEGER NDIM,MODE
      DOUBLE PRECISION P1(NDIM),P2(NDIM),P3(NDIM),
     $     D14,D15,D24,D25,D34,D35,DS12,DS23,DS123
C
      IF(NDIM.GT.3) THEN
        D14=P1(4)
        D24=P2(4)
        D34=P3(4)
      ELSE
        D14=SQRT(P1(3)**2+P1(2)**2+P1(1)**2)
        D24=SQRT(P2(3)**2+P2(2)**2+P2(1)**2)
        D34=SQRT(P3(3)**2+P3(2)**2+P3(1)**2)
      ENDIF
      DS12=MAX((D14+D24)**2-(P1(3)+P2(3))**2-
     $         (P1(2)+P2(2))**2-(P1(1)+P2(1))**2,0.0D0)
      DS23=MAX((D34+D24)**2-(P3(3)+P2(3))**2-
     $         (P3(2)+P2(2))**2-(P3(1)+P2(1))**2,0.0D0)
      DS123=MAX((D14+D24+D34)**2-(P1(3)+P2(3)+P3(3))**2-
     $          (P1(2)+P2(2)+P3(2))**2-(P1(1)+P2(1)+P3(1))**2,0.0D0)
      IF (ABS(MODE).GE.2.OR.NDIM.LT.4) THEN
        DIPT2I=DS12*DS23/DS123
        IF (ABS(MODE).GT.2)
     $       DIPT2I=DIPT2I-(P2(4)**2-P2(3)**2-P2(2)**2-P2(1)**2)
      ELSE
        IF(NDIM.GT.4) THEN
          D15=P1(5)
          D25=P2(5)
          D35=P3(5)
        ELSE
          D15=SQRT(MAX(0.0D0,P1(4)**2-P1(3)**2-P1(2)**2-P1(1)**2))
          D25=SQRT(MAX(0.0D0,P2(4)**2-P2(3)**2-P2(2)**2-P2(1)**2))
          D35=SQRT(MAX(0.0D0,P3(4)**2-P3(3)**2-P3(2)**2-P3(1)**2))
        ENDIF
        DIPT2I=MAX(DS12-(D15+D25)**2,0.0D0)*
     $         MAX(DS23-(D35+D25)**2,0.0D0)/DS123
      ENDIF
C
      RETURN
C
      END
C
C***********************************************************************
C
      DOUBLE PRECISION FUNCTION DINVM2(P1,P2,NDIM,MODE)
C
      IMPLICIT NONE
C
      INTEGER NDIM,MODE
      DOUBLE PRECISION P1(NDIM),P2(NDIM),D14,D15,D24,D25
C
      IF(NDIM.GT.3) THEN
        D14=P1(4)
        D24=P2(4)
      ELSE
        D14=SQRT(P1(3)**2+P1(2)**2+P1(1)**2)
        D24=SQRT(P2(3)**2+P2(2)**2+P2(1)**2)
      ENDIF
      DINVM2=MAX((D14+D24)**2-(P1(3)+P2(3))**2-
     $           (P1(2)+P2(2))**2-(P1(1)+P2(1))**2,0.0D0)
      IF (ABS(MODE).LT.2.AND.NDIM.GT.3) THEN
        IF(NDIM.GT.4) THEN
          D15=P1(5)
          D25=P2(5)
        ELSE
          D15=SQRT(MAX(0.0D0,P1(4)**2-P1(3)**2-P1(2)**2-P1(1)**2))
          D25=SQRT(MAX(0.0D0,P2(4)**2-P2(3)**2-P2(2)**2-P2(1)**2))
        ENDIF
        DINVM2=DINVM2-(D15+D25)**2
      ENDIF
C
      RETURN
C
      END
C
C***********************************************************************

