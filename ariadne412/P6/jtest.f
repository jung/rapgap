      PROGRAM LEP


C...First set parameters as Ariadne wants them
      CALL ARTUNE('4.11')

C...Call a user supplied routine setting
C...the parameters and switches in JETSET
      CALL SETJET


C...Call a user supplied routine setting
C...the parameters and switches in Ariadne
      CALL ARISET


C...Initialize Ariadne to run with JETSET
      CALL ARINIT('JETSET')


C...Loop over a number of events
      DO 100 IEVE=1,10


C...Generate an LEP event with JETSET
        CALL PYEEVT(0,91.0D0)


C...Apply the Dipole Cascade
        CALL AREXEC


C...Call a user supplied analysis routine
        CALL LEPANA


 100  CONTINUE


      END

      SUBROUTINE SETJET

      INCLUDE 'arimpl.f'
      INCLUDE 'pydat3.f'

      INTEGER PYCOMP

C...Set pi0 stable
      MDCY(PYCOMP(111),1)=0

      RETURN 
      END

      SUBROUTINE ARISET

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'

C...Allow for photon radiation off quarks
      MSTA(20)=1

      RETURN
      END

      SUBROUTINE LEPANA

C...Perform Jet clustering
      CALL ARCLUS(NJET)

C...Trivial analysis - print the event
      CALL PYLIST(2)

      RETURN
      END

      SUBROUTINE DUMMY9
C...This is a dummy routine to ensure the relevant block data routines
C...are linked from the JETSET/PYTHIA library. This routine should never
C...be called.

      CALL LUDATA
      CALL PYDATA

      RETURN
      END
