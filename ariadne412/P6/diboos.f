C***********************************************************************
C $Id: diboos.f,v 3.1 1996/03/08 09:55:46 leif Exp $
      SUBROUTINE DIBOOS(DB,P)
C
C Boost
C
C Inputs:
C         DOUBLE DB(3)             Boost vector
C         DOUBLE P(4)              4-Vector to boost
C
C Outputs:
C         DOUBLE P(4)              Boosted 4-vector
C
      IMPLICIT NONE
C Subroutine arguments
      DOUBLE PRECISION DB(3),P(4)
C Other variables
      DOUBLE PRECISION DBT2,DGA,DBP,DGABP
C End of declarations
C
      DBT2=DB(1)**2+DB(2)**2+DB(3)**2
      IF (DBT2.LE.1.0D-20) RETURN
      DGA=1.0D0/SQRT(1.0D0-DBT2)
      DBP=P(1)*DB(1)+P(2)*DB(2)+P(3)*DB(3)
      DGABP=DGA*(DGA*DBP/(1.0D0+DGA)+P(4))
      P(1)=P(1)+DGABP*DB(1)
      P(2)=P(2)+DGABP*DB(2)
      P(3)=P(3)+DGABP*DB(3)
      P(4)=DGA*(P(4)+DBP)
C
      RETURN
C
      END
C
C***********************************************************************
