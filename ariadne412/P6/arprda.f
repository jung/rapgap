C***********************************************************************
C $Id: arprda.f,v 3.8 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE ARPRDA

C...ARiadne subroutine PRint DAta

C...Prints out parameters and switches used in Ariadne.

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'


      WRITE(MSTA(7),*)
      WRITE(MSTA(7),1000)
      DO 100 I=1,20
        WRITE(MSTA(7),1010) I,MSTA(I),MSTA(I+20),PARA(I),PARA(I+20)
 100  CONTINUE
      WRITE(MSTA(7),*)

 1000 FORMAT(10X,'Parameters and switches used by Ariadne:',/,/,
     $     '         I   MSTA(I) MSTA(I+20)   PARA(I) PARA(I+20)',/)
 1010 FORMAT(2I10,I11,3X,2G11.3)

      RETURN

C**** END OF ARPRDA ****************************************************
      END

C***********************************************************************
C $Id: arprda.f,v 3.8 2001/10/21 19:36:49 leif Exp $

      DOUBLE PRECISION FUNCTION ARPRNT()

C...ARiadne function PRiNT

C...Prints out current internal event record.


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'arint3.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'pyjets.f'

      DIMENSION DTOT(5)
      CHARACTER ON


      WRITE(*,*)
C...Reset momentum counter. Include Drell-Yan produced particle and
C...others in special positions if present and check its momentum
C...consistency.
      DO 100 J=1,4
        DTOT(J)=0.0D0
 100  CONTINUE
      DO 110 I=MAXPAR-4,MAXPAR-2
        IF (.NOT.QQ(I)) GOTO 110
        DO 120 J=1,4
          DTOT(J)=DTOT(J)+BP(I,J)
 120    CONTINUE
        DMASS=BP(I,4)**2-BP(I,3)**2-BP(I,2)**2-BP(I,1)**2
        IF ( DMASS.GE.0.0D0 ) THEN
          DMASS = SQRT(DMASS)
        ELSE
          DMASS = -SQRT(-DMASS)
        ENDIF
        WRITE(*,600) I,IFL(I),IDI(I),IDO(I),(BP(I,J),J=1,5),DMASS
 110  CONTINUE
      
C...Sum all partons momentum and check their momentum concistency.
      DO 130 I=1,IPART
        DMASS=BP(I,4)**2-BP(I,3)**2-BP(I,2)**2-BP(I,1)**2
        IF ( DMASS.GE.0.0D0 ) THEN
          DMASS = SQRT(DMASS)
        ELSE
          DMASS = -SQRT(-DMASS)
        ENDIF
        WRITE(*,600) I,IFL(I),IDI(I),IDO(I),(BP(I,J),J=1,5),DMASS
        DO 140 J=1,4
          DTOT(J)=DTOT(J)+BP(I,J)
 140    CONTINUE
 130  CONTINUE
      DTOT(5)=DSQRT(MAX(DTOT(4)**2-DTOT(3)**2-DTOT(2)**2-DTOT(1)**2,
     $                  0.0D0))
      WRITE(*,601) (DTOT(J),J=1,5)
      WRITE(*,601) (DPTOT(J),J=1,5)
      WRITE(*,*)
      DO 200 I=1,IDIPS
        ON = '*'
        IF ( QDONE(I) ) THEN
          ON = ' '
        ELSE
          SDIP(I)=ARMAS2(IP1(I),IP3(I))
        ENDIF
        WRITE(*,602) I,IP1(I),IP3(I),SQRT(SDIP(I)),
     $       SQRT(SDIP(I))-BP(IP1(I),5)-BP(IP3(I),5),
     $       PT2IN(I),BX1(I),BX3(I),AEX1(I),AEX3(I),ICOLI(I),ON
 200  CONTINUE

      DO 210 I=1,ISTRS
        WRITE(*,604) I,IPF(I),IPL(I),IO,SIGN(1,IFLOW(I))
 210  CONTINUE

      ON = '*'
      IF ( QDUMP ) ON = ' '
      WRITE(*,603) SQRT(PT2LST),SQRT(PT2MAX),IO,ON

      DIFF=0.0D0
      DO 300 J=1,5
        DIFF=DIFF+(DTOT(J)-DPTOT(J))**2
 300  CONTINUE
     
      ARPRNT=DIFF

 600  FORMAT(I3,I6,2I6,5F10.3,F7.3)
 601  FORMAT(21X,5F10.3)
 602  FORMAT(I3,2I4,3F10.3,4F7.3,I5,A)
 603  FORMAT(2G15.3,I4,A)
 604  FORMAT(I3,4I4)

      RETURN

C**** END OF ARPRNT ****************************************************
      END

