C***********************************************************************
C $Id: arildc.f,v 3.20 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARILDC

C...ARIadne subroutine perform LDC cascade on lepto event

C...Performs a cascade starting on a zero'th order event from LEPTO


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint4.f'
      INCLUDE 'arint5.f'
      INCLUDE 'arhide.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'leptou.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'pypars.f'

      DIMENSION DSP(4),DSP2(4),DP(7*MAXPAR),IFLV(MAXPAR),ISTRK(MAXPAR)

      INCLUDE 'ardble.f'


      PTI=0.0

C...Check which quark was struck
      IFLSTR=LST(25)

C...Get x and Q2 in double precision
      DY=XY
      DX=X
      DQ2=XQ2

C...Boost to hadronic CMS
      CALL ARBOLE(THEL,PHI1,PHI2,DBXL,DBYL,DBZL)


C...Mark all particles/partons decayed

      DO 100 J=1,4
        DSP(J)=0.0D0
        DSP2(J)=0.0D0
 100  CONTINUE
      DO 110 I=5,N
        IF (K(I,1).GT.0.AND.K(I,1).LT.10) THEN
          DO 120 J=1,4
            DSP(J)=DSP(J)+ARX2DB(P(I,J))
            K(I,1)=K(I,1)+10
 120      CONTINUE
        ENDIF
 110  CONTINUE

C...Call the LDC generator to get initial chains.
      CALL LDCEXE(K(2,2),IFLSTR,DQ2,DX,DY,MAXPAR,
     $     NPAR,DP,IFLV,NLINKS,DKY,IFLL,DVIRT,DPPV,DPMV,INFO)

C...Loop over all partons
      IFLTOT=0
      IPART=0
      IDIPS=0
      ISTRS=0
      IR1=0
      IR2=0
      IMF=5
      IML=N
      IO=0
      QDUMP=.FALSE.
      PT2LST=DBLE(PARA(40))
      QINSTR=.FALSE.
      NSTRK=0
      DO 200 IP=1,NPAR
        IOFF=(IP-1)*5
        CALL ARBOOP
        DO 210 J=1,5
          BP(IP,J)=DP(IOFF+J)
 210    CONTINUE
        IF (IFLV(IP).EQ.21) CALL ARERRM('ARILDC',5,0)
        IF (IFLV(IP).EQ.-100000) THEN
          IF (IR1.GT.0) THEN
            IR2=IP
          ELSE
            IR1=IP
          ENDIF
        ELSE
          NSTRK=NSTRK+1
          ISTRK(NSTRK)=IP
          IFLTOT=IFLTOT+IFLV(IP)
          DO 220 J=1,4
            DSP2(J)=DSP2(J)+DP(IOFF+J)
 220      CONTINUE
        ENDIF
        IFL(IP)=IFLV(IP)
        IF (IFL(IP).EQ.0) IFL(IP)=21
        IF (IFLV(IP).NE.0) QQ(IP)=.TRUE.
        IF (QINSTR) THEN
          CALL ARBOOD
          CALL ARCRDI(IDIPS,IP-1,IP,ISTRS,.FALSE.)
          PTMX2(IDIPS)=-1.0D0
          IF (IFL(IP).NE.21) THEN
            QINSTR=.FALSE.
            IPL(ISTRS)=IP
          ENDIF
        ELSE
          ISTRS=ISTRS+1
          IPF(ISTRS)=IP
          IFLOW(ISTRS)=SIGN(1,IFLV(IP))
          QINSTR=.TRUE.
        ENDIF
 200  CONTINUE

C...To avoid boost problems later on, boost struck system to its CMS
      DBZ=-DSP2(3)/DSP2(4)
      CALL ARROBO(0.0D0,0.0D0,0.0D0,0.0D0,DBZ,NSTRK,ISTRK)
      CALL ARSUME(0,DSP2(1),DSP2(2),DSP2(3),DSP2(4),DMS,NSTRK,ISTRK)

      DS2=DSP(4)**2-DSP(3)**2-DSP(2)**2-DSP(1)**2

 300  CALL ARRFLV(K(2,2),IFLTOT,MOD(INFO/8,2).eq.1,KFR1,KFR2,KFH)

      IF (KFR2.LE.0) THEN
C...Single remnant
        DRMR=PYMASS(KFR1)
        CALL ARDCMS(DS2,DMS,DRMR,DPP)
        IF (DPP.LT.0.0D0) GOTO 300
        DPN2=DPP**2
        DPO2=(DSP2(4)+DSP2(3))**2
        DBZ=(DPN2-DPO2)/(DPN2+DPO2)
        IFL(IR1)=KFR1
        DPPR=SQRT(DS2)-DPP
        DPMR=(DRMR**2)/DPPR
        BP(IR1,1)=0.0D0
        BP(IR1,2)=0.0D0
        BP(IR1,3)=0.5D0*(DPPR-DPMR)
        BP(IR1,4)=0.5D0*(DPPR+DPMR)
        BP(IR1,5)=DRMR
      ELSEIF (KFH.EQ.0) THEN
C...Two remnants
        DRMR1=PYMASS(KFR1)
        DRM2R1=DRMR1**2
        DRMR2=PYMASS(KFR2)
        DRM2R2=DRMR2**2
        CALL PYPTDI(KFR2,PX,PY)
        DPX=PX
        DPY=PY
        DMT2R2=DRM2R2+DPX**2+DPY**2
        DMT2R1=DRM2R1+DPX**2+DPY**2
        CALL PYZDIS(KFR2,0,DMT2R2,Z)
        DZ=Z
        DRM2R=DMT2R2/DZ+DMT2R1/(1.0D0-DZ)
        DRMR=SQRT(DRM2R)
        CALL ARDCMS(DS2,DMS,DRMR,DPP)
        IF (DPP.LT.0.0D0) GOTO 300
        DPN2=DPP**2
        DPO2=(DSP2(4)+DSP2(3))**2
        DBZ=(DPN2-DPO2)/(DPN2+DPO2)
        DPPR=SQRT(DS2)-DPP
        DPMR=DRM2R/DPPR
        DPMR2=DZ*DPMR
        DPPR2=DMT2R2/DPMR2
        DPMR1=(1.0D0-DZ)*DPMR
        DPPR1=DMT2R1/DPMR1
        IF (IFLOW(ISTR(IDI(IR1))).GT.0.AND.
     $       ((KFR1.GT.0.AND.KFR1.LT.10).OR.
     $       (KFR2.LT.0.AND.KFR2.GT.-10))) THEN
          IDUM=IR1
          IR1=IR2
          IR2=IDUM
        ENDIF
        IFL(IR1)=KFR1
        BP(IR1,1)=DPX
        BP(IR1,2)=DPY
        BP(IR1,3)=0.5D0*(DPPR1-DPMR1)
        BP(IR1,4)=0.5D0*(DPPR1+DPMR1)
        BP(IR1,5)=DRMR1
        IFL(IR2)=KFR2
        BP(IR2,1)=-DPX
        BP(IR2,2)=-DPY
        BP(IR2,3)=0.5D0*(DPPR2-DPMR2)
        BP(IR2,4)=0.5D0*(DPPR2+DPMR2)
        BP(IR2,5)=DRMR2
      ELSE
C...Remnant + hadron
        DRMR1=PYMASS(KFR1)
        DRM2R1=DRMR1**2
        DRMP=PYMASS(KFH)
        DRM2P=DRMP**2
        CALL PYPTDI(KFR2,PX,PY)
        DPX=PX
        DPY=PY
        DMT2R1=DRMR1**2+DPX**2+DPY**2
        DMT2P=DRM2P+DPX**2+DPY**2
        CALL PYZDIS(KFR2,0,DMT2P,Z)
        DZ=Z
        DRM2R=DMT2P/DZ+DMT2R1/(1.0D0-DZ)
        DRMR=SQRT(DRM2R)
        CALL ARDCMS(DS2,DMS,DRMR,DPP)
        IF (DPP.LT.0.0D0) GOTO 300
        DPN2=DPP**2
        DPO2=(DSP2(4)+DSP2(3))**2
        DBZ=(DPN2-DPO2)/(DPN2+DPO2)
        DPPR=SQRT(DS2)-DPP
        DPMR=DRM2R/DPPR
        DPMP=DZ*DPMR
        DPPP=DMT2P/DPMP
        DPMR1=(1.0D0-DZ)*DPMR
        DPPR1=DMT2R1/DPMR1
        IPART=IPART+1
        IR2=IPART
        IFL(IR1)=KFR1
        BP(IR1,1)=DPX
        BP(IR1,2)=DPY
        BP(IR1,3)=0.5D0*(DPPR1-DPMR1)
        BP(IR1,4)=0.5D0*(DPPR1+DPMR1)
        BP(IR1,5)=DRMR1
        IFL(IR2)=KFH
        BP(IR2,1)=-DPX
        BP(IR2,2)=-DPY
        BP(IR2,3)=0.5D0*(DPPP-DPMP)
        BP(IR2,4)=0.5D0*(DPPP+DPMP)
        BP(IR2,5)=DRMP      
      ENDIF

C...Generate a total intrinsic transverse momentum
      IF (MSTA(37).EQ.0) THEN
        IF (MSTA(1).EQ.2) THEN
          IF(MSTP(91).LE.0) THEN
            PTI=0.0D0
          ELSEIF(MSTP(91).EQ.1) THEN
            PTI=ARX2DB(PARP(91))*SQRT(-LOG(PYR(0)))
          ELSE
            RPT1=PYR(0)
            RPT2=PYR(0)
            PTI=-ARX2DB(PARP(92))*LOG(RPT1*RPT2)
          ENDIF
          IF(PTI.GT.ARX2DB(PARP(93))) GOTO 300
        ELSE
          PTI=DBLE(PARL(3))*SQRT(-LOG(PYR(0)))
        ENDIF
      ELSEIF (MSTA(37).EQ.1) THEN
        PTI=DBLE(PARA(27))*SQRT(-LOG(PYR(0)))
      ELSEIF (MSTA(37).EQ.2) THEN
        RPT1=PYR(0)
        RPT2=PYR(0)
        PTI=-DBLE(PARA(27))*LOG(RPT1*RPT2)/SQRT(6.0D0)          
      ENDIF
      PZR=ABS(0.5D0*(DPPR-DPMR))
      IF (PZR.LE.PTI) GOTO 300
      THEI=ASIN(PTI/PZR)
      PHII=ARX2DB(PARU(2))*PYR(0)
      CALL ARROBO(0.0D0,0.0D0,0.0D0,0.0D0,DBZ,NSTRK,ISTRK)
      NSTRK=NSTRK+1
      ISTRK(NSTRK)=IR1
      IF (KFH.EQ.0.AND.IR2.GT.0) THEN
        NSTRK=NSTRK+1
        ISTRK(NSTRK)=IR2
      ENDIF
        
      IF (KFH.NE.0.AND.IR2.GT.0) THEN
        IPART=IPART-1
        CALL AROBO1(0.0D0,-PHII,0.0D0,0.0D0,0.0D0,IR2)
        CALL AROBO1(THEI,PHII,0.0D0,0.0D0,0.0D0,IR2)
        N=N+1
        K(N,1)=1
        K(N,2)=IFL(IR2)
        K(N,3)=IMF
        K(N,4)=0
        K(N,5)=0
        P(N,1)=ARDB2X(BP(IR2,1))
        P(N,2)=ARDB2X(BP(IR2,2))
        P(N,3)=ARDB2X(BP(IR2,3))
        P(N,4)=ARDB2X(BP(IR2,4))
        P(N,5)=ARDB2X(BP(IR2,5))
      ENDIF

      CALL ARROBO(0.0D0,-PHII,0.0D0,0.0D0,0.0D0,NSTRK,ISTRK)
      CALL ARROBO(THEI,PHII,0.0D0,0.0D0,0.0D0,NSTRK,ISTRK)

      IF (MSTA(32).EQ.42) THEN
        MSTA(6)=0
        CALL ARCASC
        XDUM=ARPRNT()
      ELSE IF (MSTA(32).NE.-2) THEN
        CALL ARCASC
      ELSE
        CALL ARDUMP
      ENDIF

C...Boost back to original system
      CALL PYROBO(1,N,0.0D0,PHI2,0.0D0,0.0D0,0.0D0)
      CALL PYROBO(1,N,THEL,PHI1,DBXL,DBYL,DBZL)

      RETURN

C**** END OF ARILDC ****************************************************
      END
