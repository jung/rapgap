      PROGRAM NLOLEP

C      IMPLICIT DOUBLE PRECISION(A-H, O-P, R-Z)
C
C      ntry=10000000
C      ecm=91.1d0
C      cut=0.0001d0
C      do 20 j=1,8
C        nacc=0
C        nacq=0
C        nac3=0
C        cut=cut*2
C        do 10 i=1,ntry
C          kfln=21
C          call arx4jt(njet,cut,1,ecm,kfln,x1,x2,x4,x12,x14)
C          if ( njet.eq.4 ) nacc=nacc+1
C          kfln=2
C          call arx4jt(njet,cut,1,ecm,kfln,x1,x2,x4,x12,x14)
C          if ( njet.eq.4 ) nacq=nacq+1
C          call arxjt3(njet,cut,1,ecm,x1,x2)
C          if ( njet.eq.3 ) nac3=nac3+1
C 10     continue
C
C        ct=log(1d0/cut-5d0)
C        cf=4.0d0/3.0d0
C        cn=3.0d0
C        tr=0.5d0
C        wtmxgg=0.7d0/cut**2
C        xqqgg=wtmxgg*ct*(1.0d0-6.0d0*cut)**2
C        xqqgg1=-0.09773d0+0.2959d0*ct-0.2764d0*ct**2+0.08832d0*ct**3
C        xqqgg2=6.349d0-4.330d0*ct+0.8304d0*ct**2
C        wtmxqq=0.1125d0*cf*tr/cut**2
C        xqqqq=wtmxqq*(1.0d0-6.0d0*cut)**3
C        xqqqq2=0.25d0*(-0.1080d0+0.01486d0*ct+0.009364d0*ct**2)
C        xqqqq1=0.25d0*(0.003661d0-0.004888d0*ct-0.001081d0*ct**2+
C     &       0.002093d0*ct**3)
C        write(*,60) cut,dble(nacc)*xqqgg/(dble(ntry)*xqqgg1*ct*ct),
C     $       dble(nacc)*xqqgg/(dble(ntry)*xqqgg2*ct*ct),
C     $       1.0/sqrt(dble(nacc)),
C     $       dble(nacq)*xqqqq/(dble(ntry)*xqqqq1*ct*ct),
C     $       dble(nacq)*xqqqq/(dble(ntry)*xqqqq2*ct*ct),
C     $       1.0/sqrt(dble(nacq))
C        sig30=((3d0-6d0*cut+2d0*log(cut))*
C     &       log(cut/(1d0-2d0*cut))+(2.5d0+1.5d0*cut-6.571d0)*
C     &       (1d0-3d0*cut)+5.833d0*(1d0-3d0*cut)**2-3.894d0*
C     &       (1d0-3d0*cut)**3+1.342d0*(1d0-3d0*cut)**4)
C        sig3=2.0*log(cut)**2
C        write(*,61) dble(nac3)*sig3/(dble(ntry)*sig30),
C     $       1.0/sqrt(dble(nac3))
C 20   continue
C      call exit(0)
C 60   format(7f10.4)
C 61   format(20x,2f10.4)

C...Call a user supplied routine setting
C...the parameters and switches in JETSET
      CALL SETJET


C...Call a user supplied routine setting
C...the parameters and switches in Ariadne
      CALL ARISET


C...Initialize Ariadne to run with JETSET
      CALL ARINIT('EEMEPS')


C...Loop over a number of events
      DO 100 IEVE=1,1000


C...Generate an LEP event with JETSET
        CALL PYEEVT(1,91.0D0)


C...Apply the Dipole Cascade
        CALL AREXEC


C...Call a user supplied analysis routine
        IF ( IEVE.LT.10 ) CALL LEPANA


 100  CONTINUE

      CALL ARIEND

      END

      SUBROUTINE SETJET

      INCLUDE 'arimpl.f'
      INCLUDE 'pypars.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'pydat3.f'

      INTEGER PYCOMP

C...Set pi0 stable
      MDCY(PYCOMP(111),1)=0

C...Switch off hadronization
      MSTU(12)=0
      MSTJ(105)=0
      MSTJ(106)=0
      MSTP(111)=0
      PARJ(125)=0.001

      RETURN 
      END

      SUBROUTINE ARISET

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'

C...Allow for photon radiation off quarks
C      MSTA(20)=1
      call artune('4.10')
      MHAR(164)=0
      MHAR(165)=0
      MHAR(166)=1
      MHAR(167)=5
      MHAR(168)=3
      MHAR(169)=1
      MHAR(171)=-1
      MSTA(15)=0
      MSTA(12)=0

      RETURN
      END

      SUBROUTINE ARIEND

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'

      WRITE(*,*) PHAR(172),PHAR(173),PHAR(174),PHAR(175)
      WRITE(*,*) DBLE(MHAR(172))/DBLE(MSTA(4)),
     $     DBLE(MHAR(173))/DBLE(MSTA(4)),
     $     DBLE(MHAR(174))/DBLE(MSTA(4)),
     $     DBLE(MHAR(175))/DBLE(MSTA(4))

      RETURN
      END

      SUBROUTINE LEPANA

C...Perform Jet clustering
C      CALL ARCLUS(NJET)

C...Trivial analysis - print the event
C      CALL PYLIST(2)

      RETURN
      END

      SUBROUTINE DUMMY9
C...This is a dummy routine to ensure the relevant block data routines
C...are linked from the JETSET/PYTHIA library. This routine should never
C...be called.

      CALL LUDATA
      CALL PYDATA

      RETURN
      END
