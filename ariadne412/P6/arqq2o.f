C***********************************************************************
C $Id: arqq2o.f,v 1.3 2000/02/15 14:02:36 leif Exp $

      SUBROUTINE ARQQ2O

C...ARiadne subroutine join heavy QQbar pairs into Onium.

C...Go through final state partons and find heavy q-qbar pairs and join
C...them into Onium states according to given models if possible.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'arhide.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'aronia.f'
      INCLUDE 'arint2.f'
      INCLUDE 'arint3.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat1.f'

      DOUBLE PRECISION DM2(MAXPAR),DPROP(MAXPAR)
      INTEGER IPQ1(MAXPAR),IPQ2(MAXPAR),IOENT(MAXPAR)


      IF (MHAR(145).EQ.0) RETURN

C...First gather some info
      IPAIR=0
      NCH=0
      DSUMP=0.0D0
      DMD2=PYMASS(421)**2
      DMB2=PYMASS(511)**2
      DO 100 I1=1,IPART
        IF (IFL(I1).EQ.21) GOTO 100
        IF (ABS(IFL(I1)).LT.4.OR.ABS(IFL(I1)).GE.6) GOTO 100
        DO 110 I2=I1+1,IPART
          IF (IFL(I2).NE.-IFL(I1)) GOTO 110
          DMQQ2=ARMAS2(I1,I2)
          IFLP=ABS(IFL(I1))
          IF (IFLP.EQ.4.AND.4.0D0*DMD2.LT.DMQQ2) GOTO 110
          IF (IFLP.EQ.5.AND.4.0D0*DMB2.LT.DMQQ2) GOTO 110
          IPAIR=IPAIR+1
          DM2(IPAIR)=DMQQ2
          IPQ1(IPAIR)=I1
          IPQ2(IPAIR)=I2
 110    CONTINUE
 100  CONTINUE

      IF (IPAIR.EQ.0) RETURN

C...Randomize entries
      DO 200 IP=1,IPAIR*4
        IPR1=INT(IPAIR*PYR(0))+1
        IPR2=INT(IPAIR*PYR(0))+1
        IF (IPR1.EQ.IPR2) GOTO 200
        DMQQ2=DM2(IPR1)
        DM2(IPR1)=DM2(IPR2)
        DM2(IPR2)=DMQQ2
        IQ=IPQ1(IPR1)
        IPQ1(IPR1)=IPQ1(IPR2)
        IPQ1(IPR2)=IQ
        IQ=IPQ2(IPR1)
        IPQ2(IPR1)=IPQ2(IPR2)
        IPQ2(IPR2)=IQ
 200  CONTINUE

      DO 300 IP=1,IPAIR
        IF (IPQ1(IP).LE.0.OR.IPQ2(IP).LE.0) GOTO 300
        CALL ARQQCF(IPQ1(IP),IPQ2(IP),MODE,ICREP,NGP)
        IF (MODE.LT.0) GOTO 300
        NCH=0
        DSUMPR=0.0D0
        DO 310 IONI=1,NONI
          IF (MEONI(IONI).NE.-MODE) GOTO 310
          IF (IPONI(IONI).NE.ABS(IFL(IPQ1(IP)))) GOTO 310
          IF (PONI(IONI,1).EQ.0.0D0.AND.ICREP.NE.1) GOTO 310
          IF (PONI(IONI,2).GE.0.0D0.AND.INT(PONI(IONI, 
     & 2)).NE.NGP) GOTO 310
          IF (PONI(IONI,2).LT.0.0D0.AND.NGP.LT.-INT(PONI(IONI,2)))
     $         GOTO 310
          NCH=NCH+1
          DPROP(NCH)=PONI(IONI,3)
          IOENT(NCH)=IONI
          DSUMPR=DPROP(NCH)
 310    CONTINUE
        IF (DSUMPR.GT.1.0D0) CALL ARERRM('ARQQ2O',26,0)
        IF (DSUMPR.LT.PYR(0)) GOTO 300
        DPR=PYR(0)*DSUMPR
        DO 320 ICH=1,NCH
          IF (DPR.LT.0.0D0) GOTO 320
          DPR=DPR-DPROP(ICH)
          IF (DPR.GE.0.0D0) GOTO 320
          CALL ARGOQQ(IPQ1(IP),IPQ2(IP),IOENT(ICH))
          DO 330 IPP=IP+1,IPAIR
            IF (IPQ1(IPP).EQ.IPQ1(IP).OR.IPQ1(IPP).EQ.IPQ2(IP))
     $           IPQ1(IPP)=0
            IF (IPQ2(IPP).EQ.IPQ1(IP).OR.IPQ2(IPP).EQ.IPQ2(IP))
     $           IPQ2(IPP)=0
 330      CONTINUE
          IPQ1(IP)=-IPQ1(IP)
          IPQ2(IP)=-IPQ2(IP)
 320    CONTINUE
 300  CONTINUE

      DO 400 IP=1,IPAIR
        IF (IPQ1(IP).LT.0) CALL ARREMP(-IPQ1(IP))
        IF (IPQ2(IP).LT.0) CALL ARREMP(-IPQ2(IP))
 400  CONTINUE

      RETURN

C**** END OF ARQQ2O ****************************************************
      END
C***********************************************************************
C $Id: arqq2o.f,v 1.3 2000/02/15 14:02:36 leif Exp $

      SUBROUTINE ARQQCF(IQ1,IQ2,MODE,ICREP,NGP)

C...ARiadne subroutine QQbar ConFiguration.

C...Find the correct (or at least most likely) spin and colour
C...configuration of a heavy q-qbar pair returning the production MODE,
C...the colour representation (1 or 8) and number of emitted gluons
C...since created (IGP).

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'pyjets.f'

      MODE=0
      NGP=0
      ICREP=8
      IF (IDI(IQ1).GT.0) THEN
        IF (.NOT.QEM(IDI(IQ1)).AND.IP1(IDI(IQ1)).EQ.IQ2) ICREP=1
      ENDIF
      IF (IDO(IQ1).GT.0) THEN
        IF (.NOT.QEM(IDO(IQ1)).AND.IP3(IDO(IQ1)).EQ.IQ2) ICREP=1
      ENDIF

      NGP=ABS(INQ(IQ1))/10000+ABS(INQ(IQ2))/10000

      IF (INO(IQ1).NE.0.AND.INO(IQ1).EQ.INO(IQ2)) THEN
        MODE=2
        RETURN
      ELSEIF (INO(IQ1).NE.0.OR.INO(IQ2).NE.0) THEN
        RETURN
      ENDIF

      IF (MSTA(1).EQ.1) THEN
        MODE=1
        RETURN
      ENDIF

      IF (MSTA(1).NE.2) RETURN

      IJ1=MOD(-INQ(IQ1),10000)
      IJ2=MOD(-INQ(IQ2),10000)

      RETURN

C**** END OF ARQQCF ****************************************************
      END
C***********************************************************************
C $Id: arqq2o.f,v 1.3 2000/02/15 14:02:36 leif Exp $

      SUBROUTINE ARGOQQ(IQ1,IQ2,IONI)

C...ARiadne subroutine Generate Onium from QQbar pair.

C...Join a heavy q-qbar pair into an Onium according to IONI.

      INCLUDE 'arimpl.f'
      INCLUDE 'aronia.f'
      INCLUDE 'arpart.f'

      DIMENSION IP(MAXPAR),DOLD(4)


C...Sum up all energy momentum available
      NP=0
      DO 100 J=1,4
        DOLD(J)=0.0D0
 100  CONTINUE
      DO 110 I=1,IPART
        DO 120 J=1,4
          DOLD(J)=DOLD(J)+BP(I,J)
 120    CONTINUE
        IF (I.EQ.IQ1.OR.I.EQ.IQ2) GOTO 110
        NP=NP+1
        IP(NP)=I
 110  CONTINUE


      IFLO=IFLONI(IONI)
      DMO=PYMASS(IFLO)
      DMO2=DMO**2
      NG=PONI(IONI,4)+0.5D0


C...Conserve energy by shuffling a bit.
C      CALL ARCHEM(DOLD(1),DOLD(2),DOLD(3),DOLD(4),NP,IP)

      RETURN

C**** END OF ARGOQQ ****************************************************
      END
