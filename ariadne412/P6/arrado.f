C***********************************************************************
C $Id: arrado.f,v 3.14 2001/11/23 12:02:25 leif Exp $

      SUBROUTINE ARRADO(ID)

C...ARiadne subroutine RADiate Onium

C...Performs the radiation of an onium from dipole ID

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'arhide.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'aronia.f'
      INCLUDE 'arint2.f'
      INCLUDE 'arint3.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'ardble.f'


      CALL ARBOCM(ID)

      IONI=IRAD(ID)+100000
      IDIR=1
      IPP=IP1(ID)
      IPR=IP3(ID)
      IF (IONI.LT.0) THEN
        IPP=IP3(ID)
        IPR=IP1(ID)
        IDIR=-1
        IONI=-IONI
      ENDIF
      DS=SDIP(ID)
      DW=SQRT(DS)

C...Get momentum of Onium
      DM=PYMASS(IFLONI(IONI))
      DM2=DM**2
      DMT2=PT2IN(ID)
      IF (MSTA(38).LT.0) DMT2=DMT2+DM2
      DMT=SQRT(DMT2)
      DY=AEX1(ID)
      DEY=EXP(DY)
      DPO=DMT*DEY
      DMO=DMT/DEY
      DPHI=PYR(IDUM)*ARX2DB(PARU(2))
      DPT=SQRT(MAX(DMT2-DM2,0.0D0))
      DPX=DPT*SIN(DPHI)
      DPY=DPT*COS(DPHI)

C...Put Onium in /LUJETS/
      N=N+1
      K(N,1)=1
      K(N,2)=IFLONI(IONI)
      K(N,3)=IMF
      K(N,4)=0
      K(N,5)=IO
      P(N,1)=ARDB2X(DPX)
      P(N,2)=ARDB2X(DPY)
      P(N,3)=ARDB2X(IDIR*0.5D0*(DPO-DMO))
      P(N,4)=ARDB2X(0.5D0*(DPO+DMO))
      P(N,5)=ARDB2X(DM)

C...Rotate onium to global frame
      CALL PYROBO(N,N,THE,PHI,DBEX,DBEY,DBEZ)

C...Fix Energy momentum check
      DO 100 J=1,4
        DPTOT(J)=DPTOT(J)-ARX2DB(P(N,J))
 100  CONTINUE
      DPTOT(5)=SQRT(MAX(DPTOT(4)**2-DPTOT(3)**2-
     $                  DPTOT(2)**2-DPTOT(1)**2,0.0D0))

      DB=(DW-DPO)*(DW-DMO)
      DMR2=BP(IPR,5)**2
      DMP2=BP(IPP,5)**2

C...If requested, get mass of gluons for subsequent splitting
      IF (ABS(MSTA(38)).EQ.2.AND.IFL(IPP).EQ.21.AND.
     $     Q2GONI(IONI).AND.AEX3(ID).GT.DBLE(PARA(3))**2) THEN
        DMP2=AEX3(ID)
      ENDIF

C...Calculate momentum of spectator
      DMTP2=DMP2+DPT**2
      DA=DB+DMR2-DMTP2
      DMR=0.5D0*(DA+SQRT(MAX(DA**2-4.0D0*DMR2*DB,0.0D0)))/(DW-DPO)
      DPR=DMR2/DMR

      BP(IPR,1)=0.0D0
      BP(IPR,2)=0.0D0
      BP(IPR,3)=IDIR*0.5D0*(DPR-DMR)
      BP(IPR,4)=0.5D0*(DPR+DMR)

C...Calculate momentum of emitter
      DPP=DW-DPR-DPO
      DMP=DW-DMR-DMO
      BP(IPP,1)=-DPX
      BP(IPP,2)=-DPY
      BP(IPP,3)=IDIR*0.5D0*(DPP-DMP)
      BP(IPP,4)=0.5D0*(DPP+DMP)
      BP(IPP,5)=SQRT(DMP2)

C...Boost back and flagg neighboring dipoles
      CALL AROBO2(THE,PHI,DBEX,DBEY,DBEZ,IPP,IPR)
      IF (IDO(IP3(ID)).GT.0) QDONE(IDO(IP3(ID)))=.FALSE.
      IF (IDI(IP1(ID)).GT.0) QDONE(IDI(IP1(ID)))=.FALSE.
      QDONE(ID)=.FALSE.

      MHAR(139)=1

      CALL ARCHEM(0)

      IF (ABS(MSTA(38)).NE.2.OR.IFL(IPP).NE.21.OR.
     $     (.NOT.Q2GONI(IONI)).OR.AEX3(ID).LE.DBLE(PARA(3))**2)
     $     RETURN

C...Boost to CMS of massive gluon, split it into two and boost back
      DBEX=BP(IPP,1)/BP(IPP,4)
      DBEY=BP(IPP,2)/BP(IPP,4)
      DBEZ=BP(IPP,3)/BP(IPP,4)
      CALL AROBO1(0.0D0,0.0D0,-DBEX,-DBEY,-DBEZ,IPP)
      CALL ARADDG(ID,IDIR)
      DP=BP(IPP,5)*0.5D0
      BP(IPP,1)=0.0D0
      BP(IPP,2)=0.0D0
      BP(IPP,3)=DP
      BP(IPP,4)=DP
      BP(IPP,5)=0.0D0
      BP(IPART,1)=0.0D0
      BP(IPART,2)=0.0D0
      BP(IPART,3)=-DP
      BP(IPART,4)=DP
      BP(IPART,5)=0.0D0
      THE=ACOS(1.0D0-2.0D0*PYR(IDUM))
      PHI=PYR(IDUM)*ARX2DB(PARU(2))
      CALL AROBO2(THE,PHI,DBEX,DBEY,DBEZ,IPP,IPART)

      RETURN

C**** END OF ARRADO ****************************************************
      END
C***********************************************************************
C $Id: arrado.f,v 3.14 2001/11/23 12:02:25 leif Exp $

      DOUBLE PRECISION FUNCTION AR2GDI(R,Z,U)

C...ARiadne function 2 Gluon invariant mass DIstribution

C...Returns the probability distribution for a given invariant mass of
C...the two gluons accompanying colour singlet onium production.

      IMPLICIT DOUBLE PRECISION(A-H, O-Z)


      AR2GDI=0.0D0
      IF (1.0D0-2.0D0*R+R**2-2.0D0*U-2.0D0*R*U+U**2.LT.0.0D0) RETURN

      F0=(R-1.0D0)**5*R*(1.0D0+R)/8.0D0+
     $     (R-1.0D0)**3*(1.0D0+R)**2*U/4.0D0+
     $     (8.0D0-9.0D0*R-36.0D0*R**2+50.0D0*R**3-
     $      4.0D0*R**4-9.0D0*R**5)*U**2/8.0D0+
     $     (-5.0D0+10.0D0*R+32.0D0*R**2+10.0D0*R**3+
     $      9.0D0*R**4)*U**3/4.0D0-
     $     R*(23.0D0+24.0D0*R+17.0D0*R**2)*U**4/8.0D0+
     $     (5.0D0+9.0D0*R+6.0D0*R**2)*U**5/4.0D0+
     $     (-1.0D0-7.0D0*R/8.0D0)*U**6+U**7/4.0D0
 
      F1=(1.0D0-R)**5*(1.0D0+R)/4.0D0+
     $     (1.0D0-R)**3*(1.0D0+6.0D0*R+R**2)*U/4.0D0+
     $     (-2.0D0+5.0D0*R-5.0D0*R**3+2.0D0*R**4)*U**2+
     $     (3.0D0-21.0D0*R-R**2-5.0D0*R**3)*U**3/2.0D0+
     $     (5.0D0+28.0D0*R+7.0D0*R**2)*U**4/4.0D0+
     $     (-7.0D0-5.0D0*R)*U**5/4.0D0+U**6/2.0D0
 
      F2=(-1.0D0+R)**5/4.0D0+(-1.0D0/2.0D0+R-R**3+R**4/2.0D0)*U+
     $     (3.0D0-11.0D0*R+11.0D0*R**2-3.0D0*R**3)*U**2/2.0D0+
     $     R*(5.0D0+R)*U**3+
     $     (-5.0D0-3.0D0*R)*U**4/4.0D0+U**5/2.0D0
 
      G0=(1.0D0-R)**5*(1.0D0-R+3.0D0*R**2+R**3)/8.0D0+
     $     (-1.0D0+R)**3*
     $      (3.0D0-5.0D0*R-6.0D0*R**2+5.0D0*R**3+R**4)*U/4.0D0+
     $     (15.0D0-54.0D0*R-5.0D0*R**2+88.0D0*R**3-
     $     31.0D0*R**4-10.0D0*R**5-3.0D0*R**6)*U**2/8.0D0+
     $     (-5.0D0/2.0D0+7.0D0*R+7.0D0*R**2-R**3+
     $     2.0D0*R**4+3.0D0*R**5/2)*U**3+
     $     (15.0D0-34.0D0*R-48.0D0*R**2-
     $      30.0D0*R**3-23.0D0*R**4)*U**4/8.0D0+
     $     (-3.0D0+6.0D0*R+10.0D0*R**2+9.0D0*R**3)*U**5/4.0D0+
     $     (1.0D0-2.0D0*R-5.0D0*R**2)*U**6/8.0D0
 
      G1=(-1.0D0+R)**5*(1.0D0+R)**2/4.0D0+
     $     (1.0D0-R)**3*(5.0D0-3.0D0*R-7.0D0*R**2+R**3)*U/4.0D0+
     $     (-5.0D0+22.0D0*R-19.0D0*R**2-3.0D0*R**3+
     $      4.0D0*R**4+R**5)*U**2/2.0D0+
     $     (5.0D0-27.0D0*R+10.0D0*R**2-
     $      7.0D0*R**3-5.0D0*R**4)*U**3/2.0D0+
     $     (-5.0D0+33.0D0*R+7.0D0*R**2+13.0D0*R**3)*U**4/4.0D0+
     $     (1.0D0-8.0D0*R-5.0D0*R**2)*U**5/4.0D0
 
      G2=(1.0D0-R)**5*(1.0D0+R)/4+
     $     (1.0D0-R)**3*(-2.0D0+3.0D0*R)*U/2.0D0+
     $     (3.0D0-16.0D0*R+20.0D0*R**2-6.0D0*R**3-R**4)*U**2/2.0D0+
     $     (-2.0D0+13.0D0*R-3.0D0*R**2+4.0D0*R**3)*U**3/2.0D0+
     $     (1.0D0-8.0D0*R-5.0D0*R**2)*U**4/4.0D0
 
      FFAC=16.0D0/
     $     ((-R+(1.0D0+R-U)**2/4.0D0)**2*(1.0D0-R+U)**2*(-1.0D0+R+U)**2)

      GFAC=0.0D0
      IF (U.GT.0.0D0) GFAC=16.0D0*U*
     $     LOG((-1.0D0+R+U-
     $          SQRT(1.0D0-2.0D0*R+R**2-2.0D0*U-2.0D0*R*U+U**2))/
     $     (-1.0D0+R+U+
     $          SQRT(1.0D0-2.0D0*R+R**2-2.0D0*U-2.0D0*R*U+U**2)))/
     $     ((-R+(1.0D0+R-U)**2/4.0D0)**(5.0D0/2.0D0)*
     $      (1.0D0-R-U)**3*(1.0D0-R+U)**2)
 
      AR2GDI=FFAC*(F0+Z*F1+Z*Z*F2)+GFAC*(G0+Z*G1+Z*Z*G2)

      RETURN

C**** END OF AR2GDI ****************************************************
      END

