C***********************************************************************
C $Id: arnofl.f,v 3.4 2000/02/15 14:02:33 leif Exp $

      DOUBLE PRECISION FUNCTION ARNOFL(W,MNOFL)

C...ARiadne function Number Of FLavours 

C...Returns the number of flavourspossible at energy W

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat2.f'


      ARNOFL=0.0D0
      DO 100 I=1,MNOFL
        IF (W.LT.2.0D0*PQMAS(I)) RETURN
        ARNOFL=DBLE(I)
 100  CONTINUE

      RETURN

C**** END OF ARNOFL ****************************************************
      END
