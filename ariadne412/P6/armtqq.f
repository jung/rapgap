C***********************************************************************
C $Id: armtqq.f,v 3.10 2001/11/23 12:02:25 leif Exp $

      SUBROUTINE ARMTQQ(KF,KQ,PM,PT2MAX,PT2MIN,X,XQ2,YQ,PHI)

C...ARiadne get PT2 of initial state g->QQ

C...Get kinematical variables describing an initial-state g->qqbar
C...splitting.


      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'ardble.f'


      PHI=0
      T2=XQ2
      PT2MAX=MIN(PT2MAX,0.25D0*PM**2)
      IF (MHAR(102).LT.0) GOTO 900
      RMQ=PYMASS(KQ)
      PT2CUT=PT2MIN
      PT2CUT=MAX(PT2MIN,DBLE(PARA(3))**2+RMQ**2)
      IF (PT2MAX.LE.PT2CUT) GOTO 900

      XNUMFL=MAX(ARNOFL(SQRT(T2/X),MAX(5,MSTA(15))),3.0D0)
      ALPHA0=12.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)
      SQ2MIN=DBLE(PHAR(103))*PT2CUT/DBLE(PARA(21))
      SQ2MAX=DBLE(PHAR(103))*PT2MAX/DBLE(PARA(21))
      IF (MHAR(151).EQ.1) SQ2MIN=MAX(SQ2MIN,XQ2)
      SQ2MIN=MAX(SQ2MIN,4.0D0*RMQ**2)
      STRA0=ARSTRA(KF,KQ,X,1.0D0,SQ2MIN)
      STRA0=MAX(STRA0,ARSTRA(KF,KQ,X,1.0D0,XQ2))
      STRA0=MAX(STRA0,ARSTRA(KF,KQ,X,1.0D0,SQ2MAX))
      
      C=DBLE(PHAR(104))*ALPHA0*STRA0/ARX2DB(PARU(1))
      ZINT=1.0D0-X
      CN=1.0D0/(C*ZINT)
      XLAM2=DBLE((PARA(1)**2)/PHAR(103))

 100  IF (PT2MAX.LE.PT2CUT) GOTO 900
      ARG=PYR(IDUM)
      IF (LOG(ARG)*CN.LT.
     $     LOG(LOG(PT2CUT/XLAM2)/LOG(PT2MAX/XLAM2))) GOTO 900
      PT2MAX=XLAM2*(PT2MAX/XLAM2)**(ARG**CN)

      Z=X+PYR(0)*(1.0D0-X)
      SQ2=DBLE(PHAR(103))*PT2MAX/DBLE(PARA(21))

      W=(Z**2+(1.0D0-Z)**2)*0.25D0
      IF (MHAR(151).EQ.1) THEN
        W=W*MIN(1.0D0,LOG(PT2MAX/XLAM2)/LOG(DBLE(PARA(21)*XQ2)/XLAM2))
        SQ2=MAX(SQ2,DBLE(XQ2))
      ENDIF
      SQ2=MAX(SQ2,SQ2MIN)
      IF (MHAR(113).EQ.1) THEN
        STRA=ARSTRA(KF,KQ,X,Z,SQ2)
        W=W*STRA/STRA0
      ELSE
        BETA=DBLE(PARA(25))
        IF (MSTA(25).EQ.0) BETA=0.0D0
        PTIN=SQRT(DBLE(PHAR(103))*PT2MAX)
        IF (MHAR(113).EQ.2) PTIN=2.0D0*PTIN
        XMU=DBLE(PARA(11))
        ALPHA=DBLE(PARA(10))
        IF (PARA(10).GT.0.0) THEN
          XMU=DBLE(PARA(11))
          ALPHA=DBLE(PARA(10))
        ELSEIF (PTIN.GE.DBLE(ABS(PARA(10)))) THEN
          XMU=DBLE(SQRT(ABS(PARA(10)*PARA(11))))
          ALPHA=2.0D0
        ELSE
          XMU=DBLE(PARA(11))
          ALPHA=1.0D0
        ENDIF
        IF (X/Z.GT.((1.0D0/PYR(IDUM)-1.0D0)**BETA)*(XMU/PTIN)**ALPHA)
     $       GOTO 100
      ENDIF

      IF (W.GT.1.0D0) THEN
        CALL ARERRM('ARPTQQ',22,0)
        RETURN
      ENDIF

      IF (W.LT.PYR(IDUM)) GOTO 100

      IF (MHAR(113).EQ.-1) THEN
        IF (PT2MAX.LT.Z*(1.0D0-X)*XQ2) GOTO 100
        IF (PT2MAX.LT.(1.0D0-Z)*(1.0D0-X)*XQ2) GOTO 100
      ENDIF

      YQ=0.5D0*LOG(PT2MAX*(Z/((1.0D0-Z)*X*PM))**2)
      PHI=ARX2DB(PARU(2))*PYR(IDUM)

      RETURN

 900  PT2MAX=0.0D0
      RETURN

C**** END OF ARMTQQ ****************************************************
      END
