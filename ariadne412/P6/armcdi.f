C***********************************************************************
C $Id: armcdi.f,v 3.7 2001/11/10 01:54:17 leif Exp $

      SUBROUTINE ARMCDI(ARRNDX,ARRNDY,ARVETO)

C...ARiadne subroutine Monte Carlo DIstribution

C...Generates x_1 and x_3 for a radiating dipole

      INCLUDE 'arimpl.f'
      INCLUDE 'arint1.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'


C...Exit if below cut
 100  IF (XT2M.LT.XT2C) GOTO 900
      QFAIL=.FALSE.

C...Generate random XT2
      XT2=ARRNDX()
      IF (XT2.LT.XT2C) GOTO 900
      XT=SQRT(XT2)

C...Generate rapidity Y
      Y=ARRNDY()

C...Calculate energy fractions
      B1=BC1-XT*EXP(Y)
      B3=BC3-XT*EXP(-Y)
      B2=2.0D0-B1-B3

C...Set maximum XT2 for possible next random call (VETO algorithm)
      XT2M=XT2

C...Redo random calls according to veto-algorithm
      IF (QFAIL.OR.ARVETO().LE.PYR(IDUM)) GOTO 100

C...Check that Current values are kinematically allowed
      CALL ARCHKI(0,IOK)
      IF (IOK.EQ.0) GOTO 100

C...Extra colour factor check 2CF/Nc in EFGH model
      IF(MSTA(39).GT.0.AND.XT2M.GE.XT2C) THEN
         IGS=0
         YHALF=LOG(W/DBLE(PARA(1)))
         IF(YG1.GT.YHALF-Y) IGS=1
         IF(YG3.GT.YHALF+Y) IGS=3
         IF(IGS.EQ.0.AND.PYR(IDUMMY).GT.8.0D0/9.0D0) GOTO 100
      ENDIF


      RETURN

C...If below cuts set XT2 to 0
 900  B1=BC1
      B3=BC3
      XT2=0.0D0
      IGS=0

      RETURN

C**** END OF ARMCDI ****************************************************
      END
