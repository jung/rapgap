C***********************************************************************
C $Id: dijoin.f,v 3.4 1998/05/22 09:34:11 leif Exp $
      SUBROUTINE DIJOIN(MODE,P1,P2,P3,IL1,IL3)
C
C Cluster three jets into two
C
C Inputs:
C         INTEGER MODE             Recoil Strategy
C         DOUBLE P1(6),P2(6),P3(6) Initial momenta
C         INTEGER IL1,IL3          Pseudo particle information for
C                                  momenta 1 and 3
C Outputs:
C         DOUBLE P1(6),P3(6)       Momenta of jets 1 and 3 after
C                                  clustering
C
      IMPLICIT NONE
C Subroutine arguments
      INTEGER MODE,IL1,IL3
      DOUBLE PRECISION P1(6),P2(6),P3(6)
C Other variables
      DOUBLE PRECISION DE,DB(3),P1S(6),P3S(6),PHI,PHI2,THE,PSI,BET,
     $     S12,S23,DINVM2
      INTEGER J
C End of declarations
C
C Especially simple for MODE < 0

      IF (MODE.LT.0) THEN
        S12=DINVM2(P1(1),P2(1),6,MODE)
        S23=DINVM2(P3(1),P2(1),6,MODE)
        IF (S12.LT.S23) THEN
          DO 10 J=1,4
            P1(J)=P1(J)+P2(J)
 10       CONTINUE
          P1(5)=SQRT(S12)
          P1(6)=S12
        ELSE
          DO 20 J=1,4
            P3(J)=P3(J)+P2(J)
 20       CONTINUE
          P3(5)=SQRT(S23)
          P3(6)=S23
        ENDIF
        RETURN
      ENDIF

C Deal with special cases for pseudoparticles
      IF (IL1.EQ.4.AND.IL3.EQ.4) RETURN
      IF (IL1.EQ.4) THEN
        DO 100 J=1,6
          P1S(J)=P1(J)
 100    CONTINUE
      ENDIF
      IF (IL3.EQ.4) THEN
        DO 110 J=1,6
          P3S(J)=P3(J)
 110    CONTINUE
      ENDIF
      IF (IL1.GE.3.AND.IL3.GE.3) THEN
        DE=P1(4)+P3(4)
        DO 200 J=1,3
          DB(J)=-(P1(J)+P3(J))/DE
 200    CONTINUE
      ELSE
        DE=P1(4)+P2(4)+P3(4)
        DO 210 J=1,3
          DB(J)=-(P1(J)+P2(J)+P3(J))/DE
 210    CONTINUE
      ENDIF
C
C Boost particles to CMS
      IF (DB(1)**2+DB(2)**2+DB(3)**2.GE.1.0D0) THEN
        DO 220 J=1,4
          P1(J)=P1(J)+P2(J)*0.5D0
          P3(J)=P3(J)+P2(J)*0.5D0
 220    CONTINUE
        P1(5)=0.0D0
        P1(6)=0.0D0
        P3(5)=0.0D0
        P3(6)=0.0D0
        RETURN
      ENDIF
      CALL DIBOOS(DB,P1)
      CALL DIBOOS(DB,P2)
      CALL DIBOOS(DB,P3)
C
C Rotate according to recoil strategy
      DE=(P1(4)+P2(4)+P3(4))*0.5D0
      PHI=0.0D0
      IF (P1(2).NE.0.0D0.OR.P1(1).NE.0.0D0) PHI=ATAN2(P1(2),P1(1))
      CALL DIRPHI(-PHI,P1)
      CALL DIRPHI(-PHI,P3)
      THE=0.0D0
      IF (P1(3).NE.0.0D0.OR.P1(1).NE.0.0D0) THE=ATAN2(P1(1),P1(3))
      CALL DIRTHE(-THE,P1)
      CALL DIRTHE(-THE,P3)
C
      IF (IL1.LT.3.OR.IL3.LT.3) THEN
        PHI2=0.0D0
        IF (P3(2).NE.0.0D0.OR.P3(1).NE.0.0D0) PHI2=ATAN2(P3(2),P3(1))
        CALL DIRPHI(-PHI2,P1)
        CALL DIRPHI(-PHI2,P3)
        IF (IL1.GT.1.OR.(ABS(MODE).GE.1.AND.P1(4).GE.P3(4))) THEN
          PSI=0.0D0
        ELSE
          BET=-ATAN2(P3(1),-P3(3))
          IF (IL3.GT.1.OR.(ABS(MODE).GE.1.AND.P1(4).LT.P3(4))) THEN
            PSI=BET
          ELSE
            PSI=BET*(P3(4)**2)/(P1(4)**2+P3(4)**2)
          ENDIF
        ENDIF
      ELSE
        PHI2=0.0D0
        PSI=0.0D0
      ENDIF
C
C Set new momenta of remaining jets
      P1(1)=0.0D0
      P1(2)=0.0D0
      P1(3)=DE
      P1(4)=DE
      P1(5)=0.0D0
      P1(6)=0.0D0
      P3(1)=0.0D0
      P3(2)=0.0D0
      P3(3)=-DE
      P3(4)=DE
      P3(5)=0.0D0
      P3(6)=0.0D0
C
C Boost back
      DO 300 J=1,3
        DB(J)=-DB(J)
 300  CONTINUE
      CALL DIRTHE(PSI,P1)
      CALL DIRTHE(PSI,P3)
      CALL DIRPHI(PHI2,P1)
      CALL DIRPHI(PHI2,P3)
      CALL DIRTHE(THE,P1)
      CALL DIRTHE(THE,P3)
      CALL DIRPHI(PHI,P1)
      CALL DIRPHI(PHI,P3)
      CALL DIBOOS(DB,P1)
      CALL DIBOOS(DB,P3)
C
C Reset jets to original values for special pseudoparticles
      IF (IL1.EQ.4) THEN
        DO 400 J=1,6
          P1(J)=P1S(J)
 400    CONTINUE
      ENDIF
      IF (IL3.EQ.4) THEN
        DO 410 J=1,6
          P3(J)=P3S(J)
 410    CONTINUE
      ENDIF
C
      RETURN
C
      END
C
C***********************************************************************
