C***********************************************************************
C $Id: arpcms.f,v 3.5 2000/02/15 14:02:34 leif Exp $

      DOUBLE PRECISION FUNCTION ARPCMS(S,SM1,SM2)

C...ARiadne function get Positive ligth-cone component in CMS

C...Returns the positive light-cone  component of a particle momentum
C...when placed in the cms system of itself and an other particle given
C...the two particle masses SM1 and SM2 and the total energy squared S

      INCLUDE 'arimpl.f'

      ARPCMS=-1
      A1=0.25D0*(S-(SM1+SM2)**2)
      IF (A1.LT.0.0D0) RETURN
      A2=(S-(SM1-SM2)**2)/S
      ARPCMS=SQRT(A1*A2)+SQRT(A1*A2+SM1**2)

      RETURN

C**** END OF ARPCMS ****************************************************
      END
C***********************************************************************
C $Id: arpcms.f,v 3.5 2000/02/15 14:02:34 leif Exp $

      SUBROUTINE ARDCMS(DS,DSM1,DSM2,DPP)

C...ARiadne subroutine Double positive ligth-cone component in CMS

C...Calculates the positive light-cone component of a particle momentum
C...when placed in the cms system of itself and an other particle given
C...the two particle masses DSM1 and DSM2 and the total energy squared DS
C...All variables are in double precision.

      INCLUDE 'arimpl.f'

      DPP=-1.0D0
      DA1=0.25D0*(DS-(DSM1+DSM2)**2)
      IF (DA1.LT.0.0D0) RETURN
      DA2=(DS-(DSM1-DSM2)**2)/DS
      DPP=SQRT(DA1*DA2)+SQRT(DA1*DA2+DSM1**2)

      RETURN

C**** END OF ARDCMS ****************************************************
      END
