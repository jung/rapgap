C***********************************************************************
C $Id: arpoki.f,v 3.7 2001/11/23 12:02:25 leif Exp $

      SUBROUTINE ARPOKI(IT,IQR,IDR,IRP,IDIR,KFTF,KFPR,XPOM,TPOM,QFAIL)

C...ARiadne subroutine POmeron KInematics

C...Redistribute remnants assuming an incoming hadron IT, outgoing
C...hadron KFTF with a pomeron (or other colourless  sub object) with
C...momentum fraction XPOM and viruality -TPOM giving a remnant of
C...flavour KFPR. IQR, IDR and IRP as in subroutine ARREMN


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'leptou.f'
      INCLUDE 'pypars.f'
      INCLUDE 'ardble.f'


      QFAIL=.TRUE.

C...Sum up remnant momentum
      BRX=0.0D0
      BRY=0.0D0
      BRZ=0.0D0
      BRE=0.0D0
      IF (IQR.GT.0) THEN
        IF (IDR.GT.0.AND.KFPR.NE.21) CALL ARERRM('ARPOKI',30,0)
        BRX=BRX+BP(IQR,1)
        BRY=BRY+BP(IQR,2)
        BRZ=BRZ+BP(IQR,3)
        BRE=BRE+BP(IQR,4)
      ENDIF
      IF (IDR.GT.0) THEN
        IF (IQR.GT.0.AND.KFPR.NE.21) CALL ARERRM('ARPOKI',30,0)
        BRX=BRX+BP(IDR,1)
        BRY=BRY+BP(IDR,2)
        BRZ=BRZ+BP(IDR,3)
        BRE=BRE+BP(IDR,4)
      ENDIF
      IF (IRP.GT.0) THEN
        IF (IQR.GT.0.AND.IDR.GT.0) CALL ARERRM('ARPOKI',30,0)
        BRX=BRX+ARX2DB(P(IRP,1))
        BRY=BRY+ARX2DB(P(IRP,2))
        BRZ=BRZ+ARX2DB(P(IRP,3))
        BRE=BRE+ARX2DB(P(IRP,4))
      ENDIF

      IF (KFPR.EQ.21) THEN
        IF (IQR.LE.0.OR.IDR.LE.0) CALL ARERRM('ARPOKI',30,0)
        IR=MIN(IQR,IDR)
      ELSE
        IR=MAX(IQR,IDR)
      ENDIF

C...Select particle to fix momentum transfer with
      IF (QQ(MAXPAR-2)) THEN
        ISQ=MAXPAR-2
      ELSEIF (IDI(IR).GT.0) THEN
        IF (IDO(IR).GT.0.AND.QEX(IP1(IDI(IR)))) THEN
          ISQ=IP3(IDO(IR))
        ELSE
          ISQ=IP1(IDI(IR))
        ENDIF
      ELSE
        ISQ=IP3(IDO(IR))
      ENDIF

      RM2PI=ARX2DB(P(IT,5)**2)
      RMPF=PYMASS(KFTF)
      RM2PF=RMPF**2
      PT2PF=TPOM*(1.0D0-XPOM)+XPOM*(RM2PI*(1.0D0-XPOM)-RM2PF)
      IF (PT2PF.LT.0.0D0) RETURN

      PHIPF=ARX2DB(PARU(2))*PYR(0)
      PTXPF=SQRT(PT2PF)*COS(PHIPF)
      PTYPF=SQRT(PT2PF)*SIN(PHIPF)
      PPI=ARX2DB(P(IT,4)+IDIR*P(IT,3))
      PPF=(1.0D0-XPOM)*PPI
      RMT2PF=RM2PF+PT2PF
      K(N+1,1)=1
      K(N+1,2)=KFTF
      K(N+1,3)=IT
      K(N+1,4)=0
      K(N+1,5)=0
      P(N+1,1)=ARDB2X(PTXPF)
      P(N+1,2)=ARDB2X(PTYPF)
      P(N+1,3)=ARDB2X(IDIR*0.5D0*(PPF-RMT2PF/PPF))
      P(N+1,4)=ARDB2X(0.5D0*(PPF+RMT2PF/PPF))
      P(N+1,5)=ARDB2X(RMPF)

C...Calculate boost to struck pomeron CMS and check kinematics
      
      RMPR=PYMASS(KFPR)
      RMT2PR=RMPR**2
      RMT2SQ=(BRX+BP(ISQ,1)-PTXPF)**2+
     $     (BRY+BP(ISQ,2)-PTYPF)**2+BP(ISQ,5)**2
      BSE=BRE+BP(ISQ,4)-ARX2DB(P(N+1,4))
      BSZ=BRZ+BP(ISQ,3)-ARX2DB(P(N+1,3))
      BSY=BRY+BP(ISQ,2)-ARX2DB(P(N+1,2))
      BSX=BRX+BP(ISQ,1)-ARX2DB(P(N+1,1))
      S=BSE**2-BSZ**2-BSY**2-BSX**2
      DBZ=BSZ/BSE
      
      IF (S.LT.0) RETURN
      BW=SQRT(S)
      IF (SQRT(RMT2PR)+SQRT(RMT2SQ).GE.BW) RETURN

 100  IF (PARA(16).GT.0.0) THEN
        PTI=DBLE(PARA(16))*SQRT(-LOG(PYR(0)))
        IF (PTI.GT.DBLE(PARA(17))) GOTO 100
      ELSEIF (MSTA(1).EQ.2) THEN
        IF(MSTP(91).LE.0) THEN
          PTI=0.D0
        ELSEIF(MSTP(91).EQ.1) THEN
          PTI=ARX2DB(PARP(91))*SQRT(-LOG(PYR(0)))
        ELSE
          RPT1=PYR(0)
          RPT2=PYR(0)
          PTI=-ARX2DB(PARP(92))*LOG(RPT1*RPT2)
        ENDIF
        IF(PTI.GT.ARX2DB(PARP(93))) GOTO 100
      ELSE
        PTI=DBLE(PARL(3))*SQRT(-LOG(PYR(0)))
      ENDIF
      PHII=ARX2DB(PARU(2))*PYR(0)
      PTIX=PTI*COS(PHII)
      PTIY=PTI*SIN(PHII)

      RMT2PR=PTI**2+RMPR**2
      RMTPR=SQRT(RMT2PR)
      RMT2SQ=(BSX-PTIX)**2+(BSY-PTIY)**2+BP(ISQ,5)**2
      RMTSQ=SQRT(RMT2SQ)
      IF (RMTPR+RMTSQ.GE.BW) GOTO 100
      PT2SQ=BSX**2+BSY**2
      PZTOT=ARZCMS(S+PT2SQ,RMTSQ,RMTPR)
      IF (PZTOT.LT.0.0D0) GOTO 100

      BP(ISQ,1)=BSX-PTIX
      BP(ISQ,2)=BSY-PTIY
      BP(ISQ,3)=-IDIR*PZTOT
      BP(ISQ,4)=SQRT(PZTOT**2+RMT2SQ)

      IF (KFPR.EQ.21) CALL ARJOQQ(IQR,IDR)

      BP(IR,1)=PTIX
      BP(IR,2)=PTIY
      BP(IR,3)=IDIR*PZTOT
      BP(IR,4)=SQRT(PZTOT**2+RMT2PR)
      BP(IR,5)=RMPR

      IFL(IR)=KFPR
      QEX(IR)=.TRUE.
      XPMU(IR)=DBLE(PARA(14))*MAX(PTI,SQRT(TPOM))
      XPA(IR)=DBLE(PARA(15))
      PT2GG(IR)=0.0D0
      IF (IDI(IR).GT.0) QDONE(IDI(IR))=.FALSE.
      IF (IDO(IR).GT.0) QDONE(IDO(IR))=.FALSE.

      CALL AROBO2(0.0D0,0.0D0,0.0D0,0.0D0,DBZ,ISQ,IR)

      N=N+1
      IF (IRP.GT.0) THEN
        IF (K(IRP,1).LT.10) K(IRP,1)=K(IRP,1)+10
        IRP=-IRP
      ENDIF
      QFAIL=.FALSE.

      RETURN

C**** END OF ARPOKI ****************************************************
      END
