C***********************************************************************
C $Id: dirthe.f,v 3.1 1996/03/08 09:56:18 leif Exp $
      SUBROUTINE DIRTHE(THE,P)
C
C Rotate around y-axis
C
C Inputs:
C         DOUBLE THE          Rotation angle
C         DOUBLE P(3)         Vector to rotate
C
C Outputs:
C         DOUBLE P(3)         Rotated vector
C
      IMPLICIT NONE
C Subroutine arguments
      DOUBLE PRECISION THE,P(3)
C Other variables
      DOUBLE PRECISION CTHE,STHE,PX,PZ
C End of declarations
C
      IF (ABS(THE).LE.1.0D-20) RETURN
C
      STHE=SIN(THE)
      CTHE=COS(THE)
      PX=P(1)
      PZ=P(3)
      P(3)=CTHE*PZ-STHE*PX
      P(1)=STHE*PZ+CTHE*PX
C
      RETURN
C
      END
C
C***********************************************************************
