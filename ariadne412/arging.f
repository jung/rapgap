C***********************************************************************
C $Id: arging.f,v 3.25 2001/12/05 13:29:23 leif Exp $

      SUBROUTINE ARGING(ID,IRP)

C...ARiadne Generate INitial state G->QQ

C...Generate kinematical variables describing an initial-state g->qqbar
C...splitting.


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'arlist.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'leptou.f'

      INTEGER NTT,NTE1,NTE2

      INCLUDE 'ardble.f'

      DATA NTT/0/,NTE1/0/,NTE2/0/


      CY=0.0D0
      CQ=0.0D0
      THEMAX=0.0D0
      YINT=0.0D0
      ZQ=0.0D0
      STRA=0.0D0

      IF (MHAR(120).NE.0) THEN
        CALL ARGIG2(ID,IRP)
        RETURN
      ENDIF

      IF (MHAR(102).LT.0) RETURN

      QEXDIS=(MSTA(1).EQ.3.AND.IO.EQ.0)

      IR=INQ(IRP)
      IT=IRP+5-MAXPAR

      IF (IRAD(ID).EQ.10000+IRP) THEN
        PT2IN(ID)=PT2SAV(IT)
        IRAD(ID)=IRASAV(IT)
        AEX1(ID)=A1SAVE(IT)
        AEX3(ID)=A3SAVE(IT)
        BX1(ID)=B1SAVE(IT)
        BX3(ID)=B3SAVE(IT)
      ENDIF

      NPREM=2
      IPREM(1)=IRP
      IPREM(2)=IR
      IDIR=IRDIR(IT)
      KQ=IDO(IRP)
      KF=K(IT,2)
      RMQ=PYMASS(KQ)
      PM=ARX2DB(P(IT,4)+IDIR*P(IT,3))
      PT2CUT=MAX(DBLE(PARA(3))**2+RMQ**2,PT2IN(ID))/DBLE(PHAR(103))

      IF (NPTOT.EQ.0) THEN
        DO 10 I=1,IPART
          IPTOT(I)=I
 10     CONTINUE
        NPTOT=IPART
      ENDIF

      IF (MHAR(103).GT.0) THEN
        NPSTQ=0
        DO 20 I=1,NPTOT
          IF (INO(IPTOT(I)).NE.0) THEN
            NPSTQ=NPSTQ+1
            IPSTQ(NPSTQ)=IPTOT(I)
          ENDIF
 20     CONTINUE
        CALL ARPADD(-IDI(IRP),NPSTQ,IPSTQ)
      ELSE
        NPSTQ=1
        IPSTQ(1)=IDI(IRP)
      ENDIF

      CALL ARSUME(0,DXR,DYR,DZR,DER,DMR,NPREM,IPREM)
      CALL ARSUME(0,DXQ,DYQ,DZQ,DEQ,DMQ,NPSTQ,IPSTQ)
      B0P=DEQ-IDIR*DZQ
      B0M=DEQ+IDIR*DZQ
      BRP=DER-IDIR*DZR
      BRM=DER+IDIR*DZR

      XX=1.0D0-BRM/PM
      XXY=0.0D0
      XXQ2=0.0D0
      IF (QEXDIS) THEN
        XX=DBLE(X)
        XXY=DBLE(XY)
        XXQ2=DBLE(XQ2)
      ENDIF

      PT2MX=MIN((SQRT(DXQ**2+DYQ**2+DZQ**2)-
     $     SQRT(DXQ**2+DYQ**2))**2,PT2LST)
      IF (MHAR(119).GT.0) THEN
        RMTQ=SQRT(B0P*B0M)
        STOT=(DEQ+DER)**2-(DZQ+DZR)**2-(DYQ+DYR)**2-(DXQ+DXR)**2
        PT2MX=MIN(ARZCMS(STOT,RMTQ,RMQ)**2,PT2LST)
      ENDIF
      IF (PT2MX.LE.PT2CUT) GOTO 900
      IF (QEXDIS) THEN
        SQ2MAX=0.5D0*(XXQ2+PT2MX)
        SQ2MIN=PT2CUT/
     $       ((0.5D0+SQRT(MAX(0.25D0-PT2CUT*XX/(XXQ2*(1.0D0-XX)), 
     & 0.0D0)))*(1.0D0-XX))
      ELSE
        SQ2MAX=PT2MX+XX*B0P*PM
        SQ2MIN=PT2CUT/(1.0D0-XX)
      ENDIF

      XNUMFL=MAX(ARNOFL(SQRT(SQ2MAX),MAX(5,MSTA(15))),3.0D0)
      ALPHA0=12.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)
      IF (MHAR(118).EQ.0) THEN
        STRA0=ARSTRA(KF,KQ,XX,1.0D0,SQ2MIN)
        DO 30 IQ2=1,20
          SQ2=EXP(LOG(SQ2MIN) 
     & +DBLE(IQ2)*(LOG(SQ2MAX)-LOG(SQ2MIN))/20.0D0)
          STRA0=MAX(STRA0,ARSTRA(KF,KQ,XX,1.0D0,SQ2))
 30     CONTINUE
      ELSE
        STRA0=ARSTRA(KF,KQ,XX,1.0D0,SQ2MAX)*DBLE(MHAR(118))
      ENDIF
      IF (STRA0.LE.0.0D0) THEN
        GOTO 900
      ENDIF

      C=DBLE(PHAR(104))*ALPHA0*STRA0/ARX2DB(PARU(1))
      IF (MHAR(152).NE.0) C=C*ARWGHT(10000+IRP)
      ZINT=1.0D0-XX
      CN=1.0D0/(C*ZINT)
      XLAM2=DBLE((PARA(1)**2)/PHAR(103))
      IF (QEXDIS) THEN
        CY=(1.0D0-XXY)/(1.0D0+(1.0D0-XXY)**2)
        CQ=0.125D0+0.25D0*CY
        C=DBLE(PHAR(104))*0.25D0*ALPHA0*STRA0*CQ/ARX2DB(PARU(1))
        IF (MHAR(152).NE.0) C=C*ARWGHT(10000+IRP)
        THEMAX=PT2MX
        YINT=4.0D0*LOG(SQRT(PT2MX/PT2CUT)+SQRT(PT2MX/PT2CUT-1.0D0))
        CN=1.0D0/(YINT*C)
      ENDIF

 100  IF (PT2MX.LE.PT2CUT) GOTO 900
      ARG=PYR(IDUM)
      IF (LOG(ARG)*CN.LT.
     $     LOG(LOG(PT2CUT/XLAM2)/LOG(PT2MX/XLAM2))) GOTO 900
      PT2MX=XLAM2*(PT2MX/XLAM2)**(ARG**CN)

      IF (QEXDIS) THEN
        YMAX=2.0D0*LOG(SQRT(THEMAX/PT2MX)+SQRT(THEMAX/PT2MX-1.0D0))
        Y=(PYR(IDUM)*2.0D0-1.0D0)*YMAX
        ZQ=1.0D0/(1.0D0+EXP(-Y))
        IF (MHAR(102).EQ.2) THEN
          Z=XXQ2*ZQ*(1.0D0-ZQ)/(PT2MX+XXQ2*ZQ*(1.0D0-ZQ))
        ELSE
          Z=ZQ*(1.0D0-ZQ)*XXQ2/PT2MX
        ENDIF
        IF (Z.LE.XX.OR.Z.GE.1.0D0) GOTO 100
        SQ2=PT2MX/
     $     ((0.5D0+SQRT(MAX(0.25D0-PT2MX*Z/(XXQ2*(1.0D0-Z)), 
     & 0.0D0)))*(1.0D0-Z))
        W=2.0D0*YMAX/YINT
        W=W*(Z*(1.0D0-Z)*(Z**2+(1.0D0-Z)**2)*(ZQ**2+(1.0D0-ZQ)**2)+
     $       16.0D0*((Z*(1.0D0-Z))**2)*ZQ*(1.0D0-ZQ)*CY)/CQ
        IF (MHAR(151).EQ.1) THEN
          W=W*MIN(1.0D0,LOG(PT2MX/XLAM2)/LOG(DBLE(PARA(21))*XQ2/XLAM2))
          SQ2=MAX(SQ2,XXQ2)
        ENDIF
      ELSE
        Z=XX+PYR(0)*(1.0D0-XX)
        W=(Z**2+(1.0D0-Z)**2)*0.25D0
        IF (MHAR(119).EQ.0.AND.
     $       Z.GE.1.0D0/(1.0D0+PT2MX/(XX*B0P*PM))) GOTO 100
        SQ2=PT2MX/(1.0D0-Z)
      ENDIF

      IF (MHAR(113).EQ.1) THEN
        STRA=ARSTRA(KF,KQ,XX,Z,SQ2)
        IF (MHAR(118).EQ.0.AND.STRA.LE.0.0D0) GOTO 100
        IF (STRA.LT.0.0D0) THEN
          GOTO 100
        ENDIF
        W=W*STRA/STRA0
      ELSE
        BETA=DBLE(PARA(25))
        IF (MSTA(25).EQ.0) BETA=0.0D0
        PTIN=SQRT(DBLE(PHAR(103))*PT2MX)
        IF (MHAR(113).EQ.2) PTIN=2.0D0*PTIN
        XMU=DBLE(PARA(11))
        ALPHA=DBLE(PARA(10))
        IF (PARA(10).GT.0.0) THEN
          XMU=DBLE(PARA(11))
          ALPHA=DBLE(PARA(10))
        ELSEIF (PTIN.GE.DBLE(ABS(PARA(10)))) THEN
          XMU=DBLE(SQRT(ABS(PARA(10)*PARA(11))))
          ALPHA=2.0D0
        ELSE
          XMU=DBLE(PARA(11))
          ALPHA=1.0D0
        ENDIF
        IF (XX/Z.GT.((1.0D0/PYR(IDUM)-1.0D0)**BETA)*(XMU/PTIN)**ALPHA)
     $       GOTO 100
      ENDIF

      IF (MHAR(118).EQ.0.AND.W.GT.1.0D0) THEN
        CALL ARERRM('ARGING',22,0)
        GOTO 900
      ENDIF

      IF (W.LT.PYR(IDUM)) GOTO 100

      IF (MHAR(113).EQ.-1) THEN
        IF (PT2MX.LT.Z*(1.0D0-XX)*XXQ2) GOTO 100
        IF (PT2MX.LT.(1.0D0-Z)*(1.0D0-XX)*XXQ2) GOTO 100
      ENDIF

      IF (QEXDIS) THEN
        YQ=-IDIR*0.5D0*LOG(ZQ*(1.0D0-XX)/((1.0D0-ZQ)*(XX/Z-XX)))
        XA=0.125D0*(1.0D0+(1.0D0-XXY)**2)*(Z**2+(1.0D0-Z)**2)*
     $       (ZQ**2+(1.0D0-ZQ)**2)/(ZQ*(1.0D0-ZQ)) 
     & +2.0D0*(1.0D0-XXY)*Z*(1.0D0-Z)
        XB=0.5D0*XXY*SQRT((1.0D0-XXY)*Z*(1.0D0-Z)/(ZQ*(1.0D0-ZQ)))*
     $       (1.0D0-2.0D0/XXY)*(1.0D0-2.0D0*ZQ)*(1.0D0-2.0D0*Z)
        XC=(1.0D0-XXY)*Z*(1.0D0-Z)
        ABC=ABS(XA)+ABS(XB)+ABS(XC)
 200    PHI=ARX2DB(PARU(2))*PYR(IDUM)
        IF (XA+XB*COS(PHI)+XC*COS(2.0D0*PHI).LT.PYR(IDUM)*ABC) GOTO 200
      ELSE
        YQ=-IDIR*0.5D0*LOG(PT2MX*(Z/((1.0D0-Z)*XX*PM))**2)
        PHI=ARX2DB(PARU(2))*PYR(IDUM)
      ENDIF

      IF (MHAR(119).GT.0) THEN

        YQ=Z
        BM=(1.0D0-XX/Z)*PM
        IF (BM.LT.DMR) GOTO 100
        BPH=B0P+BRP-BRP*BRM/BM
        BMH=B0M+BRM-BM

        DPT2Q=PT2MX-RMQ**2
        RMTQ=SQRT(PT2MX)
        DXS=DXQ-SQRT(DPT2Q)*COS(PHI)
        DYS=DYQ-SQRT(DPT2Q)*SIN(PHI)
        RMTS=SQRT(DMQ**2+DXS**2+DYS**2)
        STOT=BPH*BMH
        DZS=ARZCMS(STOT,RMTS,RMTQ)
        IF (DZS.LT.0.0D0) GOTO 100
        IF (DZS**2+DYS**2+DXS**2.LE.DYQ**2+DXQ**2) GOTO 100

      ELSE
        

        DPT2Q=PT2MX-RMQ**2
        DMT2Q=PT2MX

        BXQ=SQRT(DPT2Q)*COS(PHI)
        BYQ=SQRT(DPT2Q)*SIN(PHI)
        BZQ=SQRT(DMT2Q)*SINH(YQ)
        BEQ=SQRT(DMT2Q)*COSH(YQ)
        BQP=BEQ-IDIR*BZQ
        BQM=BEQ+IDIR*BZQ

        BM0D2=DMQ**2+(DXQ-BXQ)**2+(DYQ-BYQ)**2
        BRQP=B0P+BRP-BQP
        BRQM=B0M+BRM-BQM

        BA=(BRQP*BRQM+BRP*BRM-BM0D2)/(2.0D0*BRQM*BRP)
        BB=BRM*BRQP/(BRP*BRQM)

        IF (BA**2.LT.BB.OR.BA.LE.0.0D0.OR.BRQP.LE.0.0D0 
     & .OR.BRQM.LE.0.0D0)
     $       GOTO 100

        DAR=BA-SQRT(BA**2-BB)

        IF (DAR.LE.1.0D0) GOTO 100

        DQ=SQRT(DXQ**2+DYQ**2+DZQ**2)
        IF (DQ.LE.SQRT((DXQ-BXQ)**2+(DYQ-BYQ)**2)) GOTO 100

      ENDIF

      NTT=NTT+1
      IF (W.GT.1.0D0) THEN
        NTE1=NTE1+1
        IF (MOD(NTE1,10).EQ.0) WRITE(0,*) REAL(NTE1)/REAL(NTT)
      ENDIF
      IF (STRA.GT.STRA0) THEN
        NTE2=NTE2+1
        IF (MOD(NTE2,10).EQ.0) WRITE(0,*) REAL(NTE2)/REAL(NTT)
      ENDIF

      IF (PT2MX*DBLE(PHAR(103)).GT.PT2IN(ID)) THEN
        PT2SAV(IT)=PT2IN(ID)
        IRASAV(IT)=IRAD(ID)
        A1SAVE(IT)=AEX1(ID)
        A3SAVE(IT)=AEX3(ID)
        B1SAVE(IT)=BX1(ID)
        B3SAVE(IT)=BX3(ID)
        PT2GG(IRP)=PT2MX
        PT2IQQ(IT)=PT2MX
        PT2IN(ID)=PT2MX*DBLE(PHAR(103))
        IRAD(ID)=10000+IRP
        AEX1(ID)=YQ
        AEX3(ID)=YQ
        BX1(ID)=PHI
        BX3(ID)=PHI
      ENDIF

      RETURN

 900  PT2GG(IRP)=0.0D0
      RETURN

C**** END OF ARGING ****************************************************
      END
C***********************************************************************
C $Id: arging.f,v 3.25 2001/12/05 13:29:23 leif Exp $

      SUBROUTINE ARGIG2(ID,IRP)

C...ARiadne Generate INitial state G->QQ

C...Generate kinematical variables describing an initial-state g->qqbar
C...splitting.


      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'arlist.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'leptou.f'

      INTEGER NTT,NTE1,NTE2

      INCLUDE 'ardble.f'

      DATA NTT/0/,NTE1/0/,NTE2/0/


      XIINT=0.0D0
      CY=0.0D0
      CQ=0.0D0
      THEMAX=0.0D0
      YINT=0.0D0
      ZQ=0.0D0

      IF (MHAR(102).LT.0) RETURN

      QEXDIS=((MSTA(1).EQ.3.AND.IO.EQ.0.AND.MHAR(120).GT.0).OR.
     $     (MSTA(1).EQ.2.AND.IO.EQ.0.AND.
     $     XQ2.GT.0.0.AND.MHAR(120).GT.0))

      IR=INQ(IRP)
      IT=IRP+5-MAXPAR

      IF (IRAD(ID).EQ.10000+IRP) THEN
        PT2IN(ID)=PT2SAV(IT)
        IRAD(ID)=IRASAV(IT)
        AEX1(ID)=A1SAVE(IT)
        AEX3(ID)=A3SAVE(IT)
        BX1(ID)=B1SAVE(IT)
        BX3(ID)=B3SAVE(IT)
      ENDIF

      NPREM=2
      IPREM(1)=IRP
      IPREM(2)=IR
      KQ=IDO(IRP)
      KF=K(IT,2)
      RMQ=PYMASS(KQ)
      DMQ2=RMQ**2
      IT=IRP+5-MAXPAR
      IDIR=IRDIR(IT)
      PM=ARX2DB(P(IT,4)+IDIR*P(IT,3))
      PT2CUT=MAX(DBLE(PARA(3))**2,PT2IN(ID))/DBLE(PHAR(103))
      IF (QEXDIS) PT2CUT=MAX(DBLE(PARA(3))**2+DMQ2,PT2IN(ID))/
     $     DBLE(PHAR(103))

      IF (NPTOT.EQ.0) THEN
        DO 10 I=1,IPART
          IPTOT(I)=I
 10     CONTINUE
        NPTOT=IPART
      ENDIF

      NPSTQ=0
      DO 20 I=1,NPTOT
        IF (INO(IPTOT(I)).EQ.0) THEN
          IF (INQ(IPTOT(I)).GE.0) GOTO 20
          IF (K(MOD(-INQ(IPTOT(I)),10000),3).LE.2) GOTO 20
        ENDIF
        NPSTQ=NPSTQ+1
        IPSTQ(NPSTQ)=IPTOT(I)
 20   CONTINUE
      IF (MSTA(1).NE.2.OR.IDI(IRP).GT.IPART)
     $     CALL ARPADD(-IDI(IRP),NPSTQ,IPSTQ)

      QEXDY=((MHAR(120).GT.0.AND.NPSTQ.EQ.1.AND.IPSTQ(1).EQ.MAXPAR-2)
     $     .OR.(MHAR(124).EQ.2.AND.
     $     ((NPSTQ.EQ.1.AND.IPSTQ(1).EQ.MAXPAR-2).OR.NPSTQ.GT.1)))

      CALL ARSUME(0,DXR,DYR,DZR,DER,DMR,NPREM,IPREM)
      CALL ARSUME(0,DXQ,DYQ,DZQ,DEQ,DMQ,NPSTQ,IPSTQ)

      DSTOT=(DER+DEQ)**2-(DZR+DZQ)**2-(DYR+DYQ)**2-(DXR+DXQ)**2
      DMS2=DMQ**2

      XX=1.0D0-(DER+IDIR*DZR)/PM
      XXY=0.0D0
      XXQ2=0.0D0
      IF (QEXDY) XX=DMS2/DSTOT
      IF (QEXDIS) THEN
        XX=DBLE(X)
        XXY=DBLE(XY)
        XXQ2=DBLE(XQ2)
      ENDIF

      PT2MX=MIN(((DSTOT+DMQ2-DMS2)**2)/(4.0D0*DSTOT)-DMQ2,PT2LST)
      SMT2MX=PT2MX+DMQ2
      SMT2CT=PT2CUT+DMQ2
      
      IF (QEXDIS) PT2MX=PT2MX+DMQ2
      
      IF (PT2MX.LE.PT2CUT) GOTO 900

      IF (QEXDIS) THEN
        SQ2MAX=0.5D0*(XXQ2+PT2MX)
        SQ2MIN=PT2CUT/
     $       ((0.5D0+SQRT(MAX(0.25D0-PT2CUT*XX/(XXQ2*(1.0D0-XX)), 
     & 0.0D0)))*(1.0D0-XX))
      ELSE
        SQ2MAX=DSTOT-DMQ2-DMS2
        SQ2MIN=0.5D0*(DSTOT-DMQ2-DMS2-
     $       SQRT((DSTOT-DMQ2-DMS2)**2-4.0D0*(DMQ2*DMS2+PT2CUT*DSTOT)))
      ENDIF
      SQ2MIN=MAX(SQ2MIN,DMQ2+DBLE(PHAR(109)))
      IF (ABS(KQ).EQ.4) SQ2MIN=MAX(SQ2MIN,2.56D0+DBLE(PHAR(109)))

      XNUMFL=MAX(ARNOFL(SQRT(SQ2MAX),MAX(5,MSTA(15))),3.0D0)
      ALPHA0=12.0D0*ARX2DB(PARU(1))/(33.0D0-2.0D0*XNUMFL)

      IF (MHAR(127).EQ.0) THEN
        STRA0=ARSTRA(KF,KQ,XX,1.0D0,SQ2MAX)
        IF (STRA0.LE.0.0D0) THEN
          GOTO 900
        ENDIF
        IF (MHAR(118).GT.0) THEN
          STRA0=STRA0*DBLE(MHAR(118))
        ELSEIF (MHAR(118).LT.0) THEN
          STRA0=2.0D0*STRA0/(1.0D0-XX)
          IF (ABS(KQ).GE.4) STRA0=STRA0*ABS(DBLE(MHAR(118)))
        ELSE
          STRA0=MAX(STRA0,ARSTRA(KF,KQ,XX,1.0D0,SQ2MIN))
        ENDIF
      ELSE
        STRA0=ARSTRA(KF,KQ,XX,1.0D0,DBLE(MHAR(127))*SMT2MX)
        IF (STRA0.LE.0.0D0) THEN
          GOTO 900
        ENDIF
        STRA0=MAX(STRA0,ARSTRA(KF,KQ,XX,1.0D0,DBLE(MHAR(127))*SMT2CT))
      ENDIF

      C=DBLE(PHAR(104))*ALPHA0*STRA0/ARX2DB(PARU(1))
      IF (MHAR(152).NE.0) C=C*ARWGHT(10000+IRP)
      ZINT=1.0D0-XX
      CN=1.0D0/(C*ZINT)
      XLAM2=DBLE((PARA(1)**2)/PHAR(103))
      IF (QEXDY) THEN
        SQARG=1.0D0-4.0D0*(PT2CUT+DMQ2)*DSTOT/((DSTOT+DMQ2-DMS2)**2)
        XIINT=LOG((1.0D0+SQRT(SQARG))/(1.0D0-SQRT(SQARG)))
        CN=1.0D0/(C*XIINT)
      ELSEIF (QEXDIS) THEN
        CY=(1.0D0-XXY)/(1.0D0+(1.0D0-XXY)**2)
        CQ=0.125D0+0.25D0*CY
        C=DBLE(PHAR(104))*0.25D0*ALPHA0*STRA0*CQ/ARX2DB(PARU(1))
        IF (MHAR(152).NE.0) C=C*ARWGHT(10000+IRP)
        THEMAX=PT2MX
        YINT=4.0D0*LOG(SQRT(PT2MX/PT2CUT)+SQRT(PT2MX/PT2CUT-1.0D0))
        CN=1.0D0/(YINT*C)
      ENDIF

 100  IF (PT2MX.LE.PT2CUT) GOTO 900
      ARG=PYR(IDUM)
      IF (LOG(ARG)*CN.LT.
     $     LOG(LOG(PT2CUT/XLAM2)/LOG(PT2MX/XLAM2))) GOTO 900
      PT2MX=XLAM2*(PT2MX/XLAM2)**(ARG**CN)

      IF (QEXDIS) THEN
        YMAX=2.0D0*LOG(SQRT(THEMAX/PT2MX)+SQRT(THEMAX/PT2MX-1.0D0))
        Y=(PYR(IDUM)*2.0D0-1.0D0)*YMAX
        ZQ=1.0D0/(1.0D0+EXP(-Y))
        IF (MHAR(102).EQ.2) THEN
          Z=XXQ2*ZQ*(1.0D0-ZQ)/(PT2MX+XXQ2*ZQ*(1.0D0-ZQ))
        ELSE
          Z=ZQ*(1.0D0-ZQ)*XXQ2/PT2MX
        ENDIF
        IF (Z.LE.XX.OR.Z.GE.1.0D0) GOTO 100
        SQ2=PT2MX/
     $     ((0.5D0+SQRT(MAX(0.25D0-PT2MX*Z/(XXQ2*(1.0D0-Z)), 
     & 0.0D0)))*(1.0D0-Z))
        W=2.0D0*YMAX/YINT
        W=W*(Z*(1.0D0-Z)*(Z**2+(1.0D0-Z)**2)*(ZQ**2+(1.0D0-ZQ)**2)+
     $       16.0D0*((Z*(1.0D0-Z))**2)*ZQ*(1.0D0-ZQ)*CY)/CQ
        IF (MHAR(151).EQ.1) THEN
          W=W*MIN(1.0D0,LOG(PT2MX/XLAM2)/LOG(DBLE(PARA(21))*XXQ2/XLAM2))
          SQ2=MAX(SQ2,XXQ2)
        ENDIF
        XI=ZQ
        IF (XI.GE.1.0D0) GOTO 100
        IF (SQRT((PT2MX+DMQ2*(1.0D0-XI)+DMS2*XI)/(XI*(1.0D0-XI))).GE.
     $       SQRT(DSTOT)-DMR) GOTO 100
      ELSEIF (QEXDY) THEN
        XIMAX=0.5D0*(DSTOT+DMQ2-DMS2+
     $       SQRT((DSTOT+DMQ2-DMS2)**2-4.0D0*(PT2MX+DMQ2)*DSTOT))/DSTOT
        XIMIN=0.5D0*(DSTOT+DMQ2-DMS2-
     $       SQRT((DSTOT+DMQ2-DMS2)**2-4.0D0*(PT2MX+DMQ2)*DSTOT))/DSTOT
        XI=XIMIN*((XIMAX/XIMIN)**PYR(0))
        SH=(PT2MX+DMQ2*(1.0D0-XI)+DMS2*XI)/(XI*(1.0D0-XI))
        TH=-(PT2MX+DMS2*XI)/(1.0D0-XI)
        UH=DMS2+DMQ2-SH-TH
        SQ2=-TH
        IF (SQRT(DSTOT).LE.SQRT(SH)+DMR) GOTO 100
        Z=DMS2/SH
        IF (MHAR(124).GT.0) THEN
          PMR=ARPCMS(DSTOT,DMR,SH)
          PMR0=ARPCMS(DSTOT,DMR,DMQ)
          Z=XX/(1.0D0-(1.0D0-XX)*PMR/PMR0)
        ENDIF
        W=0.25D0*PT2MX/(PT2MX+XI*DMS2)
        IF (MHAR(120).EQ.1) W=W*Z
        IF (MHAR(125).EQ.940801) W=W*2.0D0
        W=W*LOG(XIMAX/XIMIN)/XIINT
        W=W*(SH**2+TH**2+2.0D0*DMS2*UH)/(SH**2)
      ELSE
        Z=XX+PYR(0)*(1.0D0-XX)
        W=(Z**2+(1.0D0-Z)**2)*0.25D0
        SQ2=PT2MX/(1.0D0-Z)
        XI=(DMQ2+PT2MX)/(XX*(1.0D0/Z-1.0D0)*DSTOT)
        IF (MHAR(124).EQ.1) THEN
          AARG=DSTOT*XX*(1.0D0-Z)+DMS2*(Z-XX)
          BARG=(DMS2-DMQ2)*Z*(1.0D0-XX)-AARG
          CARG=(DMQ2+PT2MX)*(1.0D0-XX)*Z
          SQARG=BARG**2-4.0D0*AARG*CARG
          IF (SQARG.LT.0.0D0) GOTO 100
          XI=0.5D0*(-BARG-SQRT(SQARG))/AARG
          IF (XI.LE.0.0D0) GOTO 100
        ELSEIF (MHAR(124).EQ.3) THEN
          XI=(SQ2+DMQ2)/((1.0D0-(1.0D0-XX/Z)/(1.0D0-XX))*(DSTOT-DMS2))
        ENDIF
        IF (XI.GE.1.0D0) GOTO 100
        IF (SQRT((PT2MX+DMQ2*(1.0D0-XI)+DMS2*XI)/(XI*(1.0D0-XI))).GE.
     $       SQRT(DSTOT)-DMR) GOTO 100
      ENDIF

      IF (MHAR(138).EQ.1.AND.SQ2.LT.SQ2MIN) GOTO 100
      SQ2=MAX(SQ2,SQ2MIN)
      IF (MHAR(127).EQ.0) THEN
        STRA=ARSTRA(KF,KQ,XX,Z,SQ2)
      ELSE
        SMT2MX=PT2MX+DMQ2
        IF (QEXDIS) SMT2MX=PT2MX
        STRA=ARSTRA(KF,KQ,XX,Z,DBLE(MHAR(127))*SMT2MX)
      ENDIF
      IF (STRA.LT.0.0D0) GOTO 100
      W=W*STRA/STRA0

      IF (W.LT.PYR(IDUM)) GOTO 100

      IF (QEXDIS) THEN
        YQ=ZQ
        XA=0.125D0*(1.0D0+(1.0D0-XXY)**2)*(Z**2+(1.0D0-Z)**2)*
     $       (ZQ**2+(1.0D0-ZQ)**2)/(ZQ*(1.0D0-ZQ)) 
     & +2.0D0*(1.0D0-XXY)*Z*(1.0D0-Z)
        XB=0.5D0*XXY*SQRT((1.0D0-XXY)*Z*(1.0D0-Z)/(ZQ*(1.0D0-ZQ)))*
     $       (1.0D0-2.0D0/XXY)*(1.0D0-2.0D0*ZQ)*(1.0D0-2.0D0*Z)
        XC=(1.0D0-XXY)*Z*(1.0D0-Z)
        ABC=ABS(XA)+ABS(XB)+ABS(XC)
 200    PHI=ARX2DB(PARU(2))*PYR(IDUM)
        IF (XA+XB*COS(PHI)+XC*COS(2.0D0*PHI).LT.PYR(IDUM)*ABC) GOTO 200
      ELSE
        YQ=XI
        PHI=ARX2DB(PARU(2))*PYR(IDUM)
      ENDIF

      NTT=NTT+1
      IF (W.GT.1.0D0) THEN
        NTE1=NTE1+1
        WRITE(0,*) REAL(NTE1)/REAL(NTT),REAL(NTE2)/REAL(NTT)
      ENDIF
      IF (STRA.GT.STRA0) THEN
        NTE2=NTE2+1
      ENDIF

      IF (QEXDIS) PT2MX=PT2MX-DMQ2

      IF (PT2MX*DBLE(PHAR(103)).GT.PT2IN(ID)) THEN
        PT2SAV(IT)=PT2IN(ID)
        IRASAV(IT)=IRAD(ID)
        A1SAVE(IT)=AEX1(ID)
        A3SAVE(IT)=AEX3(ID)
        B1SAVE(IT)=BX1(ID)
        B3SAVE(IT)=BX3(ID)
        PT2GG(IRP)=PT2MX
        PT2IQQ(IT)=PT2MX
        PT2IN(ID)=PT2MX*DBLE(PHAR(103))
        IRAD(ID)=10000+IRP
        AEX1(ID)=YQ
        AEX3(ID)=YQ
        BX1(ID)=PHI
        BX3(ID)=PHI
      ENDIF

      RETURN

 900  PT2GG(IRP)=0.0D0
      RETURN

C**** END OF ARGIG2 ****************************************************
      END
