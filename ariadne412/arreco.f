C***********************************************************************
C $Id: arreco.f,v 3.8 2001/11/22 10:52:01 leif Exp $

      SUBROUTINE ARRECO(ISL,PT2I,SQQI,PROB,NSL)

C...ARiadne function REConstruct Possible emissions

C...Reconstruct all possible last emissions in the event record

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'

      DIMENSION ISL(3,MAXPAR),PT2I(MAXPAR),SQQI(MAXPAR),PROB(MAXPAR)

      INXTP(I)=IP3(IDO(I))
      IPRVP(I)=IP1(IDI(I))


      NSL=0

      DO 100 I=1,IPART
C...The easy case of gluon emission
        IF ( IFL(I).EQ.21 ) THEN
          IF ( MHAR(171).GE.0 ) THEN
            NSL=NSL+1
            ISL(1,NSL)=IPRVP(I)
            ISL(2,NSL)=I
            ISL(3,NSL)=INXTP(I)
            PT2I(NSL)=ARIPT2(ISL(1,NSL),I,ISL(3,NSL))
            SQQI(NSL)=-1.0D0
            PROB(NSL)=ARPROB(ISL(1,NSL),I,ISL(3,NSL))
          ELSE
            DO 120 I1=1,IPART-1
              IF ( I1.EQ.I ) GOTO 120
              DO 130 I3=I1+1,IPART
                IF ( I3.EQ.I ) GOTO 130
                NSL=NSL+1
                ISL(1,NSL)=I1
                ISL(2,NSL)=I
                ISL(3,NSL)=I3
                PT2I(NSL)=ARIPT2(ISL(1,NSL),ISL(2,NSL),ISL(3,NSL))
                SQQI(NSL)=-1.0D0
                PROB(NSL)=ARPROB(ISL(1,NSL),ISL(2,NSL),ISL(3,NSL))
 130          CONTINUE
 120        CONTINUE
          ENDIF
        ELSE
C...This is a quark. Find parton in this string which could have been
C...part of the emitting dipole
          IF ( MHAR(171).EQ.1.AND.(I.EQ.1.OR.I.EQ.IPART) ) GOTO 100
          ID=IDO(I)
          IF ( ID.GT.0 ) THEN
            IP=IP3(ID)
          ELSE
            ID=IDI(I)
            IP=IP1(ID)
          ENDIF
          IS=ISTR(ID)
C...Find corresponding antiquark in another string
          DO 110 IQB=1,IPART
            IF ( MHAR(171).EQ.1.AND.(IQB.EQ.1.OR.IQB.EQ.IPART) )
     $           GOTO 110
            IF ( IFL(IQB).NE.-IFL(I) ) GOTO 110
            IF ( IDO(IQB).GT.0 ) THEN
              IF ( ISTR(IDO(IQB)).EQ.IS ) GOTO 110
            ELSE
              IF ( ISTR(IDI(IQB)).EQ.IS ) GOTO 110
            ENDIF
            NSL=NSL+1
            ISL(1,NSL)=IP
            ISL(2,NSL)=I
            ISL(3,NSL)=IQB
            PT2I(NSL)=ARIPT2(ISL(1,NSL),I,ISL(3,NSL))
            SQQI(NSL)=-1.0D0
            IF ( ABS(MSTA(20)).EQ.1 ) SQQI(NSL)=ARMAS2(I,IQB)
            IF ( ABS(MSTA(20)).EQ.2 )
     $           SQQI(NSL)=ARMAS2(I,IQB)-(BP(I,5)+BP(IQB,5))**2
            PROB(NSL)=ARPROB(ISL(1,NSL),I,ISL(3,NSL))
 110      CONTINUE
        ENDIF
 100  CONTINUE

      RETURN

C**** END OF ARRECO ****************************************************
      END
