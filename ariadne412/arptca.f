C***********************************************************************
C $Id: arptca.f,v 3.2 2001/11/22 10:52:01 leif Exp $

      DOUBLE PRECISION FUNCTION ARPTCA()

C...ARiadne function Cut on PT for all partons.

C...Return negative if any parton could have come from an emission with
C...pt less than the cutoff.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'


      INXTP(I)=IP3(IDO(I))
      IPRVP(I)=IP1(IDI(I))


      ARPTCA=-1.0D0

      DO 100 I=1,IPART
C...The easy case of gluon emission
        IF ( IFL(I).EQ.21 ) THEN
          IF ( ARIPT2(IPRVP(I),I,INXTP(I)).LT.DBLE(PARA(3))**2 )
     $         RETURN
        ELSE
C...This is a quark. Find parton in this string which could have been
C...part of the emitting dipole
          ID=IDO(I)
          IF ( ID.GT.0 ) THEN
            IP=IP3(ID)
          ELSE
            ID=IDI(I)
            IP=IP1(ID)
          ENDIF
          IS=ISTR(ID)
C...Find corresponding antiquark in another string
          DO 110 IQB=1,IPART
            IF ( IFL(IQB).NE.-IFL(I) ) GOTO 110
            IF ( IDO(IQB).GT.0 ) THEN
              IF ( ISTR(IDO(IQB)).EQ.IS ) GOTO 110
            ELSE
              IF ( ISTR(IDI(IQB)).EQ.IS ) GOTO 110
            ENDIF
            IF ( ARIPT2(IP,I,IQB).LT.DBLE(PARA(3))**2 ) RETURN
 110      CONTINUE
        ENDIF
 100  CONTINUE

      ARPTCA=1.0D0

      RETURN

C**** END OF ARPTCA ****************************************************
      END
