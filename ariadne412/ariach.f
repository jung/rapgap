C***********************************************************************
C $Id: ariach.f,v 3.9 2000/02/15 14:02:29 leif Exp $

      DOUBLE PRECISION FUNCTION
     $     ARIACH(DW,DMO,DMR,DV4,DPPMIN,DPPMAX,DSMAX)

C...ARiadne function Integrate ArcCosH(x)/x in a strange interval

C...Integrating ArcCosH(X)/X from strange interval determined by the
C...arguments. 

      IMPLICIT DOUBLE PRECISION(A-H, O-Z)

      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'


      ARIACH=-1.0D0

      DMR2=DMR**2
      DMO2=DMO**2

      CALL ARDCMS(DW**2,DMO,DMR,DPPMAX)
      IF (DPPMAX.LE.0.0D0) RETURN

      DPPMIN=(1.0D0-DV4)*DPPMAX
      DETA=((2.0D0+DV4)**2)/(4.0D0+4.0D0*DV4)
      DMT2MX=DMO2*DETA

      DINT=-LOG(1.0D0-DV4)*LOG(DETA)

      DLPPU=LOG(DPPMAX)
      DLPPL=LOG(DPPMIN)
      DLMTU=LOG(DETA)

      NTRY=0
      DO 100 I=1,MHAR(144)
 900    NTRY=NTRY+1
        IF (NTRY.GT.10000*MHAR(144)) THEN
          RETURN
        ENDIF
        DMT2=EXP(PYR(0)*DLMTU)*DMO2
        DOP=EXP(DLPPL+PYR(0)*(DLPPU-DLPPL))
        DZ=DOP/DPPMAX
        DS=DMT2/DZ+(DMT2-DMO2)/(1.0D0-DZ)
        IF (DS.GT.DSMAX) GOTO 900
        IF (DS.GT.DMO2*(1.0D0+DV4)) GOTO 900
        DOM=DMT2/DOP
        IF (DOM.GE.DW.OR.DOP.GE.DW) GOTO 900
        DB=(DW-DOP)*(DW-DOM)
        DMTP2=DMT2-DMO2
        DA=DB+DMR2-DMTP2
        DARG=DA**2-4.0D0*DB*DMR2
        IF (DARG.LT.0.0D0.OR.DA.LE.0.0D0) GOTO 900
        DRM=0.5D0*(DA+SQRT(DARG))/(DW-DOP)
        DRP=DMR2/DRM
        IF (DW-DRP-DOP.LT.0.0D0.OR.DW-DRP-DOM.LT.0.0D0) GOTO 900
 100  CONTINUE

      ARIACH=DBLE(MHAR(144))*DINT/DBLE(NTRY)

      RETURN

C**** END OF ARIACH ****************************************************
      END
