C***********************************************************************
C $Id: armeps.f,v 3.22 2001/12/06 13:34:40 leif Exp $

      SUBROUTINE ARMEPS(NSTART,NEND)

C...ARiadne function Matrix Element Parton shower interface version 3

C...Add dipole cascade to fixed order matrix elements

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'arstrs.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'aronia.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'pydat1.f'

      DIMENSION PT(0:6),PTMINI(0:5),PTMAXI(0:5)
      DIMENSION I1(5),I2(5),I3(5),PT2(5)

      INCLUDE 'ardble.f'

      INXTP(I)=IP3(IDO(I))
      IPRVP(I)=IP1(IDI(I))


      MHAR(163)=1

C...First scan and save the event record
      XDUM=0.0
      NHQ=0
      CALL ARSCAN(NSTART,NEND,0,IDUM)
      QDUMP=.FALSE.
      PT2LST=DBLE(PARA(40))

C...Check that initial jets are actually above specified cuts
      IF ( ARMECU(1,IPART).LT.0.0D0 ) RETURN

C...Calculate total momentum of strings for debugging
      IF (MSTA(9).GT.0) CALL ARCHEM(1)

      CALL ARPUTR(10)

C...Get the number of steps we have to go back
      NEMIT=MHAR(161)-MHAR(162)
      IF ( NEMIT.GT.5 ) THEN
        CALL ARERRM('ARMEPS',39,0)
        RETURN
      ENDIF

      PT(0)=0.0
      PTMAXI(0)=0.0
      PTMINI(0)=0.0
      IF ( MHAR(160).EQ.1 ) PT(0)=DBLE(PHAR(161))

      IF ( NEMIT.GT.0 ) THEN
        CALL ARRECS(I1,I2,I3,PT2,NEMIT)
        IF ( I1(1).LE.0.OR.I2(1).LE.0.OR.I3(1).LE.0 ) THEN
          IF (MHAR(167).EQ.0) CALL ARERRM('ARMEPS',39,0)
          RETURN
        ENDIF
      ENDIF

C...Reconstruct the most likely equivalent cascade
      DO 100 IEMIT=1,NEMIT

        PT(IEMIT)=SQRT(PT2(IEMIT))
        PTMAXI(IEMIT)=MAX(PT(IEMIT),PTMAXI(IEMIT-1))
        IF ( MHAR(160).EQ.1.AND.PT(IEMIT).LT.PT(0) ) RETURN

C...Weight with running alpha_s
        STOT=ARMAS3(I1(IEMIT),I2(IEMIT),I3(IEMIT))
        ALPRAT=ARALPS(PT2(IEMIT),STOT)/DBLE(PHAR(160))
        IF (ALPRAT.LT.PYR(0)) RETURN

C...Now reconstruct the previous step and save it
C        CALL ARUNDO(I1(IEMIT),I2(IEMIT),I3(IEMIT))
C        CALL ARPUTR(10-IEMIT)

 100  CONTINUE

      PT(NEMIT+1)=DBLE(PARA(40))
      PTMAXI(NEMIT+1)=DBLE(PARA(40))
      PTMINI(NEMIT+1)=DBLE(PARA(40))
      DO 210 IEMIT=NEMIT,1,-1
        PTMINI(IEMIT)=MIN(PTMINI(IEMIT+1),PT(IEMIT))
 210  CONTINUE

      IF ( MHAR(165).LE.1 ) THEN
        DO 220 IEMIT=NEMIT,1,-1
          IF ( MHAR(165).EQ.1)
     $         PTMINI(IEMIT)=SQRT(PTMAXI(IEMIT)*PTMINI(IEMIT))
          PTMAXI(IEMIT)=PTMINI(IEMIT)
 220    CONTINUE
      ENDIF

C      DO 200 IEMIT=NEMIT,1,-1
C
CC...Make one trial emission
C        NP=IPART
C        CALL AREVO1(PTMINI(IEMIT+1),PTMAXI(IEMIT))
C        IF ( IPART.GT.NP ) RETURN
C
CC...Recall next saved partonic state
C        CALL ARGETR(11-IEMIT)
C
C 200  CONTINUE
      IF ( ARSUVE(I1,I2,I3,PT2,NEMIT).LT.0.0D0 ) RETURN
      IF ( MSTA(28).NE.0 ) THEN
        DO 300 I=1,IPART
          IF ( .NOT.QQ(I) ) PT2GG(I)=PTMINI(1)**2
 300    CONTINUE
      ENDIF
      IF ( MHAR(162).GT.0 ) THEN
C...loops
        NP=IPART
        CALL AREVO1(PTMINI(1),0.0D0)
        IF ( IPART.GT.NP ) THEN
          IF ( ARMECU(NP+1,IPART).GT.0.0D0 ) RETURN
        ENDIF
        IF ( MHAR(170).EQ.0) CALL ARCONT
      ELSE
C...No loops
        IF ( MHAR(170).EQ.0) CALL AREVOL(PTMINI(1),0.0D0)
        IF (.NOT.QDUMP) CALL ARDUMP
      ENDIF

      MHAR(163)=0

      RETURN

C**** END OF ARMEPS ****************************************************
      END
