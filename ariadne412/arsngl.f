C***********************************************************************
C $Id: arsngl.f,v 3.4 2000/02/15 14:02:39 leif Exp $

      DOUBLE PRECISION FUNCTION ARSNGL(SR,Z)

C...ARiadne function SiNGLet integration.

C...Returns a numeric estimate of a difficult integral in the singlet
C...mechanism for onium production

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)

      TERM1(SR,Z)=(1-Z)*(-SR**6+2*SR**8-44*SR**10+3*SR**4*Z-16*SR**6*Z+
     $      108*SR**8*Z+72*SR**10*Z-2*SR**2*Z**2+18*SR**4*Z**2-
     $      42*SR**6*Z**2-236*SR**8*Z**2-6*SR**2*Z**3-32*SR**4*Z**3+
     $      204*SR**6*Z**3+48*SR**8*Z**3+2*Z**4+11*SR**2*Z**4-
     $      36*SR**4*Z**4-72*SR**6*Z**4-Z**5-4*SR**2*Z**5+
     $      24*SR**4*Z**5)/
     $     ((-1+SR)**2*SR**2*(1+SR)**2*(SR**2-2*Z+Z**2)*
     $      (SR**2-2*SR**2*Z+Z**2))


      TERM2(SR,Z)=2*(5*SR**2-12*SR**4+8*SR**6+Z+SR**2*Z-4*SR**4*Z+Z**2)*
     $    (ATAN(SQRT(1-SR**2)/SR)-ATAN(SR*(1-Z)/(SQRT(1-SR**2)*Z))-
     $     ATAN((-SR**2+Z)/(SR*SQRT(1-SR**2))))/
     $     (SR*(1-SR**2)**(5.0D0/2.0D0))


      TERM3(SR,Z)=(89*SR**2-301*SR**4+216*SR**6+21*SR**8-40*SR**10-
     $      89*SR**2*SQRT(1-SR**2)+220*SR**4*SQRT(1-SR**2)-
     $      20*SR**6*SQRT(1-SR**2)-53*SR**8*SQRT(1-SR**2)-
     $      50*SR**2*Z+262*SR**4*Z-302*SR**6*Z+104*SR**8*Z+
     $      50*SR**2*SQRT(1-SR**2)*Z-220*SR**4*SQRT(1-SR**2)*Z+
     $      118*SR**6*SQRT(1-SR**2)*Z-Z**2+19*SR**2*Z**2-
     $      112*SR**4*Z**2+135*SR**6*Z**2-48*SR**8*Z**2+
     $      SQRT(1-SR**2)*Z**2-18*SR**2*SQRT(1-SR**2)*Z**2+
     $      98*SR**4*SQRT(1-SR**2)*Z**2-55*SR**6*SQRT(1-SR**2)*Z**2)*
     $     LOG(1-SR**2)/
     $    (2*(-1+SR)**2*SR**2*(1+SR)**2*(-1+SR**2+SQRT(1-SR**2)))

      TERM4(SR,Z)=(8-24*SR**2-12*SR**4+13*SR**6-8*Z+36*SR**2*Z-
     $     14*SR**4*Z+4*Z**2-18*SR**2*Z**2+7*SR**4*Z**2)*
     $    (LOG(1+SQRT(1-SR**2))-LOG(1-SR**2+SQRT(1-SR**2)))/
     $   (1-SR**2)**(5.0D0/2.0D0)



      TERM5(SR,Z)=(1-SQRT(1-SR**2))*(-1+Z)*
     $    (-32*SR**8+96*SR**10-68*SR**12+79*SR**6*Z-150*SR**8*Z-
     $      200*SR**10*Z+252*SR**12*Z+2*SR**4*Z**2-328*SR**6*Z**2+
     $      1048*SR**8*Z**2-188*SR**10*Z**2-384*SR**12*Z**2 +
     $      97*SR**4*Z**3+70*SR**6*Z**3-1660*SR**8*Z**3+
     $      904*SR**10*Z**3+240*SR**12*Z**3+12*SR**2*Z**4-
     $      280*SR**4*Z**4+840*SR**6*Z**4+804*SR**8*Z**4-
     $      960*SR**10*Z**4+33*SR**2*Z**5+58*SR**4*Z**5-
     $      932*SR**6*Z**5+364*SR**8*Z**5+192*SR**10*Z**5+2*Z**6-
     $      40*SR**2*Z**6+152*SR**4*Z**6+188*SR**6*Z**6-
     $      192*SR**8*Z**6-Z**7+14*SR**2*Z**7-80*SR**4*Z**7+
     $      48*SR**6*Z**7)*LOG(SR**2*(1-Z)/Z)/
     $   (2*(-1+SR)*(1+SR)*SQRT(1-SR**2)*(-1+SR**2+SQRT(1-SR**2))*
     $     (SR**2-Z+SQRT(1-SR**2)*Z)*(-SR**2+Z+SQRT(1-SR**2)*Z)*
     $     (SR**2-2*SR**2*Z+Z**2)**2)


      TERM6(SR,Z)=2*SR**2*(-1+SQRT(1-SR**2))*
     $    (-8*SR**2+8*SR**4+SR**6+16*Z-8*SR**2*Z-5*SR**4*Z-
     $      7*SR**6*Z-24*Z**2+10*SR**2*Z**2+20*SR**4*Z**2+16*Z**3-
     $      13*SR**2*Z**3-7*SR**4*Z**3-4*Z**4 +5*SR**2*Z**4)*LOG(Z)/
     $   ((1-SR)*(1+SR)*SQRT(1-SR**2)*(-1+SR**2+SQRT(1-SR**2))*
     $     (SR**2-Z+SQRT(1-SR**2)*Z)*(-SR**2+Z+SQRT(1-SR**2)*Z))

      TERM7(SR,Z)=(17*SR**8 -68*SR**10-32*SR**12+40*SR**14-19*SR**6*Z+
     $      4*SR**8*Z+460*SR**10*Z-36*SR**12*Z-160*SR**14*Z+
     $      2*SR**4*Z**2+151*SR**6*Z**2-716*SR**8*Z**2-
     $      732*SR**10*Z**2+524*SR**12*Z**2+160*SR**14*Z**2-
     $      99*SR**4*Z**3+168*SR**6*Z**3+1752*SR**8*Z**3-
     $      264*SR**10*Z**3-736*SR**12*Z**3+12*SR**2*Z**4+
     $      191*SR**4*Z**4-1392*SR**6*Z**4-688*SR**8*Z**4+
     $     1120*SR**10*Z**4+112*SR**12*Z**4-73*SR**2*Z**5+
     $      384*SR**4*Z**5+936*SR**6*Z**5-660*SR**8*Z**5-
     $      304*SR**10*Z**5+2*Z**6-15*SR**2*Z**6-400*SR**4*Z**6+
     $      64*SR**6*Z**6+300*SR**8*Z**6-Z**7+52*SR**2*Z**7 +
     $      68*SR**4*Z**7-128*SR**6*Z**7-16*SR**2*Z**8+20*SR**4*Z**8)*
     $    LOG(-SR**2+Z)/
     $   (2*(-1+SR)**2*SR**2*(1+SR)**2*(-1-SQRT(1-SR**2)+Z)*
     $     (-1+SQRT(1-SR**2)+Z)*(SR**2-2*SR**2*Z+Z**2)**2)


      TERM8(SR,Z)=(15*SR**2-38*SR**4-36*SR**6+16*SR**8+10*SR**2*Z+
     $      12*SR**4*Z+16*SR**6*Z+Z**2-4*SR**2*Z**2-16*SR**4*Z**2)*
     $    (-ARIATN(SQRT(1-SR**2)/SR)-
     $      ARIATN(SR*(-1+Z)/(SQRT(1-SR**2)*Z))+
     $      ARIATN((-SR**2+Z)/(SR*SQRT(1-SR**2)))+
     $      ATAN(SQRT(1-SR**2)/SR)*LOG(1-SR**2)-
     $      ATAN(SR*(1-Z)/(SQRT(1-SR**2)*Z))*LOG(SR**2*(1-Z)/Z)-
     $      ATAN((-SR**2+Z)/(SR*SQRT(1-SR**2)))*LOG(-SR**2+Z))/
     $   (2*SR**3*(1-SR**2)**(5.0D0/2.0D0))


      TERM9(SR,Z)=2*(8-8*SR**2-SR**4-8*Z+3*SR**2*Z+7*SR**4*Z+4*Z**2-
     $      5*SR**2*Z**2)*LOG(-SR**2+2*Z-Z**2)/(-1+SR**2)**2


      TERM0(SR,Z)=(4*SR**2-6*SR**4+SR**6-4*Z-2*SR**2*Z+15*SR**4*Z-
     $      7*SR**6*Z+4*Z**2-10*SR**2*Z**2+5*SR**4*Z**2)*
     $    (LOG(1-SR**2)**2-2*LOG(1-SR**2)*LOG(1-SR**2+SQRT(1-SR**2))
     $     -LOG(1-SR**2)*LOG(1-Z)-2*LOG(1+SQRT(1-SR**2))*
     $     LOG(SR**2*(1-Z)/Z)+2*LOG(1-SR**2+SQRT(1-SR**2))*
     $     LOG(SR**2*(1-Z)/Z)+2*LOG(1+SQRT(1-SR**2)-Z)*
     $     LOG(SR**2*(1-Z)/Z)+LOG(1-SR**2)*LOG(Z)-LOG(1-SR**2)*
     $     LOG(-SR**2+Z)+2*LOG(1-SR**2+SQRT(1-SR**2))*LOG(-SR**2+Z)+
     $      LOG(1 -Z)*LOG(-SR**2+Z)-2*LOG(1 +SQRT(1-SR**2)-Z)*
     $     LOG(-SR**2+Z)-LOG(SR**2*(1-Z)/Z)*LOG(-SR**2+Z)-LOG(Z)*
     $     LOG(-SR**2+Z)-LOG(SR**2*(1-Z)/Z)*LOG(-SR**2+2*Z-Z**2)+
     $      LOG(-SR**2+Z)*LOG(-SR**2+2*Z-Z**2)+
     $      ARDILG((1-SR)*(1+SR)/(1-SR**2+SQRT(1-SR**2)))-
     $      ARDILG((-1+SR)*(1+SR)/(-1+SR**2+SQRT(1-SR**2)))+
     $      ARDILG((SR**2-Z)/(-1+SR**2+SQRT(1-SR**2)))-
     $      ARDILG(SR**2*(-1+Z)/((-1+SR**2-SQRT(1-SR**2))*Z))+
     $      ARDILG(SR**2*(-1+Z)/((-1+SR**2+SQRT(1-SR**2))*Z))-
     $      ARDILG((-SR**2+Z)/(1-SR**2+SQRT(1-SR**2))))/
     $   (1-SR**2)**(5.0D0/2.0D0)

      TERMS(SR,Z)=(TERM1(SR,Z)+TERM2(SR,Z)+TERM3(SR,Z)+TERM4(SR,Z)+
     $             TERM5(SR,Z)+TERM6(SR,Z)+TERM7(SR,Z)+TERM8(SR,Z)+
     $             TERM9(SR,Z)+TERM0(SR,Z))*Z*SR*(1-SR)


      ARSNGL=0.0D0
      IF (Z.LE.SR**2) RETURN

      ARSNGL=TERMS(SR,Z)

      RETURN
      END
