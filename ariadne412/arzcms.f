C***********************************************************************
C $Id: arzcms.f,v 3.4 2000/02/15 14:02:43 leif Exp $

      DOUBLE PRECISION FUNCTION ARZCMS(S,SM1,SM2)

C...ARiadne function get Z component in CMS

C...Returns the z component of a particle momentum when placed in the
C...cms system of itself and an other particle given the two particle
C...masses SM1 and SM2 and the total energy squared S

      INCLUDE 'arimpl.f'

      ARZCMS=-1
      AA=(S-(SM1+SM2)**2)
      IF (AA.LT.0.0D0) RETURN
      ARZCMS=0.5D0*SQRT(AA*(S-(SM1-SM2)**2)/S)

      RETURN

C**** END OF ARZCMS ****************************************************
      END
