C***********************************************************************
C $Id: arbook.f,v 3.3 2001/11/09 17:33:08 leif Exp $

      SUBROUTINE ARBOOD

C...ARiadne subroutine BOOk Dipole entry

C...Adds a new dipole entry in ardips and initializes all fields

      INCLUDE 'arimpl.f'
      INCLUDE 'ardips.f'


C...Book new dipole and check if there is enough room.
      IDIPS=IDIPS+1
      IF (IDIPS.GE.MAXDIP-10) CALL ARERRM('ARBOOD',7,0)

C...Initialize all fields
      BX1(IDIPS)=0.0D0
      BX3(IDIPS)=0.0D0
      PT2IN(IDIPS)=0.0D0
      SDIP(IDIPS)=0.0D0
      IP1(IDIPS)=0
      IP3(IDIPS)=0
      AEX1(IDIPS)=0.0D0
      AEX3(IDIPS)=0.0D0
      QDONE(IDIPS)=.FALSE.
      QEM(IDIPS)=.FALSE.
      IRAD(IDIPS)=0
      ISTR(IDIPS)=0
      ICOLI(IDIPS)=0
      PTMX2(IDIPS)=-1.0D0

C...EFGHDI fields:
      YGLU1(IDIPS)=0
      YGLU3(IDIPS)=0
      IGSIDE(IDIPS)=0

      RETURN

C**** END OF ARBOOD ****************************************************
      END
C***********************************************************************
C $Id: arbook.f,v 3.3 2001/11/09 17:33:08 leif Exp $

      SUBROUTINE ARBOOP

C...ARiadne subroutine BOOk Parton entry

C...Adds a new parton entry in arpart and initializes all fields

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'


C...Book new parton and check if there is enough room.
      IPART=IPART+1
      IF (IPART.GE.MAXPAR-10) CALL ARERRM('ARBOOP',6,0)

C...Initialize all fields
      BP(IPART,1)=0.0D0
      BP(IPART,2)=0.0D0
      BP(IPART,3)=0.0D0
      BP(IPART,4)=0.0D0
      BP(IPART,5)=0.0D0
      IFL(IPART)=0
      QEX(IPART)=.FALSE.
      QQ(IPART)=.FALSE.
      IDI(IPART)=0
      IDO(IPART)=0
      INO(IPART)=0
      INQ(IPART)=0
      XPMU(IPART)=0.0D0
      XPA(IPART)=0.0D0
      PT2GG(IPART)=0.0D0

      RETURN

C**** END OF ARBOOD ****************************************************
      END
