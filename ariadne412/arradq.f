C***********************************************************************
C $Id: arradq.f,v 3.5 2001/11/09 17:33:08 leif Exp $

      SUBROUTINE ARRADQ(ID)

C...ARiadne subroutine RADiate Q-qbar pair

C...Performs the emission of a q-qbar pair from gluon in dipole ID

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arint2.f'

      INXT(I)=IDO(IP3(I))
      IPRV(I)=IDI(IP1(I))

C...Boost dipole to its CMS and copy its invariant mass^2
      CALL ARBOCM(ID)
      BS=ARMAS2(IP1(ID),IP3(ID))
      IF (ABS(BS-SDIP(ID)).GT.(BS+SDIP(ID))*DBLE(PARA(39)).AND.
     $     MSTA(9).GE.2) CALL ARERRM('ARRADQ',13,0)

C...Check which gluon to split
      IF (IRAD(ID).LT.0) THEN
C.......Determine patons ability to recoil, save pointers and flag
C.......affected dipoles
        QR1=.TRUE.
        QR3=(QQ(IP3(ID)).AND.MSTA(16).GT.0)
        IPG=IP1(ID)
        IDN=ID
        IDP=IPRV(ID)
        IF (INXT(ID).NE.0) QDONE(INXT(ID))=.FALSE.

C...Save masses of recoiling dipoles
        YSRP=LOG(ARMAS2(IP1(IDP),IP3(IDP))/DBLE(PARA(1))**2)
        YSRN=LOG(ARMAS2(IP1(IDN),IP3(IDN))/DBLE(PARA(1))**2)

C.......Split the gluon entry, orientate the partons, and boost back
        CALL ARSPLG(IPG,ABS(IRAD(ID)))
        CALL ARORIE(IP3(IDP),IP1(IDN),IP3(IDN),BS,BX1(ID),BX3(ID),
     $              QR1,QR3,0.0D0,0.0D0)
        CALL AROBO3(THE,PHI,DBEX,DBEY,DBEZ,
     $              IP3(IDP),IP1(IDN),IP3(IDN))

C...Update colour factor rapidity regions
        YSRPN=LOG(ARMAS2(IP1(IDP),IP3(IDP))/DBLE(PARA(1))**2)
        YSRNN=LOG(ARMAS2(IP1(IDN),IP3(IDN))/DBLE(PARA(1))**2)
        YGLU1(IDN)=MAX(YGLU1(IDN)+YSRNN-YSRN,0.0D0)
        IF(YGLU3(IDN).GT.YSRNN) YGLU3(IDN)=YSRNN
        YGLU3(IDP)=MAX(YGLU3(IDP)+YSRPN-YSRP,0.0D0)
        IF(YGLU1(IDP).GT.YSRPN) YGLU1(IDP)=YSRPN

      ELSE
C.......Determine patons ability to recoil, save pointers and flag
C.......affected dipoles
        QR3=.TRUE.
        QR1=(QQ(IP1(ID)).AND.MSTA(16).GT.0)
        IPG=IP3(ID)
        IDP=ID
        IDN=INXT(ID)
        IF (IPRV(ID).NE.0) QDONE(IPRV(ID))=.FALSE.

C...Save masses of recoiling dipoles
        YSRP=LOG(ARMAS2(IP1(IDP),IP3(IDP))/DBLE(PARA(1))**2)
        YSRN=LOG(ARMAS2(IP1(IDN),IP3(IDN))/DBLE(PARA(1))**2)

C.......Split the gluon entry, orientate the partons, and boost back
        CALL ARSPLG(IPG,ABS(IRAD(ID)))
        CALL ARORIE(IP1(IDP),IP3(IDP),IP1(IDN),BS,BX1(ID),BX3(ID),
     $              QR1,QR3,0.0D0,0.0D0)
        CALL AROBO3(THE,PHI,DBEX,DBEY,DBEZ,
     $              IP1(IDP),IP3(IDP),IP1(IDN))

C...Update colour factor rapidity regions
        YSRPN=LOG(ARMAS2(IP1(IDP),IP3(IDP))/DBLE(PARA(1))**2)
        YSRNN=LOG(ARMAS2(IP1(IDN),IP3(IDN))/DBLE(PARA(1))**2)
        YGLU1(IDN)=MAX(YGLU1(IDN)+YSRNN-YSRN,0.0D0)
        IF(YGLU3(IDN).GT.YSRNN) YGLU3(IDN)=YSRNN
        YGLU3(IDP)=MAX(YGLU3(IDP)+YSRPN-YSRP,0.0D0)
        IF(YGLU1(IDP).GT.YSRPN) YGLU1(IDP)=YSRPN

      ENDIF



      RETURN

C**** END OF ARRADQ ****************************************************
      END
