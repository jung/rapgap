C***********************************************************************

      DOUBLE PRECISION FUNCTION ARSTRA(KF,KQ,X,XP,XQ2)

C...ARiadne function Structure Function RAtio

C...Return ratio of structure functions for given flavour, x, xp and Q^2

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'

      DOUBLE PRECISION XPYST(-25:25)
      REAL XPQ(-6:6)

      IF (X.LE.0.0.OR.X.GT.1.0.OR.XP.LE.0.0.OR.XP.GT.1.0) THEN
        ARSTRA=-1.0
        RETURN
      ENDIF
      IF (MSTA(1).EQ.3) THEN
        CALL LNSTRF(SNGL(X/ABS(XP)),SNGL(XQ2),XPQ)
        ARSTRA=XPQ(0)
        CALL LNSTRF(SNGL(X),SNGL(XQ2),XPQ)
        XFQ=MIN(XPQ(KQ),XPQ(-KQ))
        IF (XP.LT.0.0D0.AND.DBLE(PARA(19)).LT.0.0) THEN
          IF (XFQ.LT.-DBLE(PARA(19))) THEN
            ARSTRA=-1.0D0
            RETURN
          ENDIF
        ENDIF
C        IF (MHAR(118).EQ.0) XFQ=MAX(XFQ,ABS(PARA(19)))
        IF (MHAR(118).EQ.0.AND.XFQ.LT.DBLE(ABS(PARA(19)))) THEN
          XFQ=DBLE(ABS(PARA(19)))
        ENDIF
        IF (XFQ.GT.0.0D0) THEN
          ARSTRA=ARSTRA/XFQ
        ELSE
          ARSTRA=-1.0D0
        ENDIF
      ELSE
        CALL PYPDFU(KF,X/ABS(XP),XQ2,XPYST)
        ARSTRA=XPYST(0)
        CALL PYPDFU(KF,X,XQ2,XPYST)
        XFQ=MIN(XPYST(KQ),XPYST(-KQ))
        IF (XP.LT.0.0D0.AND.PARA(19).LT.0.0) THEN
          IF (XFQ.LT.-DBLE(PARA(19))) THEN
            ARSTRA=-1.0D0
            RETURN
          ENDIF
        ENDIF
C        IF (MHAR(118).EQ.0) XFQ=MAX(XFQ,ABS(PARA(19)))
        IF (MHAR(118).EQ.0.AND.XFQ.LT.DBLE(ABS(PARA(19)))) THEN
          XFQ=DBLE(ABS(PARA(19)))
        ENDIF
        IF (XFQ.GT.0.0D0) THEN
          ARSTRA=ARSTRA/XFQ
        ELSE
          ARSTRA=-1.0D0
        ENDIF
      ENDIF
      IF (MHAR(102).NE.2) ARSTRA=ARSTRA*XP

      RETURN

C**** END OF ARSTRA ****************************************************
      END
