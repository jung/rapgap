C***********************************************************************
C $Id: armade.f,v 3.12 2000/02/15 14:02:31 leif Exp $

      SUBROUTINE ARMADE

C...ARiadne subroutine set MAss DEpendencies

C...Sets some mass dependencies needed for ARMCDI

      INCLUDE 'arimpl.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'
      INCLUDE 'arint1.f'


      SSY=SY1+SY2+SY3
      Y1=SY1**2
      Y2=SY2**2
      Y3=SY3**2

      BC1=DBLE(Y1)+1.0D0-DBLE(SY2+SY3)**2
      IF (IFLG.GT.0.AND.MSTA(23).EQ.2) BC1=DBLE(Y1)+1.0D0
      IF (MHAR(154).EQ.1) BC1=DBLE(Y1)+1.0D0
      BC3=DBLE(Y3)+1.0D0-DBLE(SY2+SY1)**2
      IF (MHAR(154).EQ.1) BC3=DBLE(Y3)+1.0D0
      XT2M=0.0D0
      IF (SQRT(0.25D0+Y2)-1.0D0+(BC1+BC3)/2.0D0.LT.0.0D0) RETURN
      XTS=(SQRT(0.25D0+Y2)-1.0D0+(BC1+BC3)/2.0D0)**2
      XT1=BC1-2.0D0*SY1
      XT3=BC3-2.0D0*SY3
      IF (XT1.LT.0.0D0) RETURN
      IF (XT3.LT.0.0D0) RETURN
      SQUARG=1.0D0+(Y1-Y3)**2-2.0D0*(Y1+Y3)
      IF (SQUARG.LT.0.0D0) RETURN
      XT2M=MIN(XTS,XT1*XT3)

      BZP=0.5D0*(1.0D0+Y1-Y3+SQRT(SQUARG))
      BZM=0.5D0*(1.0D0+Y3-Y1+SQRT(SQUARG))

      RETURN

C**** END OF ARMADE ****************************************************
      END
