C***********************************************************************

      SUBROUTINE LDCEXE(KF,IFL,DQ2,DX,DY,MAXPAR,NPAR,DP,IFLV,NLINKS,
     $     DKY,IFLL,DVIRT,DPPV,DPMV,INFO)

C...ariadne dummy routine LDCEXE

C...Produce an error message if this routine is called without proper
C...linking of the LDC model

      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
      DIMENSION DP(MAXPAR*7),IFLV(MAXPAR)


      CALL ARERRM('LDCEXE',24,0)

      RETURN

C**** END OF LDCEXE ****************************************************
      END
