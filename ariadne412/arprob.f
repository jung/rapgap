C***********************************************************************
C $Id: arprob.f,v 3.4 2002/01/03 14:06:30 leif Exp $

      DOUBLE PRECISION FUNCTION ARPROB(I1,I2,I3)

C...ARiadne function reconstructed PROBability

C...Returns the invariant p_t^2 of three partons

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'arhide.f'

      save prob,S12,S23,S123,SY1,SY2,SY3,Y12,Y23,XT2,X1,X3,X2,CF


      ARPROB=1.0D0

      S12 = ARMAS2(I1,I2)
      S23 = ARMAS2(I2,I3)
      S123 = ARMAS3(I1,I2,I3)

      SY1=BP(I1,5)/SQRT(S123)
      SY2=BP(I2,5)/SQRT(S123)
      SY3=BP(I3,5)/SQRT(S123)

      Y12 = S12/S123-(SY1+SY2)**2
      Y23 = S23/S123-(SY2+SY3)**2
      XT2 = Y12*Y23

      X1=1.0D0-(S23-BP(I1,5)**2)/S123
      X3=1.0D0-(S12-BP(I3,5)**2)/S123
      X2=2.0D0-X1-X3

      PROB=1.0D0
      IF ( MHAR(169).EQ.0 ) PROB=ARALPS(S123*XT2, S123)
      IF ( MHAR(169).EQ.2 ) RETURN

      IF ( IFL(I2).EQ.21 ) THEN
        NXP1=2
        NXP3=2
        CF=2.0/3.0
        IF ( IFL(I1).EQ.21 ) THEN
          NXP1=3
          IF (MHAR(155).GT.0) NXP1=0
          CF=3.0/4.0
        ENDIF
        IF ( IFL(I3).EQ.21 ) THEN
          NXP3=3
          IF (MHAR(155).GT.0) NXP3=0
          CF=3.0/4.0
        ENDIF
        PROB=CF*PROB*(X1**NXP1+X3**NXP3)/XT2
      ELSE
        PROB=PROB*0.125*((1.0D0-X3+SY3**2)**2+(1.0D0-X2+SY2**2)**2)/
     $       (1.0D0-X1+SY1**2)
        IF ( MHAR(181).GT.0.OR.MSTA(23).GE.3 ) THEN
          IDN=IDO(I3)
          IF ( IDN.EQ.0 ) THEN
            I0=IP1(IDI(I3))
          ELSE
            I0=IP3(IDN)
          ENDIF
          PROP=PROP*2.0D0/(1.0D0+S123/ARMAS3(I0,I3,I2))
        ENDIF
      ENDIF

      ARPROB=MAX(PROB,0.0D0)

      RETURN

C**** END OF ARPROB ****************************************************
      END
