C***********************************************************************
C $Id: arclus.f,v 3.16 2001/11/23 12:02:24 leif Exp $

      SUBROUTINE ARCLUS(NJET)

C...ARiadne subroutine jet CLUStering

C...Clusters particle in the /LUJETS/ common block into jets according
C...the dipole clustering algorithm.

      INCLUDE 'arimpl.f'
      INCLUDE 'arpart.f'
      INCLUDE 'ardips.f'
      INCLUDE 'pyjets.f'
      INCLUDE 'ardat1.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'pydat2.f'

      INTEGER NPMAX
      PARAMETER (NPMAX=1000)
      DOUBLE PRECISION DSUMP(5),PP(5,NPMAX),PJ(5,NPMAX),PTNEXT
      INTEGER IJ(NPMAX),IP(NPMAX)
      INTEGER PYCOMP,PYCHGE

      INCLUDE 'ardble.f'

C...Reset error flag.
      MSTA(13)=0
      NJMIN=MAX(MSTU(47),2)
      IJ(1)=1

      IF (MSTU(48).NE.1) THEN
C...This is a new run
C...Reset momentum sum
        DO 100 J=1,4
          DSUMP(J)=0.0D0
 100    CONTINUE
        NP=0

C...Loop over all particles in the event record
        DO 200 I=1,N

C...Disregard all decayed particles and unknown entries
          IF (K(I,1).LE.0.OR.K(I,1).GE.10) GOTO 200

C...Disregard neutrinos and neutral particles according to MSTU(41)
          IF (MSTU(41).GE.2) THEN
            KC=PYCOMP(K(I,2))
            IF (KC.EQ.0.OR.KC.EQ.12.OR.KC.EQ.14
     $           .OR.KC.EQ.16.OR.KC.EQ.18) 
     $           GOTO 200
            IF (MSTU(41).GE.3.AND.KCHG(KC,2).EQ.0.AND.
     $           PYCHGE(K(I,2)).EQ.0)
     $           GOTO 200
          ENDIF

          IF (NP.GE.NPMAX) THEN
            CALL ARERRM('ARCLUS',21,0)
            NJET=-2
            RETURN
          ENDIF

C...Tag found jet-initiator
          NP=NP+1
          DO 210 J=1,5
            PP(J,NP)=ARX2DB(P(I,J))
            DSUMP(J)=DSUMP(J)+PP(J,NP)
 210      CONTINUE
          IP(NP)=I
 200    CONTINUE

        MSTU(62)=NP
        PARU(61)=ARDB2X(SQRT(MAX(0.0D0,DSUMP(4)**2-
     $       DSUMP(3)**2-DSUMP(2)**2-DSUMP(1)**2)))

        CALL DICLUS(2,5,NP,PP,0,0,
     $       DBLE(SQRT(PARA(31))),PTNEXT,NPMAX,NJMIN,NJ,IJ,PJ,IERR)
      ELSE

C...Just continuing from previous run
        CALL DICLUS(2,5,NP,PP,0,0,
     $       DBLE(SQRT(PARA(31))),PTNEXT,NPMAX,-NJMIN,NJ,IJ,PJ,IERR)

      ENDIF

      IF (IERR.NE.0) THEN
        NJET=-1
        RETURN
      ENDIF

C...Error if no space left in /ARPART/
      IF (NJ+N.GT.MSTU(4)) THEN
        CALL PYERRM(11,'(ARCLUS:) no more memory left in PYJETS')
        NJET=-1
        MSTU(3)=0
        RETURN
      ENDIF

C...Copy jet into /LUJETS/

      DO 400 I=1,NJ
        DO 410 J=1,5
          P(N+I,J)=ARDB2X(PJ(J,I))
 410    CONTINUE
        K(N+I,1)=31
        K(N+I,2)=97
        K(N+I,3)=I
        K(N+I,4)=0
        K(N+I,5)=0
 400  CONTINUE

C...Assign all participating particles to jets
      DO 420 I=1,NP
        IF (K(IP(I),3).NE.3) K(IP(I),4)=IJ(I)
        K(N+IJ(I),4)=K(N+IJ(I),4)+1
 420  CONTINUE

      IF (MSTU(43).EQ.2) THEN
        MSTU(3)=0
        N=N+NJ
      ELSE
        MSTU(3)=NJ
      ENDIF

      NJET=NJ
      PARU(63)=ARDB2X(PTNEXT)

      RETURN

C**** END OF ARCLUS ****************************************************
      END
