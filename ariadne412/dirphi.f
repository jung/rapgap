C***********************************************************************
C $Id: dirphi.f,v 3.1 1996/03/08 09:56:05 leif Exp $
      SUBROUTINE DIRPHI(PHI,P)
C
C Rotate around z-axis
C
C Inputs:
C         DOUBLE PHI          Rotation angle
C         DOUBLE P(3)         Vector to rotate
C
C Outputs:
C         DOUBLE P(3)         Rotated vector
C
      IMPLICIT NONE
C Subroutine arguments
      DOUBLE PRECISION PHI,P(3)
C Other variables
      DOUBLE PRECISION CPHI,SPHI,PX,PY
C End of declarations
C
      IF (ABS(PHI).LE.1.0D-20) RETURN
C
      SPHI=SIN(PHI)
      CPHI=COS(PHI)
      PX=P(1)
      PY=P(2)
      P(1)=CPHI*PX-SPHI*PY
      P(2)=SPHI*PX+CPHI*PY
C
      RETURN
C
      END
C
C***********************************************************************
