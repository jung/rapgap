C***********************************************************************
C $Id: arrflv.f,v 3.6 2001/10/21 19:36:49 leif Exp $

      SUBROUTINE ARRFLV(KF,IFL,QSEA,KFR1,KFR2,KFH)

C...ARiadne subroutine Remnant FLaVours

C...Generate flavours of remnants of particle KF, when taking awau a
C...parton IFL (which is a sea quark if QSEA). Returns kf codes KFR1 and
C...KFR2 of remnants. KFH is the code of a possible chopped off hadron
C...corresponding to the valens(-di-)quark KFR2.


      INCLUDE 'arimpl.f'
      INCLUDE 'pydat1.f'
      INCLUDE 'leptou.f'


      KFR1=0
      KFR2=0
      KFH=0

C...Get valens flavours of colliding particle
      IFL1=SIGN(MOD(ABS(KF)/1000,10),KF)
      IFL2=SIGN(MOD(ABS(KF)/100,10),KF)
      IFL3=SIGN(MOD(ABS(KF)/10,10),KF)

      IF (IFL.NE.IFL1.AND.IFL.NE.IFL2.AND.IFL.NE.IFL3) QSEA=.TRUE.

      IF (.NOT.QSEA) THEN
C...Deal with valens case
        IF (IFL.EQ.IFL2) THEN
          IFL2=IFL1
        ELSEIF (IFL.EQ.IFL3) THEN
          IFL3=IFL1
        ENDIF
        IFL1=IFL
        IF (IFL3.EQ.0) THEN
          KFR1=IFL2
          RETURN
        ENDIF
        KFR1=SIGN(MAX(ABS(IFL2),ABS(IFL3))*1000+
     $       MIN(ABS(IFL2),ABS(IFL3))*100+3,IFL1)
        IF (IFL2.NE.IFL3.AND.PYR(0).LT.DBLE(PARL(4)))
     $       KFR1=SIGN(ABS(KFR1)-2,KFR1)
        RETURN
      ENDIF

C...Divide valens quarks into two parts
 900  IF (IFL3.EQ.0) THEN
C...Meson case
        KFR1=IFL1
        KFR2=IFL2
      ELSE
C...Baryon case        
        RND=PYR(0)
        IF (RND.GT.1.0D0/3.0D0) THEN
          IFL0=IFL1
          IFL1=IFL2
          IFL2=IFL0
        ELSEIF (RND.GT.2.0D0/3.0D0) THEN
          IFL0=IFL1
          IFL1=IFL3
          IFL3=IFL0
        ENDIF
        KFR2=SIGN(MAX(ABS(IFL2),ABS(IFL3))*1000+
     $       MIN(ABS(IFL2),ABS(IFL3))*100+3,IFL1)
        IF (IFL2.NE.IFL3.AND.PYR(0).LT.DBLE(PARL(4)))
     $       KFR2=SIGN(ABS(KFR2)-2,KFR2)
        KFR1=IFL1
      ENDIF

      IF (IFL.EQ.21.OR.IFL.EQ.0) RETURN

C...Combine struck parner with one of the valens
C...flavours into a hadron.
      IF (KFR1*IFL.GT.0) THEN
        KFH=KFR1
        KFR1=KFR2
        KFR2=KFH
      ENDIF
      CALL PYKFDI(KFR2,-IFL,IDUM,KFH)
      IF (KFH.EQ.0) GOTO 900


      RETURN

C**** END OF ARRFLV ****************************************************
      END

