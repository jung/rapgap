# Copyright (C) 1995-2019, Rene Brun and Fons Rademakers.
# All rights reserved.
#
# For the licensing terms see $ROOTSYS/LICENSE.
# For the list of contributors see $ROOTSYS/README/CREDITS.

# - Locate ariadne library
# Defines:
#
#  ARIADNE_FOUND
#  ARIADNE_LIBRARY
#  ARIADNE_LIBRARY_DIR (not cached)
#  ARIADNE_LIBRARIES (not cached)

set(_ariadnedirs
  ${ARIADNE}
  $ENV{ARIADNE}
  ${ARIADNE_DIR}
  $ENV{ARIADNE_DIR}
)

find_library(ARIADNE_LIBRARY NAMES ariadne412 ar4
             HINTS ${_ariadnedirs}
             PATH_SUFFIXES lib lib64
             DOC "Specify the Ariadne library here.")

set(ARIADNE_LIBRARIES ${ARIADNE_LIBRARY})


get_filename_component(ARIADNE_LIBRARY_DIR ${ARIADNE_LIBRARY} PATH)


include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Ariadne DEFAULT_MSG ARIADNE_LIBRARY)

mark_as_advanced(ARIADNE_LIBRARY)
