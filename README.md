# RAPGAP

- Installation
You need the following external packages:

HepMC-2.06.09 installed under e.g. /Users/jung/MCgenerators/hepmc/HepMC-2.06.09/local

lhapdf/6.2.1 installed under e.g. /Users/jung/MCgenerators/lhapdf/6.2.1/local
-- hepmc2 (if you want to write out hepmc files)

-- git clone  ssh://git@gitlab.cern.ch:7999/jung/rapgap.git

cd rapgap

autoreconf --install --force

./configure --disable-shared --prefix=/Users/jung/jung/cvs/rapgap33/local  --with-hepmc="/Users/jung/MCgenerators/hepmc/HepMC-2.06.09/local" --with-lhapdf6="/Users/jung/MCgenerators/lhapdf/6.2.1/local"

make

make install 

- The executbale etc are then under e.g. /Users/jung/jung/cvs/rapgap33/local 




