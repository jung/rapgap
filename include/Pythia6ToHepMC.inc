CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      INTEGER HepMC_new_writer,HepMC_delete_writer
      INTEGER HepMC_set_event_number
      INTEGER HepMC_set_hepevt_address
      INTEGER HepMC_convert_event,HepMC_write_event,HepMC_clear_event
      INTEGER HepMC_set_attribute_int,HepMC_set_attribute_double
      INTEGER HepMC_set_pdf_info,HepMC_set_cross_section
      INTEGER HepMC_new_weight
      INTEGER HepMC_set_weight_by_index
      INTEGER HepMC_set_weight_by_name
      INTEGER  HepMC_version
      EXTERNAL HepMC_new_writer,HepMC_delete_writer
      EXTERNAL HepMC_set_event_number
      EXTERNAL HepMC_set_hepevt_address
      EXTERNAL HepMC_convert_event,HepMC_write_event
      EXTERNAL HepMC_clear_event
      EXTERNAL HepMC_set_attribute_int,HepMC_set_attribute_double
      EXTERNAL HepMC_set_pdf_info,HepMC_set_cross_section
      EXTERNAL HepMC_new_weight
      EXTERNAL HepMC_set_weight_by_index
      EXTERNAL HepMC_set_weight_by_name
      EXTERNAL HepMC_version
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      INTEGER NMXHEP
      PARAMETER (NMXHEP=4000)
      COMMON /HEPEVT/  NEVHEP,NHEP,ISTHEP(NMXHEP),IDHEP(NMXHEP),
     &                 JMOHEP(2,NMXHEP),JDAHEP(2,NMXHEP),PHEP(5,NMXHEP),
     &                 VHEP(4,NMXHEP)
      INTEGER          NEVHEP,NHEP,ISTHEP,IDHEP,JMOHEP,JDAHEP
      DOUBLE PRECISION PHEP,VHEP 
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
C...Copyright (C) 2014-2019 The HepMC collaboration
C...These variables will be used for conversion. Block size is different
C...in Pythia6 and in HepMC3, so the most simple portable way is to have
C... a second block of same size as in HepMC3 and  copy the content of 
C...block directly.
      INTEGER NMXHEPL
      PARAMETER (NMXHEPL=10000)
      COMMON /HEPEVTL/  NEVHEPL,NHEPL,ISTHEPL(NMXHEPL),IDHEPL(NMXHEPL),
     &           JMOHEPL(2,NMXHEPL),JDAHEPL(2,NMXHEPL),PHEPL(5,NMXHEPL),
     &                 VHEPL(4,NMXHEPL)
      INTEGER          NEVHEPL,NHEPL,ISTHEPL,IDHEPL,JMOHEPL,JDAHEPL
      DOUBLE PRECISION PHEPL,VHEPL
      

