C Construct PDFs for pion or other meson
      SUBROUTINE PION_XPQ(XX,Q2,XPQ)
      IMPLICIT REAL*8 (A-G,O-Z)
      DIMENSION XPQ(-6:6)
      DOUBLE PRECISION XX,QQ,UPV,DNV,SEA,STR,CHM,BOT,TOP,GLU

      write(6,*) ' pion_xpq: dummy routine called, since LHAPDF6 is linked '
      RETURN
      END
