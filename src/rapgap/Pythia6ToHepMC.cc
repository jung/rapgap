#ifdef PYTHIA6_USE_HEPMC2
#include "Pythia6ToHepMC2.cc"
extern "C" {
    int hepmc_version_() {
        return 2;
    }
    int hepmc_delete_writer_(const int & position) {
        return hepmc2_delete_writer_(position);
    }
    int hepmc_convert_event_(const int & position) {
        return hepmc2_convert_event_(position);
    }
    int hepmc_clear_event_(const int & position) {
        return hepmc2_clear_event_(position);
    }
    int hepmc_write_event_(const int & position) {
        return hepmc2_write_event_(position);
    }
    int hepmc_set_cross_section_(const int & position, const double& x, const double& xe, const int& n1, const int& n2)
    {
        return hepmc2_set_cross_section_(position, x, xe, n1, n2);
    }
    int hepmc_set_pdf_info_(const int & position, const int& parton_id1, const int& parton_id2, const double& x1, const double& x2,
                            const double& scale_in, const double& xf1, const double& xf2,
                            const int& pdf_id1, const int& pdf_id2)
    {
        return hepmc2_set_pdf_info_(position, parton_id1, parton_id2, x1, x2, scale_in, xf1, xf2, pdf_id1, pdf_id2 );
    }
    int hepmc_set_event_number_(const int & position,int& a)
    {
        return hepmc2_set_event_number_(position,a);
    }
    int hepmc_set_hepevt_address_(int* a) {
        return hepmc2_set_hepevt_address_(a);
    }
    int hepmc_set_attribute_int_(const int & position, const int & attval, const char* attname)
    {
        return hepmc2_set_attribute_int_(position, attval, attname);
    }
    int hepmc_set_attribute_double_(const int & position, const double & attval, const char* attname)
    {
        return hepmc2_set_attribute_double_(position, attval, attname);
    }
    int hepmc_new_writer_(const int & position, const int & mode, const char* ffilename)
    {
        return hepmc2_new_writer_(position, mode, ffilename);
    }
    int hepmc_new_weight_(const int & position, const char* name)
    {
        return hepmc2_new_weight_(position, name);
    }
    int hepmc_set_weight_by_index_(const int & position, const double& val, const int & pos)
    {
        return hepmc2_set_weight_by_index_(position, val, pos);
    }
    int hepmc_set_weight_by_name_(const int & position, const double& val, const char* name)
    {
        return hepmc2_set_weight_by_name_(position, val, name);
    }
}
#else
#include "Pythia6ToHepMC3.cc"
extern "C" {
    int hepmc_version_() {
        return 3;
    }
    int hepmc_delete_writer_(const int & position) {
        return hepmc3_delete_writer_(position);
    }
    int hepmc_convert_event_(const int & position) {
        return hepmc3_convert_event_(position);
    }
    int hepmc_clear_event_(const int & position) {
        return hepmc3_clear_event_(position);
    }
    int hepmc_write_event_(const int & position) {
        return hepmc3_write_event_(position);
    }
    int hepmc_set_cross_section_(const int & position, const double& x, const double& xe, const int& n1, const int& n2)
    {
        return hepmc3_set_cross_section_(position, x, xe, n1, n2);
    }
    int hepmc_set_pdf_info_(const int & position, const int& parton_id1, const int& parton_id2, const double& x1, const double& x2,
                            const double& scale_in, const double& xf1, const double& xf2,
                            const int& pdf_id1, const int& pdf_id2)
    {
        return hepmc3_set_pdf_info_(position, parton_id1, parton_id2, x1, x2, scale_in, xf1, xf2, pdf_id1, pdf_id2 );
    }
    int hepmc_set_event_number_(const int & position,int& a)
    {
        return hepmc3_set_event_number_(position,a);
    }
    int hepmc_set_hepevt_address_(int* a) {
        return hepmc3_set_hepevt_address_(a);
    }
    int hepmc_set_attribute_int_(const int & position, const int & attval, const char* attname)
    {
        return hepmc3_set_attribute_int_(position, attval, attname);
    }
    int hepmc_set_attribute_double_(const int & position, const double & attval, const char* attname)
    {
        return hepmc3_set_attribute_double_(position, attval, attname);
    }
    int hepmc_new_writer_(const int & position, const int & mode, const char* ffilename)
    {
        return hepmc3_new_writer_(position, mode, ffilename);
    }
    int hepmc_new_weight_(const int & position, const char* name)
    {
        return hepmc3_new_weight_(position, name);
    }
    int hepmc_set_weight_by_index_(const int & position, const double& val, const int & pos)
    {
        return hepmc3_set_weight_by_index_(position, val, pos);
    }
    int hepmc_set_weight_by_name_(const int & position, const double& val, const char* name)
    {
        return hepmc3_set_weight_by_name_(position, val, name);
    }
}
#endif
